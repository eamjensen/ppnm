#include<math.h>
#include"integrator.h"

double adaptive_quadrature(double f(double x), double a, double b, double abs, double rel, double* err, int* evals) {
	if (a > b) { //only calculate integrals with a < b
		return -1 * adaptive_quadrature(f, b, a, abs, rel, err, evals);
	}

	//handling of infinities; section 8.7
	if (isinf(a) && isinf(b)) {
		double g(double t) {
			return (f((1-t)/t)+f((t-1)/t))/t/t; //eq. (8.58)
		}
		return adaptive_quadrature(g, 0.0, 1.0, abs, rel, err, evals);
	} else if (isinf(b)) {
		double g(double t) {
			return f(a + (1-t)/t)/t/t; //eq. (8.60)
		}
		return adaptive_quadrature(g, 0.0, 1.0, abs, rel, err, evals);
	} else if (isinf(a)) {
		double g(double t) {
			return f(b - (1-t)/t)/t/t; //eq. (8.62)
		}
		return adaptive_quadrature(g, 0.0, 1.0, abs, rel, err, evals);
	}


	*err = 0.0;
	*evals = 0;

	double f2 = f(a + 2*(b-a)/6); //f(x2); eqs. (8.48) and (8.51)
	double f3 = f(a + 4*(b-a)/6); //f(x3); eqs. (8.48) and (8.51)
	*evals += 2;
	return adaptive_quadrature_subdivide(f, a, b, abs, &rel, err, evals, f2, f3);
}

double adaptive_quadrature_subdivide(double f(double x), double a, double b, double abs, double* rel, double* err, int* evals, double f2, double f3) {
	double f1 = f(a + 1*(b-a)/6); //f(x3); eqs. (8.48) and (8.51)
	double f4 = f(a + 5*(b-a)/6); //f(x4); eqs. (8.48) and (8.51)
	*evals += 2;
	

	double Q = (2*f1 + f2 + f3 + 2*f4)*(b-a)/6; //eq. (8.44) via eqs. (8.48), (8.49), (8.51)
	double q = (f1 + f2 + f3 + f4)*(b-a)/4; //eq. (8.45) via eqs. (8.48), (8.50); v[i] -> (...)

	*err = fabs(Q - q); //eq. (8.46)
	double tol = abs + (*rel)*fabs(Q); //eq. (8.47)

	if (*err < tol) { //see lines 3-5 on page 81 
		return Q;
	} else {
		double Qlower, Qupper;
		Qlower = adaptive_quadrature_subdivide(f, a, (a+b)/2, abs/sqrt(2.0), rel, err, evals, f1, f2);
		Qupper = adaptive_quadrature_subdivide(f, (a+b)/2, b, abs/sqrt(2.0), rel, err, evals, f3, f4);
		return Qlower + Qupper;
	}
}

double clenshaw_curtis(double f(double x), double a, double b, double abs, double rel, double* err, int* evals) {
	/*
	 * The integral of f(x) from x=a to x=b is equivalent to 
	 * the integral of f((a+b)/2 + (b-a)*t/2) * (b-a)/2 from t=-1 to t=1 (eq. (8.52)).
	 * Substituting t -> cos(theta) yields the following integrand redefinition and 
	 * integration domain.
	 */
	double cc(double theta) {
		return f((a+b)/2 + (b-a)*cos(theta)/2)*(b-a)*sin(theta)/2;
	}
	return adaptive_quadrature(cc, 0.0, M_PI, abs, rel, err, evals);
}
