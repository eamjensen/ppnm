#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#ifndef NEWTON_H
#define NEWTON_H
int newton(double fun(const gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double epsilon);
int newton_broyden_with_gradient(double fun(const gsl_vector* x, gsl_vector* df), gsl_vector* x, double epsilon);
void finite_difference_gradient(double fun(const gsl_vector* x), const gsl_vector* x, gsl_vector* df);
int newton_broyden_without_gradient(double fun(const gsl_vector* x), gsl_vector* x, double epsilon);
#endif
