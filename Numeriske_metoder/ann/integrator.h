#ifndef ADAPTIVE_H
#define ADAPTIVE_H
double adaptive_quadrature(double f(double x), double a, double b, double abs, double rel, double* err, int* evals);
double adaptive_quadrature_subdivide(double f(double x), double a, double b, double abs, double* rel, double* err, int* evals, double f2, double f3);
double clenshaw_curtis(double f(double x), double a, double b, double abs, double rel, double* err, int* evals);
#endif
