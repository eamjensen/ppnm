#include<gsl/gsl_vector.h>
#ifndef ANN_H
#define ANN_H
typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;
ann* ann_alloc(int number_of_hidden_neurons, double activation_function(double));
void ann_free(ann* network);
void ann_print(ann* network);
double ann_feed_forward(const ann* network, double x);
double ann_derivative(const ann* network, double x);
double ann_integral(const ann* network, double lower, double x);
void ann_train(ann* network, const gsl_vector* xlist, const gsl_vector* ylist);
#endif
