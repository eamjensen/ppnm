#include<gsl/gsl_vector.h>
#include"ann.h"
#include<math.h>
#define G exp((x - 5)/0.5)

double activation_function(double x) {
	return tanh(x);
}
double fit_function(double x) {
	return -50/(1+G);
}

double dfit_function(double x) { //analytic derivative of fit_function
	return 100*G/(1+G)/(1+G);
}

double adfit_function(double x) { //analytic anti-derivative (lower bound, x = 0) of fit_function
	return -25*(2*x + log( (exp(-10) + 1)/(1+G) ));
}

int main() {
	int n = 7;
	char* fct = "tanh(x)";
	printf("Generating a neural network with %d neurons and activation function %s.\n", n, fct);
	ann* network = ann_alloc(n, activation_function);
	
	int numpoints = 15;
	double xmin = 0.0;
	double xmax = 10.0;
	printf("Tabulating %d points of a Woods-Saxon type function in the range [%g,%g].\n", numpoints, xmin, xmax);
	gsl_vector* xlist = gsl_vector_alloc(numpoints);
	gsl_vector* ylist = gsl_vector_alloc(numpoints);
	double dx = (xmax-xmin)/(numpoints-1);
	double x;
	for (int i = 0; i < numpoints; i++) {
		x = xmin + i*dx;
		gsl_vector_set(xlist, i, x);
		gsl_vector_set(ylist, i, fit_function(x));
	}

	dx = (xmax-xmin)/(n-1);
	for (int i = 0; i < n; i++) {
		gsl_vector_set(network->data, 3*i + 0, xmin + i*dx);
		gsl_vector_set(network->data, 3*i + 1, 1.0);
		gsl_vector_set(network->data, 3*i + 2, 1.0);
	}
	printf("Before training, the list of neuron parameters are as follows.\n");
	printf("a\tb\tw\n");
	ann_print(network);

	ann_train(network, xlist, ylist);

	printf("After training, the list of neuron parameters are as follows.\n");
	printf("a\tb\tw\n");
	ann_print(network);

	char* filename = "points_analytic.dat";
	printf("Printing data points to separate file, %s, for gnuplot to use.\n", filename);
	printf("The analytic derivative and analytic anti-derivative of the data points will also be printed to the file.\n");
	FILE* file = fopen(filename, "w");
	fprintf(file, "#x\ty\tdydx\tY\n");
	double y, dy, Y;
	for (int i = 0; i < numpoints; i++) {
		x = gsl_vector_get(xlist, i);
		y = gsl_vector_get(ylist, i);
		dy = dfit_function(x);
		Y = adfit_function(x);
		fprintf(file, "%g\t%g\t%g\t%g\n", x, y, dy, Y);
	}
	fclose(file);

	dx = 1.0/64;
	filename = "points_neurons.dat";
	printf("Printing points found by neural network to separate file, %s, for gnuplot to use.\n", filename);
	printf("The neural network's approximation to the derivative and anti-derivative will also be printed to the file.\n");
	file = fopen(filename, "w");
	fprintf(file, "#x\ty\tdydx\tY\n");
	for (double z = xmin; z <= xmax; z += dx) {
		y = ann_feed_forward(network, z);
		dy = ann_derivative(network, z);
		Y = ann_integral(network, 0.0, z);
		fprintf(file, "%g\t%g\t%g\t%g\n", z, y, dy, Y);
	}
	fclose(file);
	
	printf("Please, see plot.svg for a comparison of the two sets of data.\n");
	printf("Also, please see plot_derivate.svg and plot_anti-derivative.svg for a comparison of the other corresponding sets of data.\n");

	gsl_vector_free(xlist); gsl_vector_free(ylist);
	ann_free(network);
	return 0;
}
