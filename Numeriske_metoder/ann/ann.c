#include<gsl/gsl_vector.h>
#include"ann.h"
#include"newton.h"
#include"integrator.h"
#define EPS 1e-3

ann* ann_alloc(int number_of_hidden_neurons, double activation_function(double)) {
	ann* network = malloc(sizeof(ann));
	network->n = number_of_hidden_neurons;
	network->f = activation_function;
	network->data = gsl_vector_alloc(3*number_of_hidden_neurons); //a1, b1, w1, a2, b2, w2, a3, ...
	return network;
}

void ann_free(ann* network) {
	gsl_vector_free(network->data);
	free(network);
}

void ann_print(ann* network) {
	int n = network->data->size;
	double a, b, w;
	int i = 0;
	while (3*i < n) {
		a = gsl_vector_get(network->data, 3*i + 0);
		b = gsl_vector_get(network->data, 3*i + 1);
		w = gsl_vector_get(network->data, 3*i + 2);
		printf("%.3f\t%.3f\t%.3f\n", a, b, w);
		i++;
	}
}

double ann_feed_forward(const ann* network, double x) {
	int n = network->n;
	double ysum = 0.0;
	double a, b, w;
	for (int i = 0; i < n; i++) {
		a = gsl_vector_get(network->data, 3*i + 0);
		b = gsl_vector_get(network->data, 3*i + 1);
		w = gsl_vector_get(network->data, 3*i + 2);
		ysum += network->f( (x - a)/b )*w;
	}
	return ysum;
}

double ann_derivative(const ann* network, double x) {
	int n = network->n;
	double ypsum = 0.0;
	double a, b, w;
	double dx = EPS, fc, fch;
	for (int i = 0; i < n; i++) {
		a = gsl_vector_get(network->data, 3*i + 0);
		b = gsl_vector_get(network->data, 3*i + 1);
		w = gsl_vector_get(network->data, 3*i + 2);
		fc = network->f( (x - a)/b )*w; //f(c) = f((x-a)/b)
		fch = network->f( (x+dx - a)/b)*w; // f(c+h) = f((x+dx-a)/b)
		ypsum += (fch-fc)/dx; //f'(c) ~ (f(c+h)-f(c))/dx
	}
	return ypsum;
}

double ann_integral(const ann* network, double lower, double x) {
	int n = network->n;
	double Ysum = 0.0;
	double a, b, w;
	double err;
	int evals;
	for (int i = 0; i < n; i++) {
		a = gsl_vector_get(network->data, 3*i + 0);
		b = gsl_vector_get(network->data, 3*i + 1);
		w = gsl_vector_get(network->data, 3*i + 2);
		Ysum += b*w*adaptive_quadrature(network->f, (lower - a)/b, (x - a)/b, EPS, EPS, &err, &evals); //the substitution u = (x-a)/b on the integral of f((x-a)/b)*w yields this result
	}
	return Ysum;
}

void ann_train(ann* network, const gsl_vector* xlist, const gsl_vector* ylist) {
	int n = network->data->size;

	double delta(const gsl_vector* p) {
		int numpoints = xlist->size;
		gsl_vector_memcpy(network->data, p); //copy p into network->data
		double diffsum = 0.0;
		double F, x, y;
		for (int i = 0; i < numpoints; i++) {
			y = gsl_vector_get(ylist, i);
			x = gsl_vector_get(xlist, i);
			F = ann_feed_forward(network, x); //the new p, copied into network->data, is used here
			diffsum += (F-y)*(F-y);
		}
		return diffsum/numpoints; //to be minimized
	}

	gsl_vector* p = gsl_vector_alloc(n);
	gsl_vector_memcpy(p, network->data); //copy network->data (containing initial neuron parameters at this point) into p
	int steps = newton_broyden_without_gradient(delta, p, EPS);

	fprintf(stderr, "training by means of Quasi-Newton (Broyden, no gradient) finished in %d steps\n", steps);
	gsl_vector_free(p);
}
