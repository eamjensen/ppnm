#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"ode.h"
#include"linked_list.h"

/*
 * This method is a long and tedious (but hopefully transparent, i.e. comparable to the notes) 
 * method, which calculates eqs. (7.18), (7.19), (7.22) and (7.23) by means of the Butcher's 
 * tableau on page 65. The result is an advancement from the point y(t) (argument t and yt) to the point y(t+h) (argument t, h and yth) with an estimated error of the step (argument err).
 */
void rkstep45(double t, double h, gsl_vector* yt, 
		void f(double t, gsl_vector* y, gsl_vector* dydt), 
		gsl_vector* yth, gsl_vector* err) {
	int n = yt->size;

	gsl_vector* k1 = gsl_vector_alloc(n);
	gsl_vector* k2 = gsl_vector_alloc(n);
	gsl_vector* k3 = gsl_vector_alloc(n);
	gsl_vector* k4 = gsl_vector_alloc(n);
	gsl_vector* k5 = gsl_vector_alloc(n);
	gsl_vector* k6 = gsl_vector_alloc(n);
	//here follows a calculation of (7.19) with values given by the Butcher's tableau on page 65
	double ttemp;
	gsl_vector* yttemp = gsl_vector_alloc(n);
	f(t, yt, k1); //k1 <- f(t, y)
	ttemp = t + 1.0/4*h;
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(1.0/4*h, k1, yttemp);
	f(ttemp, yttemp, k2); //k2 <- f(t+c2*h, y+a21*k1*h)
	ttemp = t + 3.0/8*h;
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(3.0/32*h, k1, yttemp);
	gsl_blas_daxpy(9.0/32*h, k2, yttemp);
	f(ttemp, yttemp, k3); //k3 <- f(t+c3*h, y+(a31*k1+a32*k2)*h)
	ttemp = t + 12.0/13*h;
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(1932.0/2197*h, k1, yttemp);
	gsl_blas_daxpy(-7200.0/2197*h, k2, yttemp);
	gsl_blas_daxpy(7296.0/2197*h, k3, yttemp);
	f(ttemp, yttemp, k4); //k4 <- f(t+c4*h, y+(a41*k1+a42*k2+a43*k3)*h)
	ttemp = t + 1.0*h;
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(439.0/216*h, k1, yttemp);
	gsl_blas_daxpy(-8.0*h, k2, yttemp);
	gsl_blas_daxpy(3680.0/513*h, k3, yttemp);
	gsl_blas_daxpy(-845.0/4104*h, k4, yttemp);
	f(ttemp, yttemp, k5); //k5 <- f(t+c5*h, y+(a51*k1+a52*k2+a53*k3+a54*k4)*h)
	ttemp = t + 1.0/2*h;
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(-8.0/27*h, k1, yttemp);
	gsl_blas_daxpy(2.0*h, k2, yttemp);
	gsl_blas_daxpy(-3544.0/2565*h, k3, yttemp);
	gsl_blas_daxpy(1859.0/4104*h, k4, yttemp);
	gsl_blas_daxpy(-11.0/40*h, k5, yttemp);
	f(ttemp, yttemp, k6); //k6 <- f(t+c6*h, y+(a61*k1+a62*k2+a63*k3+a64*k4+a65*k5)*h)

	//here follows a calculation of (7.18) with values given by the Butcher's tableau on page 65
	gsl_vector_memcpy(yth, yt); //copy yt into yth
	gsl_blas_daxpy(16.0/135*h, k1, yth);
	gsl_blas_daxpy(6656.0/12825*h, k3, yth);
	gsl_blas_daxpy(28561.0/56430*h, k4, yth);
	gsl_blas_daxpy(-9.0/50*h, k5, yth);
	gsl_blas_daxpy(2.0/55*h, k6, yth); //yth <- y evaluated at the new point t+h

	//here follows a calculation of (7.23) with values given by the Butcher's tableau on page 65
	gsl_vector_memcpy(yttemp, yt); //copy yt into yttemp
	gsl_blas_daxpy(25.0/216*h, k1, yttemp);
	gsl_blas_daxpy(1408.0/2565*h, k3, yttemp);
	gsl_blas_daxpy(2197.0/4104*h, k4, yttemp);
	gsl_blas_daxpy(-1.0/5*h, k5, yttemp); //yttemp <- y* evaluated at t+h; eq. (7.22)
	gsl_vector_memcpy(err, yth); //copy yth into err
	gsl_blas_daxpy(-1.0, yttemp, err); //err = yth - yttemp = y(t+h) - y*(t+h); eq. (7.23)

	gsl_vector_free(k1); gsl_vector_free(k2); gsl_vector_free(k3); gsl_vector_free(k4); gsl_vector_free(k5); gsl_vector_free(k6); gsl_vector_free(yttemp);
}

/*
 * This method advances the solution from the initial values t and y(t) (arguments t and yt) to 
 * the final values t=b and y(t=b), keeping a specified absolute and relative precision (abs 
 * and rel). t and yt assumes the final values on a succesful execution. The absolute and 
 * relative precisions are satisfied by only advancing the solution by a suggested step h, if 
 * the local error is smaller than the local tolerance for the given step; eq. (7.40)-(7.41).
 * The user suggests an initial stepsize h. A stepper, such as rkstep45 above, as well as a 
 * function f, describing the system of differential equations to be solved, must also be specified.
 */
void driver(double* t, double b, double* h, gsl_vector* yt, double abs, double rel, 
		void stepper(
			double t, double h, gsl_vector* yt, 
			void f(double t, gsl_vector* y, gsl_vector* dydt), 
			gsl_vector* yth, gsl_vector* err
			), 
		void f(double t, gsl_vector* y, gsl_vector* dydt)) {
	int n = yt->size;
	double a = *t;
	
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double ti, ei;
	while (*t < b) {
		if (*t + *h > b) { //avoid over-shooting
			*h = b - *t;
		}
		stepper(*t, *h, yt, f, yth, err);
		ti = (rel*gsl_blas_dnrm2(yth) + abs)*sqrt(*h/(b-a)); //eq. (7.41)
		ei = gsl_blas_dnrm2(err); //eq. (7.41)
		if (ei < ti) { //step OK; advance
			*t += *h;
			gsl_vector_memcpy(yt, yth); //copy yth into yt
		}
		if (ei > 0.0) { //step OK or not, prescribe same new value, as long as ei > 0.0
			*h *= pow(ti/ei, 0.25)*0.95; //eq. (7.40)
		} else {
			*h *= 2.0;
		}
	}

	gsl_vector_free(yth); gsl_vector_free(err);
}

/*
 * Same as above, but here, the values of t and corresponding function values f(t) which were 
 * accepted as steps towards the solution during execution are stored in a linked list. The head 
 * of an empty linked list 'head_of_list' is expected supplied as an argument, and the head of the 
 * resulting linked list (containing the final values) is returned by the function. See the files 
 * linked_list.c and linked_list.h.
 */
node* driver_with_storage(double* t, double b, double* h, gsl_vector* yt, double abs, double rel, 
		void stepper(
			double t, double h, gsl_vector* yt, 
			void f(double t, gsl_vector* y, gsl_vector* dydt), 
			gsl_vector* yth, gsl_vector* err
			), 
		void f(double t, gsl_vector* y, gsl_vector* dydt), node* head_of_list) {
	int n = yt->size;
	double a = *t;
	
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	gsl_vector* data = gsl_vector_alloc(n+1);
	double ti, ei;
	while (*t < b) {
		if (*t + *h > b) { //avoid over-shooting
			*h = b - *t;
		}
		stepper(*t, *h, yt, f, yth, err);
		ti = (rel*gsl_blas_dnrm2(yth) + abs)*sqrt(*h/(b-a)); //eq. (7.41)
		ei = gsl_blas_dnrm2(err); //eq. (7.41)
		if (ei < ti) { //step OK; advance
			gsl_vector_set(data, 0, *t);
			for (int i = 1; i < n + 1; i++) {
				gsl_vector_set(data, i, gsl_vector_get(yt, i - 1));
			}
			head_of_list = push(head_of_list, data);
			*t += *h;
			gsl_vector_memcpy(yt, yth); //copy yth into yt
		}
		if (ei > 0.0) { //step OK or not, prescribe same new value, as long as ei > 0.0
			*h *= pow(ti/ei, 0.25)*0.95; //eq. (7.40)
		} else {
			*h *= 2.0;
		}
	}
	gsl_vector_set(data, 0, *t);
	for (int i = 1; i < n + 1; i++) {
		gsl_vector_set(data, i, gsl_vector_get(yt, i - 1));
	}
	head_of_list = push(head_of_list, data);

	gsl_vector_free(yth); gsl_vector_free(err); gsl_vector_free(data);
	return head_of_list;
}

/*
 * Extension of ODE solving by reformulating the integral over x of f(x) from a to b as y'=f(x), 
 * y(a)=0, yielding the resulting integral = y(b).
 * The user can specify y(a) in yx (similarly to what is expected in the other drivers in this 
 * file), however, the value is not used, since y(a)=0 is the boundary condition for this type 
 * of ODE. x and yx assumes the final values b and y(b) on a succesful execution, however.
 * The values of the accepted steps are stored in a linked list, similarly to the method 
 * driver_with_storage.
 * The signature of this function is similar to the other drivers. The only difference is that 
 * instead of giving a function f, describing the system of differential equations to be solved, 
 * the user must instead supply the integrand.
 */
node* integ_to_ode_with_storage(double* x, double b, double* h, double* yx, double abs, double rel, 
		void stepper(
			double t, double h, gsl_vector* yt, 
			void f(double t, gsl_vector* y, gsl_vector* dydt), 
			gsl_vector* yth, gsl_vector* err
			), 
		double integrand(double x), node* head_of_list) {
	int n = 1;
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector_set(y, 0, 0.0); //y(a)=0

	void f(double t, gsl_vector* y, gsl_vector* dydt) {
		gsl_vector_set(dydt, 0, integrand(t)); //y'=f(x)
	}
	
	node* head = NULL;
	head = driver_with_storage(x, b, h, y, abs, rel, rkstep45, f, head);
	*yx = gsl_vector_get(y, 0); //I=y(b)
	return head;
}

