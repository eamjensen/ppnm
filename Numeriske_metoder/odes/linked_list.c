#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<stdlib.h>
#include"linked_list.h"

node* node_alloc(int dim) {
	node* n = malloc(sizeof(node));
	n->size = dim;
	n->data = malloc(dim*sizeof(double));
	n->next = NULL;
	n->prev = NULL;
	return n;
}

void node_free(node* n) {
	free(n->data);
	free(n);
}

node* push(node* head, gsl_vector* v) {
	int n = v->size;
	node* new = node_alloc(n);
	for (int i = 0; i < n; i++) {
		new->data[i] = gsl_vector_get(v, i);
	}
	if (head != NULL) {
		new->next = head;
		head->prev = new;
	} else {
		//nothing before and nothing after, node_alloc already took care of that
	}
	return new; //returning the new head of the linked list
}

void printf_list(node* head) {
	int k;
	node* n = head;
	if (n != NULL) {
		while (n->next != NULL) {
			n = n->next;
		}
	}
	//now at bottom of list
	while (n != NULL) {
		k = n->size;
		for (int i = 0; i < k - 1; i++) {
			printf("%.16f\t", n->data[i]);
		}
		printf("%.16f\n", n->data[k - 1]);
		n = n->prev;
	}
}

void fprintf_list(FILE* f, node* head) {
	int k;
	node* n = head;
	if (n != NULL) {
		while (n->next != NULL) {
			n = n->next;
		}
	}
	//now at bottom of list
	while (n != NULL) {
		k = n->size;
		for (int i = 0; i < k - 1; i++) {
			fprintf(f, "%.16f\t", n->data[i]);
		}
		fprintf(f, "%.16f\n", n->data[k - 1]);
		n = n->prev;
	}
}

void free_list(node* head) {
	node* n;
	while (head != NULL) {
		n = head->next;
		node_free(head);
		head = n;
	}
}
