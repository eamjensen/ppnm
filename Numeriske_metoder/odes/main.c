#include<stdio.h>
#include<assert.h>
#include<math.h>
#include<float.h>
#include<gsl/gsl_vector.h>
#include"ode.h"
#include"linked_list.h"

void logistic_diff(double t, gsl_vector* y, gsl_vector* dydt) {
	assert(y->size == 1);
	double yx = gsl_vector_get(y, 0);
	gsl_vector_set(dydt, 0, yx*(1 - yx));
}

void orbit_diff(double t, gsl_vector* y, gsl_vector* dydt) {
	assert(y->size == 2);
	double eps = 0.05;
	double y0 = gsl_vector_get(y, 0);
	double y1 = gsl_vector_get(y, 1);
	gsl_vector_set(dydt, 0, y1);
	gsl_vector_set(dydt, 1, 1 - y0 + eps*y0*y0);
}

double logoverroot(double x) {
	return log(x)/sqrt(x);
}

int main() {
	printf("In this exercise, an embedded Runge-Kutta-Fehlberg stepper with two methods of order 4 and 5 (RKF45) is implemented along with some adaptive stepsize drivers which follow the simple tolerance prescription in eq. (7.41) in the notes.\n");
	double abs = 1e-6;
	double rel = abs;
	printf("All relative and absolute accuracies are set to %g in this exercise unless otherwise specified.\n", abs);



	printf("\n------Part A------\n");
	int n = 1;
	double x = 0.0;
	double b = 3.0;
	double h = 0.05;
	gsl_vector* yx = gsl_vector_alloc(n);
	gsl_vector_set(yx, 0, 0.5);
	printf("Integrating y'(x) = y(x)*(1-y(x)) from x=%g to x=b=%g with the initial condition y(%g)=%g...\n", x, b, x, gsl_vector_get(yx, 0));
	driver(&x, b, &h, yx, abs, rel, rkstep45, logistic_diff);
	printf("After integration, x=%g is equal to b, as it should be.\n", x);
	printf("Initially, the stepsize was h=0.05. After integration, the last accepted stepsize is h=%g.\n", h);
	printf("Numerical result: y(%g)=%.16f.\n", x, gsl_vector_get(yx, 0));
	printf("Analytic result, y(x) = 0.5*(1+tanh(x/2)): y(%g)=%.16f.\n", x, 0.5*(1+tanh(x/2)));




	printf("\n------Part B------\n");
	x = 0.0;
	h = 0.05;
	gsl_vector_set(yx, 0, 0.5);
	printf("Doing the same, but now also storing the points, which the driver accepted along the way, in a linked list.\n");
	node* head = NULL;
	head = driver_with_storage(&x, b, &h, yx, abs, rel, rkstep45, logistic_diff, head);
	char* filename = "numlogistic.txt";
	FILE* f = fopen(filename, "w");
	printf("Printing the points, stored in the linked list, to a separate file %s.\n", filename);
	fprintf(f, "#x\ty\n");
	fprintf_list(f, head);
	free_list(head);
	fclose(f);
	filename = "analogistic.txt";
	f = fopen(filename, "w");
	printf("Printing some analytical points in the same interval to a separate file %s.\n", filename);
	fprintf(f, "#x\ty\n");
	for (double x = 0.0; x < 3.05; x+= 0.1) {
		fprintf(f, "%g\t%g\n", x, 0.5*(1+tanh(x/2)));
	}
	printf("See logistic.svg for a comparison.\n");

	n = 2;
	double phi = 0.0;
	b = 10*2*M_PI;
	h = 1e-3;
	gsl_vector* yphi = gsl_vector_alloc(2);
	gsl_vector_set(yphi, 0, 1.0);
	gsl_vector_set(yphi, 1, -0.5);
	printf("\nIntegrating u''(phi) + u(phi) = 1 + 0.05*u(phi)^2 from phi=%g to phi=%g (10 revolutions) with the initial conditions u(%g)=%g and u'(%g)=%g...\n", phi, b, phi, gsl_vector_get(yphi, 0), phi, gsl_vector_get(yphi, 1));
	head = NULL;
	head = driver_with_storage(&phi, b, &h, yphi, abs, rel, rkstep45, orbit_diff, head);
	filename = "numorbit.txt";
	f = fopen(filename, "w");
	printf("Printing the points which the driver accepted along the way to a separate file %s.\n", filename);
	fprintf(f, "#phi\tu\tu'\n");
	fprintf_list(f, head);
	free_list(head);
	fclose(f);
	printf("These are visualized in orbit.svg.\n");


	

	printf("\n------Part C------\n");
	x = DBL_EPSILON;
	b = 1.0;
	h = 1e-8;
	double y = logoverroot(x); //y(a); actually redundant, as y(a) <- 0 in integ_to_ode method. The method, however, still takes this argument; the argument contains the result when the method returns, similarly (note also the similar signature) to the other ODE methods.
	printf("Integrating f(x)=log(x)/sqrt(x) from x=a=%g (machine epsilon) to x=b=%g by letting y'=f(x), y(a)=0, yielding y(b) as the result...\n", x, b);
	head = NULL;
	head = integ_to_ode_with_storage(&x, b, &h, &y, abs, rel, rkstep45, logoverroot, head);
	printf("The numerical result is: integral=y(b)=%.16f\n", y);
	printf("The analyic result is: -4\n");
	filename = "numlogsqrt.txt";
	f = fopen(filename, "w");
	printf("Printing the points which the driver accepted along the way to a separate file %s.\n", filename);
	fprintf(f, "#x\ty\n");
	fprintf_list(f, head);
	free_list(head);
	fclose(f);
	printf("These are visualized in logsqrt.svg.\n");

	gsl_vector_free(yx); gsl_vector_free(yphi);
	return 0;
}
