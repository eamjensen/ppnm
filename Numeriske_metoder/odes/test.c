#include"linked_list.h"
#include<gsl/gsl_vector.h>
#include<stdio.h>

int main() {
	node* head = NULL;
	printf("printing empty list...\n");
	print_list(head);
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector_set(x, 0, 1.0);
	gsl_vector_set(x, 1, 2.0);
	printf("pushing vector x=(1,2) into list...\n");
	head = push(head, x);
	printf("printing list...\n");
	print_list(head);
	gsl_vector* y = gsl_vector_alloc(3);
	gsl_vector_set(y, 0, 3.0);
	gsl_vector_set(y, 1, 4.0);
	gsl_vector_set(y, 2, 5.0);
	printf("pushing vector y=(3,4,5) into list...\n");
	head = push(head, y);
	printf("printing list...\n");
	print_list(head);
	printf("freeing list...\n");
	free_list(head);


	gsl_vector_free(x); gsl_vector_free(y);
	return 0;
}
