#include<gsl/gsl_vector.h>
#ifndef ODE_H
#define ODE_H
#include"linked_list.h"
void rkstep45(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err);
void driver(double* t, double b, double* h, gsl_vector* yt, double abs, double rel, void stepper(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err), void f(double t, gsl_vector* y, gsl_vector* dydt));
node* driver_with_storage(double* t, double b, double* h, gsl_vector* yt, double abs, double rel, void stepper(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err), void f(double t, gsl_vector* y, gsl_vector* dydt), node* head_of_list);
node* integ_to_ode_with_storage(double* x, double b, double* h, double* yx, double abs, double rel, void stepper(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err), double integrand(double x), node* head_of_list);
#endif
