#include<gsl/gsl_vector.h>
#ifndef LINKED_LIST_H
#define LINKED_LIST_H
struct node {int size; struct node *next, *prev; double* data;};
typedef struct node node;
node* node_alloc(int dim);
void node_free(node* n);
node* push(node* head, gsl_vector* v);
void printf_list(node* head);
void fprintf_list(FILE* f, node* head);
void free_list(node* head);
#endif
