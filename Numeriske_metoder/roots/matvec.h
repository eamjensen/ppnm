#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef MATVEC_H
#define MATVEC_H
void matrix_print(char* str, gsl_matrix* A, int rows, int cols);
void vector_print(char* str, gsl_vector* x, int dim, int GENERAL_FORMAT);
#endif
