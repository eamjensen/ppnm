#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"ubvt_gkl.h"
#define LAMBDA_MIN 1.0/64

/*
 * This function finds a root of a multidimensional function f given an initial guess x and a 
 * tolerance epsilon which the norm of f evaluated at the root must be smaller than. This function 
 * is a translation of the pseudo-code on page 43 and the Python code on page 45 into C code. 
 * References to equation numbers are also added as comments. The number of steps taken to find 
 * the root is returned by the function, and the final argument func_calls counts the number of 
 * function calls made in order to find the root.
 */
int newton_with_jacobian(void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double epsilon, int* func_calls) {
	int n = x->size;

	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n, n);
	f(x, fx, J); //we now have our intitial fx and J of eq. (5.5)
	
	gsl_matrix *B, *U, *V; //used to solve J*Dx=-fx; eq. (5.5)
	gsl_vector* Dx; //same as above
	B = gsl_matrix_alloc(n, n);
	U = gsl_matrix_alloc(n, n);
	V = gsl_matrix_alloc(n, n);
	Dx = gsl_vector_alloc(n);

	double lambda;
	*func_calls = 0;
	int steps = 0;
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* ftemp = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(fx) > epsilon) {
		gsl_vector_scale(fx, -1.0); //fx = -fx
		ubvt_gkl_decomp(J, B, U, V);
		ubvt_gkl_solve(B, U, V, fx, Dx); //solving eq. (5.5)
		lambda = 1.0;
		gsl_vector_memcpy(xtemp, x); //copy x into xtemp
		gsl_blas_daxpy(lambda, Dx, xtemp); //xtemp = xtemp + lambda*Dx = x + lambda*Dx
		f(xtemp, ftemp, J); // ftemp <- f(xtemp) = f(x + lambda*Dx). Don't need J now
		(*func_calls)++;
		while (gsl_blas_dnrm2(ftemp) > (1 - lambda/2)*gsl_blas_dnrm2(fx) && lambda > LAMBDA_MIN) {
			lambda /= 2;
			gsl_blas_daxpy(-lambda, Dx, xtemp); //xtemp -= lambda*Dx = x + lambda*Dx
			f(xtemp, ftemp, J);
			(*func_calls)++;
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		gsl_vector_memcpy(fx, ftemp); //copy ftemp into fx; ready to solve eq. (5.5) again
		steps++;
	}

	gsl_matrix_free(B); gsl_matrix_free(U); gsl_matrix_free(V); gsl_matrix_free(J);
	gsl_vector_free(Dx); gsl_vector_free(fx); gsl_vector_free(xtemp); gsl_vector_free(ftemp);
	return steps;
}

/*
 * This function calculates eq. (5.7) and puts it in the Jacobian J (eq. (5.6)), given a function 
 * f, the "current" vectors x and fx and the difference dx.
 */
void finite_difference_Jacobian(void f(const gsl_vector* x, gsl_vector* fx), const gsl_vector* x, const gsl_vector* fx, gsl_matrix* J, double dx) {
	int n = x->size;
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* fy = gsl_vector_alloc(n);

	for (int k = 0; k < n; k++) { //calculating eq. (5.7)
		gsl_vector_set_basis(y, k); //k'th row = 1; all others = 0
		gsl_vector_scale(y, dx); //k'th row = dx
		gsl_blas_daxpy(1.0, x, y); //y += x
		f(y, fy); //fy <- f(y)
		gsl_blas_daxpy(-1.0, fx, fy); //fy <- f(y) - f(x)
		gsl_vector_scale(fy, 1/dx); //fy /= dx
		for (int i = 0; i < n; i++) {
			gsl_matrix_set(J, i, k, gsl_vector_get(fy, i));
		}
	}

	gsl_vector_free(y);
	gsl_vector_free(fy);
}

/*
 * Same as newton_with_jacobian, but without analytic Jacobian. Instead, the function 
 * finite_difference_Jacobian is used to find an approximate Jacobian.
 */
int newton(void f(const gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double epsilon, int* func_calls) {
	int n = x->size;

	gsl_vector* fx = gsl_vector_alloc(n);
	f(x, fx); //intitial fx of eq. (5.5)
	gsl_matrix* J = gsl_matrix_alloc(n, n);
	finite_difference_Jacobian(f, x, fx, J, dx); //initial J of eq. (5.5)
	
	gsl_matrix *B, *U, *V;
	gsl_vector* Dx;
	B = gsl_matrix_alloc(n, n);
	U = gsl_matrix_alloc(n, n);
	V = gsl_matrix_alloc(n, n);
	Dx = gsl_vector_alloc(n);

	double lambda;
	*func_calls = 0;
	int steps = 0;
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* ftemp = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(fx) > epsilon) {
		gsl_vector_scale(fx, -1.0);
		ubvt_gkl_decomp(J, B, U, V);
		ubvt_gkl_solve(B, U, V, fx, Dx);
		lambda = 1.0;
		gsl_vector_memcpy(xtemp, x);
		gsl_blas_daxpy(lambda, Dx, xtemp);
		f(xtemp, ftemp);
		(*func_calls)++;
		while (gsl_blas_dnrm2(ftemp) > (1 - lambda/2)*gsl_blas_dnrm2(fx) && lambda > dx) {
			lambda /= 2;
			gsl_blas_daxpy(-lambda, Dx, xtemp);
			f(xtemp, ftemp);
			(*func_calls)++;
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		gsl_vector_memcpy(fx, ftemp); //copy ftemp into fx
		finite_difference_Jacobian(f, x, fx, J, dx); //recalculate J; ready to solve eq. (5.5) again
		steps++;
	}

	gsl_matrix_free(B); gsl_matrix_free(U); gsl_matrix_free(V); gsl_matrix_free(J);
	gsl_vector_free(Dx); gsl_vector_free(fx); gsl_vector_free(xtemp); gsl_vector_free(ftemp);
	return steps;
}

int newton_quadratic_interpolation(void f(const gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double epsilon, int* func_calls) {
	int n = x->size;

	gsl_vector* fx = gsl_vector_alloc(n);
	f(x, fx);
	gsl_matrix* J = gsl_matrix_alloc(n, n);
	finite_difference_Jacobian(f, x, fx, J, dx);
	
	gsl_matrix *B, *U, *V;
	gsl_vector* Dx;
	B = gsl_matrix_alloc(n, n);
	U = gsl_matrix_alloc(n, n);
	V = gsl_matrix_alloc(n, n);
	Dx = gsl_vector_alloc(n);

	double lambda;
	*func_calls = 0;
	int steps = 0;
	double g0 = 0.5*gsl_blas_dnrm2(fx)*gsl_blas_dnrm2(fx); //g(0); between eq. (5.9) and (5.10)
	double gp0 = -2.0*g0; //g'(0); between eq. (5.9) and (5.10)
	double c, gltrial; //gltrial means g(lambda_trial); between eq. (5.9) and (5.10)
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* ftemp = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(fx) > epsilon) {
		gsl_vector_scale(fx, -1.0);
		ubvt_gkl_decomp(J, B, U, V);
		ubvt_gkl_solve(B, U, V, fx, Dx);
		lambda = 1.0;
		gsl_vector_memcpy(xtemp, x);
		gsl_blas_daxpy(lambda, Dx, xtemp);
		f(xtemp, ftemp);
		(*func_calls)++;
		while (gsl_blas_dnrm2(ftemp) > (1 - lambda/2)*gsl_blas_dnrm2(fx) && lambda > dx) {
			gltrial = 0.5*gsl_blas_dnrm2(ftemp)*gsl_blas_dnrm2(ftemp);
			c = (gltrial - g0 - gp0*lambda)/(lambda*lambda); //eq. (5.11)
			lambda = -gp0/(2*c); //eq. (5.12)
			gsl_blas_daxpy(-lambda, Dx, xtemp);
			f(xtemp, ftemp);
			(*func_calls)++;
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		gsl_vector_memcpy(fx, ftemp); //copy ftemp into fx
		finite_difference_Jacobian(f, x, fx, J, dx); //recalculate J
		g0 = 0.5*gsl_blas_dnrm2(fx)*gsl_blas_dnrm2(fx); //recalculate g(0)
		gp0 = -2.0*g0; //recalculate g'(0)
		steps++;
	}

	gsl_matrix_free(B); gsl_matrix_free(U); gsl_matrix_free(V); gsl_matrix_free(J);
	gsl_vector_free(Dx); gsl_vector_free(fx); gsl_vector_free(xtemp); gsl_vector_free(ftemp);
	return steps;
}
