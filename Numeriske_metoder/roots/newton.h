#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef NEWTON_H
#define NEWTON_H
int newton_with_jacobian(void f(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x, double epsilon, int* func_calls);
void finite_difference_Jacobian(void f(const gsl_vector* x, gsl_vector* fx), const gsl_vector* x, const gsl_vector* fx, gsl_matrix* J, double dx);
int newton(void f(const gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double epsilon, int* func_calls);
int newton_quadratic_interpolation(void f(const gsl_vector* x, gsl_vector* fx), gsl_vector* x, double dx, double epsilon, int* func_calls);
#endif
