#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include"newton.h"
#include"matvec.h"

void funs(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) { //see problem description
	assert(x->size == 2);
	double A = 10000.0;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, A*x0*x1 - 1);
	gsl_vector_set(fx, 1, exp(-x0) + exp(-x1) - 1 - 1/A);
	gsl_matrix_set(J, 0, 0, A*x1);
	gsl_matrix_set(J, 0, 1, A*x0);
	gsl_matrix_set(J, 1, 0, -exp(-x0));
	gsl_matrix_set(J, 1, 1, -exp(-x1));
}

void funs_no_J(const gsl_vector* x, gsl_vector* fx) {
	assert(x->size == 2);
	double A = 10000.0;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, A*x0*x1 - 1);
	gsl_vector_set(fx, 1, exp(-x0) + exp(-x1) - 1 - 1/A);
}

int funs_GSL(const gsl_vector* x, void* params, gsl_vector* fx) {
	assert(x->size == 2);
	double A = 10000.0;
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, A*x0*x1 - 1);
	gsl_vector_set(fx, 1, exp(-x0) + exp(-x1) - 1 - 1/A);

	return GSL_SUCCESS;
}

void rosenbrock(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) { //see problem description
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, -2*(1-x0) - 400*x0*(x1-x0*x0));
	gsl_vector_set(fx, 1, 200*(x1-x0*x0));
	gsl_matrix_set(J, 0, 0, 2 - 400*x1 + 1200*x0*x0);
	gsl_matrix_set(J, 0, 1, -400*x0);
	gsl_matrix_set(J, 1, 0, -400*x0);
	gsl_matrix_set(J, 1, 1, 200.0);
}

void rosenbrock_no_J(const gsl_vector* x, gsl_vector* fx) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, -2*(1-x0) - 400*x0*(x1-x0*x0));
	gsl_vector_set(fx, 1, 200*(x1-x0*x0));
}

int rosenbrock_GSL(const gsl_vector* x, void* params, gsl_vector* fx) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, -2*(1-x0) - 400*x0*(x1-x0*x0));
	gsl_vector_set(fx, 1, 200*(x1-x0*x0));

	return GSL_SUCCESS;
}

void himmelblau(const gsl_vector* x, gsl_vector* fx, gsl_matrix* J) { //see problem description
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7));
	gsl_vector_set(fx, 1, 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7));
	gsl_matrix_set(J, 0, 0, 12*x0*x0 + 4*x1 - 42);
	gsl_matrix_set(J, 0, 1, 4*x0 + 4*x1);
	gsl_matrix_set(J, 1, 0, 4*x0 + 4*x1);
	gsl_matrix_set(J, 1, 1, 4*x0 + 12*x1*x1 - 26);
}

void himmelblau_no_J(const gsl_vector* x, gsl_vector* fx) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7));
	gsl_vector_set(fx, 1, 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7));
}

int himmelblau_GSL(const gsl_vector* x, void* params, gsl_vector* fx) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(fx, 0, 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7));
	gsl_vector_set(fx, 1, 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7));

	return GSL_SUCCESS;
}

int main() {
	int n = 2;
	double tolerance = 1e-6, dx = 0.0;
	int steps = 0, func_calls = 0;
	gsl_vector* x = gsl_vector_alloc(n);
	printf("The tolerances of the approximations to all the roots in this exercise are %g.\n", tolerance);




	printf("\n------Exercise A------\n");
	printf("Solving the system of equations\nA*x*y = 1,\nexp(-x) + exp(-y) = 1 + 1/A;\nwith A = 10000\nwith analytic Jacobian...\n");
	gsl_vector_set(x, 0, 20.0);
	gsl_vector_set(x, 1, -5.0);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton_with_jacobian(funs, x, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 1);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	printf("\nFinding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nby searching for the root of its gradient with analytic Jacobian...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton_with_jacobian(rosenbrock, x, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	printf("\nFinding the minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nby searching for the roots of its gradient with analytic Jacobian...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton_with_jacobian(himmelblau, x, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	

	
	printf("\n------Exercise B------\n");
	printf("Solving the system of equations\nA*x*y = 1,\nexp(-x) + exp(-y) = 1 + 1/A;\nwith A = 10000\nwithout analytic Jacobian...\n");
	gsl_vector_set(x, 0, 20.0);
	gsl_vector_set(x, 1, -5.0);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	dx = 1e-8;
	printf("dx=%g\n", dx);
	steps = newton(funs_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 1);
	printf("in %d steps and %d function calls\n", steps, func_calls);
	gsl_vector_set(x, 0, 20.0);
	gsl_vector_set(x, 1, -5.0);
	gsl_multiroot_function F;
	F.f = funs_GSL;
	F.n = n;
	F.params = NULL;
	gsl_multiroot_fsolver* s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_dnewton, n);
	gsl_multiroot_fsolver_set(s, &F, x);
	int flag = GSL_CONTINUE, iter = 0;
	do {
		gsl_multiroot_fsolver_iterate(s);
		iter++;
		flag = gsl_multiroot_test_residual(s->f, tolerance);
	} while (flag == GSL_CONTINUE);
	gsl_vector_set(x, 0, gsl_vector_get(s->x, 0));
	gsl_vector_set(x, 1, gsl_vector_get(s->x, 1));
	vector_print("Under similar conditions, GSL (gsl_multiroot_fsolver_dnewton) finds the root at (x, y)=", x, n, 1);
	printf("in %d iterations\n", iter);

	printf("\nFinding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nby searching for the root of its gradient without analytic Jacobian...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	printf("dx=%g\n", dx);
	steps = newton(rosenbrock_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	F.f = rosenbrock_GSL;
	gsl_multiroot_fsolver_set(s, &F, x);
	flag = GSL_CONTINUE, iter = 0;
	do {
		gsl_multiroot_fsolver_iterate(s);
		iter++;
		flag = gsl_multiroot_test_residual(s->f, tolerance);
	} while (flag == GSL_CONTINUE);
	gsl_vector_set(x, 0, gsl_vector_get(s->x, 0));
	gsl_vector_set(x, 1, gsl_vector_get(s->x, 1));
	vector_print("Under similar conditions, GSL (gsl_multiroot_fsolver_dnewton) finds the root at (x, y)=", x, n, 0);
	printf("in %d iterations\n", iter);

	printf("\nFinding the minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nby searching for the roots of its gradient without analytic Jacobian...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	printf("dx=%g\n", dx);
	steps = newton(himmelblau_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	F.f = himmelblau_GSL;
	gsl_multiroot_fsolver_set(s, &F, x);
	flag = GSL_CONTINUE, iter = 0;
	do {
		gsl_multiroot_fsolver_iterate(s);
		iter++;
		flag = gsl_multiroot_test_residual(s->f, tolerance);
	} while (flag == GSL_CONTINUE);
	gsl_vector_set(x, 0, gsl_vector_get(s->x, 0));
	gsl_vector_set(x, 1, gsl_vector_get(s->x, 1));
	vector_print("Under similar conditions, GSL (gsl_multiroot_fsolver_dnewton) finds the root at (x, y)=", x, n, 0);
	printf("in %d iterations\n", iter);

	printf("\nThe reason for GSL being many, many times better in some cases is (probably) that GSL's implementation chooses its step-size dynamically as dx[j]=sqrt(EPS)*|x[j]| where EPS is the machine epsilon.\n");




	printf("\n------Exercise C------\n");
	printf("Solving the system of equations\nA*x*y = 1,\nexp(-x) + exp(-y) = 1 + 1/A;\nwith A = 10000\nwith quadratic interpolation and without analytic Jacobian...\n");
	gsl_vector_set(x, 0, 20.0);
	gsl_vector_set(x, 1, -5.0);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	dx = 1e-4;
	printf("dx=%g\n", dx);
	steps = newton_quadratic_interpolation(funs_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 1);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	printf("\nFinding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nby searching for the root of its gradient with quadratic interpolation and without analytic Jacobian...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	dx = 1e-8;
	printf("dx=%g\n", dx);
	steps = newton_quadratic_interpolation(rosenbrock_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	printf("\nFinding the minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nby searching for the roots of its gradient with quadratic interpolation and without analytic Jacobian...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	printf("dx=%g\n", dx);
	steps = newton_quadratic_interpolation(himmelblau_no_J, x, dx, tolerance, &func_calls);
	vector_print("Root found at (x, y)=", x, n, 0);
	printf("in %d steps and %d function calls\n", steps, func_calls);

	gsl_vector_free(x);
	gsl_multiroot_fsolver_free(s);
	return 0;
}
