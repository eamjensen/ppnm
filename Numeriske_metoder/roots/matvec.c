#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include"matvec.h"

void matrix_print(char* str, gsl_matrix* A, int rows, int cols) {
	printf("%s\n", str);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			printf("% 1.4f\t", gsl_matrix_get(A, i, j));
		}
		printf("\n");
	}
}

void vector_print(char* str, gsl_vector* x, int dim, int GENERAL_FORMAT) {
	printf("%s\n(", str);
	for (int i = 0; i < dim - 1; i++) {
		if (GENERAL_FORMAT == 0) {
			printf("% 1.4f, ", gsl_vector_get(x, i));
		} else {
			printf("% g, ", gsl_vector_get(x, i));
		}
	}
	if (GENERAL_FORMAT == 0) {
		printf("% 1.4f)\n", gsl_vector_get(x, dim - 1));
	} else {
		printf("% g)\n", gsl_vector_get(x, dim - 1));
	}
}
