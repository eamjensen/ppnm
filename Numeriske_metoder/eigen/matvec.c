#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"matvec.h"
#define RND (double) rand()/RAND_MAX

void generate_symmetric_random_matrix_elements(gsl_matrix* A, int dim) {
        double currentRND;
        for (int i = 0; i < dim; i++) {
                for (int j = i; j < dim; j++) {
                        currentRND = RND;
                        if (i == j) {
                                gsl_matrix_set(A, i, i, currentRND);
                        } else {
                                gsl_matrix_set(A, i, j, currentRND);
                                gsl_matrix_set(A, j, i, currentRND);
                        }
                }
        }
}

void generate_random_vector_elements(gsl_vector* x, int dim) {
        for (int i = 0; i < dim; i++) {
                gsl_vector_set(x, i, RND);
        }
}

void matrix_print(char* str, gsl_matrix* A, int dim) {
        printf("%s\n", str);
        for (int i = 0; i < dim; i++) {
                for (int j = 0; j < dim; j++) {
                        printf("% 1.4f\t", gsl_matrix_get(A, i, j));
                }
                printf("\n");
        }
        printf("\n");
}

void vector_print(char* str, gsl_vector* x, int dim) {
        printf("%s\n(", str);
        for (int i = 0; i < dim - 1; i++) {
                printf("% 1.4f, ", gsl_vector_get(x, i));
        }
        printf("% 1.4f)\n\n", gsl_vector_get(x, dim - 1));
}
