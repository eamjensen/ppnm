#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"matvec.h"
#include"jacobi.h"

int main() {
	printf("------Exercise B------\n");
	//argumenter hér for de ting, som står angivet i 1.a, 1.b og 2
	int n = 5;
	int rotations[n + 1];
	gsl_matrix* A = gsl_matrix_alloc(n, n);
	generate_symmetric_random_matrix_elements(A, n);
	matrix_print("A=", A, n);

	gsl_matrix* V = gsl_matrix_alloc(n, n);
	gsl_vector* e[n + 1];
	gsl_matrix* D = gsl_matrix_alloc(n, n);
	
	printf("Running Jacobi diagonalization on A first to find one eigenvalue, then on a fresh copy of A to find two eigenvalues and so on...\n");
	for (int i = 1; i <= n; i++) {
		gsl_matrix_memcpy(D, A); //copy A into D
		e[i - 1] = gsl_vector_alloc(n);
		rotations[i - 1] = jacobi_eig_by_eig(D, e[i - 1], V, i, 0);
		printf("Number of rotations on %d row(s) to find %d eigenvalue(s)=%d\n", i, i, rotations[i - 1]);
		for (int j = 1; j <= i; j++) {
			printf("\teigval #%d=% 1.4f\n", j, gsl_vector_get(e[j - 1], j - 1));
		}
	}
	printf("(Compare with outA1.txt to see that the eigenvalues are correct)\n");
	
	gsl_matrix_memcpy(D, A); //copy A into D
	e[n] = gsl_vector_alloc(n);
	rotations[n] = jacobi_cyclic_sweeps(D, e[n], V);
	printf("For comparison Jacobi diagonalization on the same matrix A with cyclic sweeps gives all eigenvalues in %d rotations.\n", rotations[n]); 
	
	/* OLD SECTION FROM WHEN JACOBI ROUTINES RETURNED THE NUMBER OF SWEEPS. PLEASE, IGNORE
	printf("\nHowever, cyclic sweeps contains (worst-case) n(n-1)/2 Jacobi rotations, whereas row-only sweeps contain (worst-case) n-k Jacobi rotations on the k'th row; n = order of matrix.\n");
	printf("So, each of the consecutive row-only sweeps contained (worst-case),\n");
	printf("%d sweeps -> %d Jacobi rotations,\n", sweeps[0], (n - 0 - 1)*sweeps[0]);
	for (int k = 1; k < n; k++) {
		int before = sweeps[k - 1];
		int after = sweeps[k];
		int diff = after - before;
		printf("%d - %d = %d sweeps -> %d Jacobi rotations,\n", after, before, diff, (n - k - 1)*diff);
	}
	printf("whereas the cyclic sweep contained (worst-case) %d Jacobi rotations, yielding all eigenvalues at once.\n", n*(n - 1)*sweeps[n]/2);
*/
	int m = 30;
	printf("\nGoing to higher n=%d, we check if the first eigenvalue can be found in fewer rotations on the first-row-only sweeps compared to finding all eigenvalues in the cyclic sweeps. This should be the case for sufficiently large n, as the number of rotations in the first case is O(n), and in the second O(n²)...\n", m);
	gsl_matrix* AA = gsl_matrix_alloc(m, m);
	generate_symmetric_random_matrix_elements(AA, m);
	gsl_matrix* DD = gsl_matrix_alloc(m, m);
	gsl_matrix_memcpy(DD, AA); //copy AA into DD
	gsl_matrix* VV = gsl_matrix_alloc(m, m);
	gsl_vector* ee = gsl_vector_alloc(m);
	int rotations_eig = jacobi_eig_by_eig(DD, ee, VV, 1, 0);
	gsl_matrix_memcpy(DD, AA);
	int rotations_cyc = jacobi_cyclic_sweeps(DD, ee, VV);
	printf("Result for first-row-only sweeps on symmetric random %dx%d matrix: %d\n", m, m, rotations_eig);
	printf("Result for cyclic sweeps on the same symmetric random %dx%d matrix: %d\n", m, m, rotations_cyc);
	printf("It seems that for sufficiently large symmetric matrices, if only the first (either lowest or highest; see below) eigenvalue is needed, then first-row-only sweeps are the way to go.\n");

	printf("\n\nFinally, here follows the arguments which the exercise asks for...\n");
	printf("One can imagine that at some point the Jacobi algorithm, with any type of sweeps, will happen upon Apq equal to or close to zero. This makes eq. (3.10) in the notes look like this:\ntan(2*phi)=0/(Aqq - App).\nThe arctan function, which determines phi, will in this case return phi=0 if Aqq > App or phi=pi/2 if Aqq < App. The first case results in an identity operation on the elements App and Aqq, and the second case results in App and Aqq being interchanged (insert phi=0 or phi=pi/2 in eq. (3.6)).\nSince App and Aqq are swapped if Aqq < App and, oppositely, are NOT swapped if Aqq > App, the smaller values on the diagonal tends towards the top of the matrix (the index q being larger than the index p) and the larger values tend towards the bottom of the matrix.\nThis scenario, with Apq practically equal to zero, is not what in reality moves the diagonal elements up and down. It is rather an effect which is accumulated through all of the Jacobi rotations carried out by the algorithm.\n");
	printf("\nChanging the criterion\ntan(2*phi)=2*Apq/(Aqq - App)\nto the equivalent criterion\ntan(2*phi)=-2*Apq/(App - Aqq)\ngives the eigenvalues on the diagonal of D in descending order instead of ascending order, since the scenario described just above is then flipped upside down: If Aqq < App the two elements are not interchanged, and if Aqq > App the two elements ARE interchanged.\n");
	printf("As an example, let us run the first-row-only sweeps on A from the beginning of this exercise with this modified criterion...\n");
	gsl_matrix_memcpy(D, A); //copy A into D
	gsl_vector* eee = gsl_vector_alloc(n);
	rotations[0] = jacobi_eig_by_eig(D, eee, V, 1, 1);
	printf("Number of rotations on 1 row(s) to find 1 eigenvalue(s)=%d\n", rotations[0]);
	printf("\teigval #1=% 1.4f\n", gsl_vector_get(eee, 0));

	
	
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	for (int i = 0; i <= n; i++) {
		gsl_vector_free(e[i]);
	}
	gsl_matrix_free(D);
	gsl_matrix_free(AA);
	gsl_matrix_free(DD);
	gsl_matrix_free(VV);
	gsl_vector_free(ee);
	gsl_vector_free(eee);
	return 0;
}
