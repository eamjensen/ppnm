#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"matvec.h"
#include"jacobi.h"

int main() {
	printf("------Exercise A------\n");
	int n = 5;
	gsl_matrix* A = gsl_matrix_alloc(n, n);
	generate_symmetric_random_matrix_elements(A, n);
	matrix_print("A=", A, n);

	gsl_matrix* V = gsl_matrix_alloc(n, n);
	gsl_vector* e = gsl_vector_alloc(n);

	gsl_matrix* D = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(D, A); //copy A into D because we need A again later
	int rotations = jacobi_cyclic_sweeps(D, e, V);
	printf("Number of rotations=%d\n\n", rotations);
	matrix_print("A (after Jacobi diag. cyclic sweeps)=", D, n);
	matrix_print("V (orth. matrix of eigvecs)=", V, n);
	vector_print("e (column vector of eigvals)=", e, n);

	gsl_matrix* X = gsl_matrix_alloc(n, n);
	gsl_matrix* Y = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, V, A, 0.0, X); //X = (V^T)*A
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, X, V, 0.0, Y); //Y = X*V = (V^T)*A*V
	matrix_print("V*A*(V^T)=D=diag(e)?", Y, n);

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	gsl_matrix_free(D);
	gsl_matrix_free(X);
	gsl_matrix_free(Y);
	return 0;
}
