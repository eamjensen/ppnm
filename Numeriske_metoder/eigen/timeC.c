#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"matvec.h"
#include"jacobi.h"

int main(int argc, char** argv) {
	int n = (argc > 1)? (int) atof(argv[1]) : 100;

	gsl_matrix* A = gsl_matrix_alloc(n, n);
	gsl_matrix* V = gsl_matrix_alloc(n, n);
	gsl_vector* e = gsl_vector_alloc(n);

	printf("Generating random symmetric elements for %dx%d matrix\n", n, n);
	generate_symmetric_random_matrix_elements(A, n);

	int rotations = jacobi_classic(A, e, V);
	printf("Diagonalized %dx%d matrix with method from exercise C in %d rotations\n", n, n, rotations);
	/*
	printf("Diagonalization of the same matrix with the method from exercise A was done in approximately 10 sweeps, so the (worst-case) number of Jacobi rotations (the number of sweeps times n(n-1)/2) was %d\n", 10*n*(n-1)/2);
	printf("In this case, with the method in exercise C, however, the (worst-case) number of Jacobi rotations (the number of sweeps times n) is %d\n", sweeps*n);
*/
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	return 0;
}
