------Exercise B------
A=
 0.8402	 0.3944	 0.7831	 0.7984	 0.9116	
 0.3944	 0.1976	 0.3352	 0.7682	 0.2778	
 0.7831	 0.3352	 0.5540	 0.4774	 0.6289	
 0.7984	 0.7682	 0.4774	 0.3648	 0.5134	
 0.9116	 0.2778	 0.6289	 0.5134	 0.9522	

Running Jacobi diagonalization on A first to find one eigenvalue, then on a fresh copy of A to find two eigenvalues and so on...
Number of rotations on 1 row(s) to find 1 eigenvalue(s)=108
	eigval #1=-0.5568
Number of rotations on 2 row(s) to find 2 eigenvalue(s)=286
	eigval #1=-0.5568
	eigval #2=-0.1235
Number of rotations on 3 row(s) to find 3 eigenvalue(s)=348
	eigval #1=-0.5568
	eigval #2=-0.1235
	eigval #3= 0.0692
Number of rotations on 4 row(s) to find 4 eigenvalue(s)=349
	eigval #1=-0.5568
	eigval #2=-0.1235
	eigval #3= 0.0692
	eigval #4= 0.4609
Number of rotations on 5 row(s) to find 5 eigenvalue(s)=349
	eigval #1=-0.5568
	eigval #2=-0.1235
	eigval #3= 0.0692
	eigval #4= 0.4609
	eigval #5= 3.0590
(Compare with outA1.txt to see that the eigenvalues are correct)
For comparison Jacobi diagonalization on the same matrix A with cyclic sweeps gives all eigenvalues in 39 rotations.

Going to higher n=30, we check if the first eigenvalue can be found in fewer rotations on the first-row-only sweeps compared to finding all eigenvalues in the cyclic sweeps. This should be the case for sufficiently large n, as the number of rotations in the first case is O(n), and in the second O(n²)...
Result for first-row-only sweeps on symmetric random 30x30 matrix: 1982
Result for cyclic sweeps on the same symmetric random 30x30 matrix: 2512
It seems that for sufficiently large symmetric matrices, if only the first (either lowest or highest; see below) eigenvalue is needed, then first-row-only sweeps are the way to go.


Finally, here follows the arguments which the exercise asks for...
One can imagine that at some point the Jacobi algorithm, with any type of sweeps, will happen upon Apq equal to or close to zero. This makes eq. (3.10) in the notes look like this:
tan(2*phi)=0/(Aqq - App).
The arctan function, which determines phi, will in this case return phi=0 if Aqq > App or phi=pi/2 if Aqq < App. The first case results in an identity operation on the elements App and Aqq, and the second case results in App and Aqq being interchanged (insert phi=0 or phi=pi/2 in eq. (3.6)).
Since App and Aqq are swapped if Aqq < App and, oppositely, are NOT swapped if Aqq > App, the smaller values on the diagonal tends towards the top of the matrix (the index q being larger than the index p) and the larger values tend towards the bottom of the matrix.
This scenario, with Apq practically equal to zero, is not what in reality moves the diagonal elements up and down. It is rather an effect which is accumulated through all of the Jacobi rotations carried out by the algorithm.

Changing the criterion
tan(2*phi)=2*Apq/(Aqq - App)
to the equivalent criterion
tan(2*phi)=-2*Apq/(App - Aqq)
gives the eigenvalues on the diagonal of D in descending order instead of ascending order, since the scenario described just above is then flipped upside down: If Aqq < App the two elements are not interchanged, and if Aqq > App the two elements ARE interchanged.
As an example, let us run the first-row-only sweeps on A from the beginning of this exercise with this modified criterion...
Number of rotations on 1 row(s) to find 1 eigenvalue(s)=24
	eigval #1= 3.0590
