#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include"jacobi.h"

/*
 * This function utilises the Jacobi eigenvalue algorithm. It performs row-by-row sweeps, 
 * terminating when no elements of the upper triangular part of the matrix A can be changed by
 * further Jacobi rotations. The number of rotations are returned as an integer. The implementation
 * is inspired by table 3.1 in the notes with occasional references to equation numbers in the
 * notes and with attempts at clarification. Only elements above the diagonal of the matrix A are
 * changed by the function. Therefore, the original symmetric matrix A can be recreated.
 */

int jacobi_cyclic_sweeps(gsl_matrix* A, gsl_vector* e, gsl_matrix* V) {
	int n = A->size1;
	/* 
	 * Diagonal elements ("initial guesses" for eigenvalues) are stored in a separate vector e. 
	 * These should be the actual eigenvalues when the function returns.
	 */
	for (int i = 0; i < n; i++) { 
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}
	gsl_matrix_set_identity(V); //corresponding "initial guesses" for eigenvectors
	
	double App, Aqq, Apq, Aip, Api, Aiq, Aqi, Vip, Viq;
	double App1, Aqq1; //1's denote primes in the language of eq. (3.9)
	double phi, c, s;
	int rotations = 0, changed = 1, p, q;
	while (changed == 1) { //has it been necessary to change any elements in A?
		changed = 0;
		for (p = 0; p < n; p++) {
			for (q = p + 1; q < n; q++) {
				/*
				 * Diagonal elements of A are stored in e, enabling restoration
				 * of the symmetric matrix A from the lower triangular part of
				 * A after jacobi_cyclic_sweeps returns.
				 */
				App = gsl_vector_get(e, p);
				Aqq = gsl_vector_get(e, q);
				Apq = gsl_matrix_get(A, p, q);
				phi = 0.5*atan2(2*Apq, Aqq - App); //eq. (3.10)
				c = cos(phi);
				s = sin(phi);
				App1 = c*c*App - 2*s*c*Apq + s*s*Aqq; //eq. (3.9)
				Aqq1 = s*s*App + 2*s*c*Apq + c*c*Aqq; //eq. (3.9)
				if (App != App1 || Aqq != Aqq1) {
					rotations++;
					changed = 1;
					gsl_vector_set(e, p, App1); //App1 -> App
					gsl_vector_set(e, q, Aqq1); //Aqq1 -> Aqq
					gsl_matrix_set(A, p, q, 0.0); //eq. (3.10)
					
					/*
					 * The following three for loops takes care of the second
					 * and third lines in eq. (3.9)
					 */
					for (int i = 0; i < p; i++) {
						Aip = gsl_matrix_get(A, i, p);
						Aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*Aip - s*Aiq);
						gsl_matrix_set(A, i, q, s*Aip + c*Aiq);
					}
					for (int i = p + 1; i < q; i++) {
						Api = gsl_matrix_get(A, p, i);
						Aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*Api - s*Aiq);
						gsl_matrix_set(A, i, q, s*Api + c*Aiq);
					}
					for (int i = q + 1; i < n; i++) {
						Api = gsl_matrix_get(A, p, i);
						Aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*Api - s*Aqi);
						gsl_matrix_set(A, q, i, s*Api + c*Aqi);
					}
					
					for (int i = 0; i < n; i++) {
						Vip = gsl_matrix_get(V, i, p);
						Viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*Vip - s*Viq); //eq. (3.11)
						gsl_matrix_set(V, i, q, s*Vip + c*Viq); //eq. (3.11)
					}
				}
			}
		}
	}

	return rotations;
}

/* 
 * Similar to the function above, but the argument num_eigvals now specifies how many eigenvalues
 * are desired. The algorithm then carries out sweeps only on the first num_eigvals rows in A.
 * Sufficient sweeps to determine the p'th < num_eigvals'th eigenvalue are carried out before 
 * sweeps are done on the p+1'th < num_eigvals'th row in A.
 * If the argument DESCENDING is equal to zero (=false), the eigenvalues are found in ascending 
 * order, otherwise they are found in descending order.
 * Repeats of long code comments, which can be found above, are omitted.
 */
int jacobi_eig_by_eig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int num_eigvals, int DESCENDING) {
	int n = A->size1;

	for (int i = 0; i < n; i++) { 
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
	}
	gsl_matrix_set_identity(V);
	
	double App, Aqq, Apq, Aip, Api, Aiq, Aqi, Vip, Viq;
	double App1, Aqq1;
	double phi, c, s;
	int rotations = 0, changed, p, q;
	for (p = 0; p < num_eigvals; p++) {
		changed = 1;
		while (changed == 1) {
			changed = 0;
			for (q = p + 1; q < n; q++) {
				App = gsl_vector_get(e, p);
				Aqq = gsl_vector_get(e, q);
				Apq = gsl_matrix_get(A, p, q);
				phi = (DESCENDING == 0)? 0.5*atan2(2*Apq, Aqq - App) 
					: 0.5*atan2(-2*Apq, App - Aqq); //eq. (3.10)
				c = cos(phi);
				s = sin(phi);
				App1 = c*c*App - 2*s*c*Apq + s*s*Aqq; //eq. (3.9)
				Aqq1 = s*s*App + 2*s*c*Apq + c*c*Aqq; //eq. (3.9)
				if (App != App1 || Aqq != Aqq1) {
					rotations++;
					changed = 1;
					gsl_vector_set(e, p, App1); //App1 -> App
					gsl_vector_set(e, q, Aqq1); //Aqq1 -> Aqq
					gsl_matrix_set(A, p, q, 0.0); //eq. (3.10)
					
					for (int i = 0; i < p; i++) {
						Aip = gsl_matrix_get(A, i, p);
						Aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, i, p, c*Aip - s*Aiq);
						gsl_matrix_set(A, i, q, s*Aip + c*Aiq);
					}
					for (int i = p + 1; i < q; i++) {
						Api = gsl_matrix_get(A, p, i);
						Aiq = gsl_matrix_get(A, i, q);
						gsl_matrix_set(A, p, i, c*Api - s*Aiq);
						gsl_matrix_set(A, i, q, s*Api + c*Aiq);
					}
					for (int i = q + 1; i < n; i++) {
						Api = gsl_matrix_get(A, p, i);
						Aqi = gsl_matrix_get(A, q, i);
						gsl_matrix_set(A, p, i, c*Api - s*Aqi);
						gsl_matrix_set(A, q, i, s*Api + c*Aqi);
					}
					
					for (int i = 0; i < n; i++) {
						Vip = gsl_matrix_get(V, i, p);
						Viq = gsl_matrix_get(V, i, q);
						gsl_matrix_set(V, i, p, c*Vip - s*Viq); //eq. (3.11)
						gsl_matrix_set(V, i, q, s*Vip + c*Viq); //eq. (3.11)
					}
				}
			}
		}
	}

	return rotations;
}

/*
 * Another take on the algorithm. We start by finding the indices of the (numerically) largest 
 * elements above the diagonal for each row and store it in an array. We then Jacobi rotate on 
 * each row p with index q from this array. The new largest elements of rows p and q are found 
 * right after each Jacobi rotation on a given row p. The convergence criterion is the same as
 * in the above functions.
 */
int jacobi_classic(gsl_matrix* A, gsl_vector* e, gsl_matrix* V) {
	int n = A->size1;

	int largest[n - 1]; //the final row has no off-diagonal elements which we can zero
	double current, absA;
	for (int i = 0; i < n; i++) { 
		gsl_vector_set(e, i, gsl_matrix_get(A, i, i));
		current = 0.0;
		//find index of (numerically) largest element above diagonal in row i
		for (int j = i + 1; j < n; j++) { 
			absA = fabs(gsl_matrix_get(A, i, j));
			if (absA > current) {
				current = absA;
				largest[i] = j;
			}
		}
	}
	gsl_matrix_set_identity(V);

	double App, Aqq, Apq, Aip, Api, Aiq, Aqi, Vip, Viq;
	double App1, Aqq1;
	double phi, c, s;
	int rotations = 0, changed = 1, p, q;
	while (changed == 1) {
		changed = 0;
		for (p = 0; p < n - 1; p++) {
			q = largest[p]; //Work on the largest element of the p'th row
			App = gsl_vector_get(e, p);
			Aqq = gsl_vector_get(e, q);
			Apq = gsl_matrix_get(A, p, q);
			phi = 0.5*atan2(2*Apq, Aqq - App); //eq. (3.10)
			c = cos(phi);
			s = sin(phi);
			App1 = c*c*App - 2*s*c*Apq + s*s*Aqq; //eq. (3.9)
			Aqq1 = s*s*App + 2*s*c*Apq + c*c*Aqq; //eq. (3.9)
			if (App != App1 || Aqq != Aqq1) {
				rotations++;
				changed = 1;
				gsl_vector_set(e, p, App1); //App1 -> App
				gsl_vector_set(e, q, Aqq1); // Aqq1 -> Aqq
				gsl_matrix_set(A, p, q, 0.0); //eq. (3.10)

				for (int i = 0; i < p; i++) {
					Aip = gsl_matrix_get(A, i, p);
					Aiq = gsl_matrix_get(A, i, q);
					gsl_matrix_set(A, i, p, c*Aip - s*Aiq);
					gsl_matrix_set(A, i, q, s*Aip + c*Aiq);
				}
				for (int i = p + 1; i < q; i++) {
					Api = gsl_matrix_get(A, p, i);
					Aiq = gsl_matrix_get(A, i, q);
					gsl_matrix_set(A, p, i, c*Api - s*Aiq);
					gsl_matrix_set(A, i, q, s*Api + c*Aiq);
				}
				for (int i = q + 1; i < n; i++) {
					Api = gsl_matrix_get(A, p, i);
					Aqi = gsl_matrix_get(A, q, i);
					gsl_matrix_set(A, p, i, c*Api - s*Aqi);
					gsl_matrix_set(A, q, i, s*Api + c*Aqi);
				}
				
				for (int i = 0; i < n; i++) {
					Vip = gsl_matrix_get(V, i, p);
					Viq = gsl_matrix_get(V, i, q);
					gsl_matrix_set(V, i, p, c*Vip - s*Viq); //eq. (3.11)
					gsl_matrix_set(V, i, q, s*Vip + c*Viq); //eq. (3.11)
				}
				//updating list of largest row elements in the following
				current = 0.0;
				//investigating p'th row of A
				for (int j = p + 1; j < n; j++) {
					absA = fabs(gsl_matrix_get(A, p, j));
					if (absA > current) {
						current = absA;
						largest[p] = j;
					}
				}
				current = 0.0;
				//investigating q'th row of A
				for (int j = q + 1; j < n; j++) {
					absA = fabs(gsl_matrix_get(A, q, j));
					if (absA > current) {
						current = absA;
						largest[q] = j;
					}
				}
				//investigating p'th column of A
				for (int j = 0; j < p; j++) {
					absA = fabs(gsl_matrix_get(A, j, p));
					current = fabs(gsl_matrix_get(A, j, largest[j]));
					if (absA > current) {
						largest[j] = p;
					}
				}
				//investigating first section of q'th column of A
				for (int j = 0; j < p; j++) {
					absA = fabs(gsl_matrix_get(A, j, q));
					current = fabs(gsl_matrix_get(A, j, largest[j]));
					if (absA > current) {
						largest[j] = q;
					}
				}
				//investigating second section of q'th column of A
				for (int j = p + 1; j < q; j++) {
					absA = fabs(gsl_matrix_get(A, j, q));
					current = fabs(gsl_matrix_get(A, j, largest[j]));
					if (absA > current) {
						largest[j] = q;
					}
				}
			}
		}
	}

	return rotations;
}
