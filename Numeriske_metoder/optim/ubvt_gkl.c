#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"ubvt_gkl.h"

/*
 * This function is based on the description of the Golub-Kahan-Lanczos Bidiagonalization 
 * Procedure in the following link: 
 * http://www.netlib.org/utk/people/JackDongarra/etemplates/node198.html
 * This procedure works for complex matrices as well, but the exercise asks for a solution
 * in the real case, so that is what is treated below.
 */
void ubvt_gkl_decomp(const gsl_matrix* A, gsl_matrix* B, gsl_matrix* U, gsl_matrix* V) {
	int n = A->size1;
	gsl_matrix_set_zero(B);

	gsl_vector_view uk, uk1, vk, vk1;
	double al, be;
	vk = gsl_matrix_column(V, 0);
	gsl_vector_set_basis(&vk.vector, 0); //1'st column of V is now the unit vector (1, 0, ...)
	for (int k = 0; k < n; k++) { //here follows line 2 to 9 of the algorithm in the link
		uk = gsl_matrix_column(U, k);
		vk = gsl_matrix_column(V, k);
		gsl_blas_dgemv(CblasNoTrans, 1.0, A, &vk.vector, 0.0, &uk.vector); //uk = A*vk
		if (k != 0) {
			uk1 = gsl_matrix_column(U, k - 1);
			gsl_blas_daxpy(-gsl_matrix_get(B, k - 1, k), 
					&uk1.vector, &uk.vector); //uk -= beta[k-1]*u[k-1]
		}
		al = gsl_blas_dnrm2(&uk.vector);
		gsl_matrix_set(B, k, k, al);
		gsl_vector_scale(&uk.vector, 1/al); //uk = uk/al
		if (k != n - 1) {
			vk1 = gsl_matrix_column(V, k + 1);
			gsl_blas_dgemv(CblasTrans, 1.0, A, &uk.vector, 
					0.0, &vk1.vector); //vk1 = (A^T)*uk
			gsl_blas_daxpy(-al, &vk.vector, &vk1.vector); //vk1 = vk1 - al*vk
			be = gsl_blas_dnrm2(&vk1.vector);
			gsl_matrix_set(B, k, k + 1, be);
			gsl_vector_scale(&vk1.vector, 1/be); //vk1 = vk1/be
		}
	}
}

void ubvt_gkl_solve(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, const gsl_vector* b, gsl_vector* x) {
        int n = B->size1;

        //apply (U^T) to b, saving the result in x
        gsl_blas_dgemv(CblasTrans, 1.0, U, b, 0.0, x);

	gsl_vector* y = gsl_vector_alloc(n);
        //in-place back-substitution, solving B*y=(U^T)*b with y=(V^T)*x and (U^T)*b already in x
        double yi, yi1, UTbi, ai, bi;
        for (int i = n - 1; i >= 0; i--) {
		UTbi = gsl_vector_get(x, i);
		ai = gsl_matrix_get(B, i, i);
		if (i == n - 1) {
			yi = UTbi/ai;
			gsl_vector_set(y, i, yi);
		} else {
			bi = gsl_matrix_get(B, i, i + 1);
			yi1 = gsl_vector_get(y, i + 1);
			yi = (UTbi - bi*yi1)/ai;
			gsl_vector_set(y, i, yi);
		}
        }

	//apply V to y (solving y = (V^T)*x), yielding the final result x
	gsl_blas_dgemv(CblasNoTrans, 1.0, V, y, 0.0, x);
	
	gsl_vector_free(y);
}

/*
 * This function solves A*x=U*B*(V^T)*x=b as x=V*(B^-1)*(U^T)*b. See own handwritten notes 
 * for justification of algorithm for B^-1.
 */
void ubvt_gkl_solve_via_inverse_of_B(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, const gsl_vector* b, gsl_vector* x) {
	int n = B->size1;
	gsl_matrix* Binv = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(Binv, B); //do not destroy B; makes ubvt_gkl_inverse easier to implement

        //apply (U^T) to b, saving the result in x
        gsl_blas_dgemv(CblasTrans, 1.0, U, b, 0.0, x);

        //determining B^-1 from B
        double c;
        for (int i = n - 1; i >= 0; i--) {
		c = gsl_matrix_get(Binv, i, i);
		gsl_matrix_set(Binv, i, i, 1/c);
                for (int j = i + 1; j < n; j++) {
			if (gsl_matrix_get(Binv, i, j) != 0.0) {
				c = gsl_matrix_get(Binv, i, j)*gsl_matrix_get(Binv, i, i)
					*gsl_matrix_get(Binv, j, j);
				gsl_matrix_set(Binv, i, j, -c);
			} else {
				c = gsl_matrix_get(Binv, i, j - 1)*gsl_matrix_get(Binv, i + 1, j)
					/gsl_matrix_get(Binv, i + 1, j - 1);
				gsl_matrix_set(Binv, i, j, c);
			}
                }
        }

	//apply B^-1 to (U^T)*b (which is stored in x), saving the result in v
	gsl_vector* v = gsl_vector_alloc(n); //dummy vector for further matrix-vector multipl.
	gsl_blas_dgemv(CblasNoTrans, 1.0, Binv, x, 0.0, v);

	//finally, apply V to (B^-1)*(U^T)*b (which is stored in v), saving the result in x
	gsl_blas_dgemv(CblasNoTrans, 1.0, V, v, 0.0, x);
	gsl_vector_free(v);
	gsl_matrix_free(Binv);
}

void ubvt_gkl_inverse(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, gsl_matrix* C) {
	int n = B->size1;
	gsl_matrix_set_identity(C);
	
	gsl_vector_view e;
	gsl_vector* x = gsl_vector_alloc(n); //dummy vector for the solver to work on
	for (int i = 0; i < n; i++) { //solving Ax[i]=U*B*(V^T)*x[i]=e[i]; eq. (2.44)
		e = gsl_matrix_column(C, i);
		ubvt_gkl_solve(B, U, V, &e.vector, x);
		gsl_matrix_set_col(C, i, x);
	}

	gsl_vector_free(x);
}

double ubvt_gkl_determinant(const gsl_matrix* A) {
	int n = A->size1;
	gsl_matrix* B = gsl_matrix_alloc(n, n);
	gsl_matrix* U = gsl_matrix_alloc(n, n);
	gsl_matrix* V = gsl_matrix_alloc(n, n);

	ubvt_gkl_decomp(A, B, U, V);

	double result = 1.0;
	for (int i = 0; i < n; i++) { //B is upper triangular, so det. is product of diag. elem.
		result *= gsl_matrix_get(B, i, i);
	}

	gsl_matrix_free(B);
	gsl_matrix_free(U);
	gsl_matrix_free(V);
	return result;
}
