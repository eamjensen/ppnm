#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<math.h>

/*
 * Returns index of highest point (point with largest value returned by the function fun) in 
 * simplex S.
 */

int find_highest_point(const gsl_matrix* S, double fun(const gsl_vector* x)) {
	int m = S->size2;
	double current = -INFINITY, f;
	int index;
	for (int i = 0; i < m; i++) {
		gsl_vector_const_view p = gsl_matrix_const_column(S, i);
		f = fun(&p.vector);
		if (f > current) {
			current = f;
			index = i;
		}
	}
	return index;
}

/*
 * Returns index of lowest point (point with smallest value returned by the function fun) in 
 * simplex S.
 */
int find_lowest_point(const gsl_matrix* S, double fun(const gsl_vector* x)) {
	int m = S->size2;
	double current = INFINITY, f;
	int index;
	for (int i = 0; i < m; i++) {
		gsl_vector_const_view p = gsl_matrix_const_column(S, i);
		f = fun(&p.vector);
		if (f < current) {
			current = f;
			index = i;
		}
	}
	return index;
}

/*
 * Calculates the centroid given the simplex S and the index of the highest point within S. 
 * The centroid is put into the gsl_vector pce.
 */
void get_centroid(gsl_vector* pce, const gsl_matrix* S, int index_of_highest_point) {
	int n = S->size1;
	int m = S->size2;
	gsl_vector_set_zero(pce);
	for (int i = 0; i < index_of_highest_point; i++) {
		gsl_vector_const_view p = gsl_matrix_const_column(S, i);
		gsl_vector_add(pce, &p.vector);
	}
	for (int i = index_of_highest_point + 1; i < m; i++) {
		gsl_vector_const_view p = gsl_matrix_const_column(S, i);
		gsl_vector_add(pce, &p.vector);
	}
	gsl_vector_scale(pce, 1.0/n);
}

/*
 * Reflects the point phi against the point pce.
 */
void reflection(const gsl_vector* pce, gsl_vector* phi) {
	gsl_vector_scale(phi, -1.0);
	gsl_blas_daxpy(2.0, pce, phi);
}

/*
 * Expands the point phi around the point pce.
 */
void expansion(const gsl_vector* pce, gsl_vector* phi) {
	gsl_vector_scale(phi, -2.0);
	gsl_blas_daxpy(3.0, pce, phi);
}

/*
 * Contracts the point phi relative to the point pce.
 */
void contraction(const gsl_vector* pce, gsl_vector* phi) {
	gsl_vector_scale(phi, 0.5);
	gsl_blas_daxpy(0.5, pce, phi);
}

/*
 * Reduces all points in the simplex S except for the lowest point with index index_of_lowest_point.
 */
void reduction(gsl_matrix* S, int index_of_lowest_point) {
	int m = S->size2;
	gsl_vector_view p;
	gsl_vector_const_view plo = gsl_matrix_const_column(S, index_of_lowest_point);
	for (int k = 0; k < index_of_lowest_point; k++) {
		p = gsl_matrix_column(S, k);
		gsl_vector_scale(&p.vector, 0.5);
		gsl_blas_daxpy(0.5, &plo.vector, &p.vector);
	}
	for (int k = index_of_lowest_point + 1; k < m; k++) {
		p = gsl_matrix_column(S, k);
		gsl_vector_scale(&p.vector, 0.5);
		gsl_blas_daxpy(0.5, &plo.vector, &p.vector);
	}
}

/*
 * Returns the size of the simplex S, which is defined as the largest of the distances between 
 * the first point in S and all other points in S.
 */
double simplex_size(const gsl_matrix* S) {
	int n = S->size1;
	int m = S->size2;
	gsl_vector_const_view p0 = gsl_matrix_const_column(S, 0);
	gsl_vector* p = gsl_vector_alloc(n);
	double result = 0.0, dist;
	for (int i = 1; i < m; i++) {
		gsl_matrix_get_col(p, S, i);
		gsl_blas_daxpy(-1.0, &p0.vector, p);
		dist = gsl_blas_dnrm2(p);
		if (dist > result) {
			result = dist;
		}
	}
	gsl_vector_free(p);
	return result;
}

void simplex_print(const gsl_matrix* S) {
	int n = S->size1;
	int m = S->size2;
	for (int j = 0; j < m; j++) {
		printf("p%d=(", j);
		for (int i = 0; i < n - 1; i++) {
			printf("% 1.4f, ", gsl_matrix_get(S, i, j));
		}
		printf("% 1.4f),   ", gsl_matrix_get(S, n - 1, j));
	}
	printf("\n");
}

/*
 * Translation of pseudo-code on page 52 into C code. Returns the number of steps.
 */
int downhill_simplex(double fun(const gsl_vector* x), gsl_matrix* S, double epsilon) {
	int n = S->size1;

	int refs = 0, exps = 0, cons = 0, reds = 0;
	int hi, lo;
	gsl_vector_view phi, plo;
	gsl_vector* pce = gsl_vector_alloc(n);
	gsl_vector* pref = gsl_vector_alloc(n);
	gsl_vector* pexp = gsl_vector_alloc(n);
	gsl_vector* pcon = gsl_vector_alloc(n);
	while (simplex_size(S) > epsilon) {
		hi = find_highest_point(S, fun);
		lo = find_lowest_point(S, fun);
		phi = gsl_matrix_column(S, hi);
		plo = gsl_matrix_column(S, lo);
		get_centroid(pce, S, hi);
		gsl_vector_memcpy(pref, &phi.vector); //copy phi into pref
		reflection(pce, pref);
		if (fun(pref) < fun(&plo.vector)) {
			gsl_vector_memcpy(pexp, &phi.vector); //copy phi into pexp
			expansion(pce, pexp);
			if (fun(pexp) < fun(pref)) {
				gsl_vector_memcpy(&phi.vector, pexp); //accept expansion
				exps++;
			} else {
				gsl_vector_memcpy(&phi.vector, pref); //accept reflection
				refs++;
			}
		} else {
			if (fun(pref) < fun(&phi.vector)) {
				gsl_vector_memcpy(&phi.vector, pref); //accept reflection
				refs++;
			} else {
				gsl_vector_memcpy(pcon, &phi.vector); //copy phi into pcon
				contraction(pce, pcon);
				if (fun(pcon) < fun(&phi.vector)) {
					gsl_vector_memcpy(&phi.vector, pcon); //accept contraction
					cons++;
				} else {
					reduction(S, lo);
					reds++;
				}
			}
		}
	}
	
	gsl_vector_free(pce); gsl_vector_free(pref); gsl_vector_free(pexp); gsl_vector_free(pcon);
	fprintf(stderr, "reflections=%d, expansions=%d, contractions=%d, reductions=%d\n", refs, exps, cons, reds);
	int steps = refs + exps + cons + reds;
	return steps;
}
