#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef SIMPLEX_H
#define SIMPLEX_H
int find_highest_point(const gsl_matrix* S, double fun(const gsl_vector* x));
int find_lowest_point(const gsl_matrix* S, double fun(const gsl_vector* x));
void get_centroid(gsl_vector* pce, const gsl_matrix* S, int index_of_highest_point);
void reflection(const gsl_vector* pce, gsl_vector* phi);
void expansion(const gsl_vector* pce, gsl_vector* phi);
void contraction(const gsl_vector* pce, gsl_vector* phi);
void reduction(gsl_matrix* S, int index_of_lowest_point);
double simplex_size(const gsl_matrix* S);
void simplex_print(const gsl_matrix* S);
int downhill_simplex(double fun(const gsl_vector* x), gsl_matrix* S, double epsilon);
#endif
