#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef UBVT_GKL_H
#define UBVT_GKL_H
void ubvt_gkl_decomp(const gsl_matrix* A, gsl_matrix* B, gsl_matrix* U, gsl_matrix* V);
void ubvt_gkl_solve(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, const gsl_vector* b, gsl_vector* x);
void ubvt_gkl_solve_via_inverse_of_B(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, const gsl_vector* b, gsl_vector* x);
void ubvt_gkl_inverse(const gsl_matrix* B, const gsl_matrix* U, const gsl_matrix* V, gsl_matrix* C);
double ubvt_gkl_determinant(const gsl_matrix* A);
#endif
