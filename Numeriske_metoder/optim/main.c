#include<assert.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include"newton.h"
#include"matvec.h"
#include"simplex.h"

double rosenbrock(const gsl_vector* x, gsl_vector* df, gsl_matrix* H) { //see problem description
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(df, 0, -2*(1-x0) - 400*x0*(x1-x0*x0));
	gsl_vector_set(df, 1, 200*(x1-x0*x0));
	gsl_matrix_set(H, 0, 0, 2 - 400*x1 + 1200*x0*x0);
	gsl_matrix_set(H, 0, 1, -400*x0);
	gsl_matrix_set(H, 1, 0, -400*x0);
	gsl_matrix_set(H, 1, 1, 200.0);
	return (1-x0)*(1-x0) + 100*(x1-x0*x0)*(x1-x0*x0);
}

double rosenbrock_no_H(const gsl_vector* x, gsl_vector* df) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(df, 0, -2*(1-x0) - 400*x0*(x1-x0*x0));
	gsl_vector_set(df, 1, 200*(x1-x0*x0));
	return (1-x0)*(1-x0) + 100*(x1-x0*x0)*(x1-x0*x0);
}

double rosenbrock_pure(const gsl_vector* x) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	return (1-x0)*(1-x0) + 100*(x1-x0*x0)*(x1-x0*x0);
}

double himmelblau(const gsl_vector* x, gsl_vector* df, gsl_matrix* H) { //see problem description
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(df, 0, 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7));
	gsl_vector_set(df, 1, 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7));
	gsl_matrix_set(H, 0, 0, 12*x0*x0 + 4*x1 - 42);
	gsl_matrix_set(H, 0, 1, 4*x0 + 4*x1);
	gsl_matrix_set(H, 1, 0, 4*x0 + 4*x1);
	gsl_matrix_set(H, 1, 1, 4*x0 + 12*x1*x1 - 26);
	return (x0*x0+x1-11)*(x0*x0+x1-11) + (x0+x1*x1-7)*(x0+x1*x1-7);
}

double himmelblau_no_H(const gsl_vector* x, gsl_vector* df) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	gsl_vector_set(df, 0, 4*x0*(x0*x0+x1-11) + 2*(x0+x1*x1-7));
	gsl_vector_set(df, 1, 2*(x0*x0+x1-11) + 4*x1*(x0+x1*x1-7));
	return (x0*x0+x1-11)*(x0*x0+x1-11) + (x0+x1*x1-7)*(x0+x1*x1-7);
}

double himmelblau_pure(const gsl_vector* x) {
	assert(x->size == 2);
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	return (x0*x0+x1-11)*(x0*x0+x1-11) + (x0+x1*x1-7)*(x0+x1*x1-7);
}

double activity_fit(const gsl_vector* x) { //see problem description
	assert(x->size == 3);
	double A = gsl_vector_get(x, 0);
	double T = gsl_vector_get(x, 1);
	double B = gsl_vector_get(x, 2);
	double t[] = {0.23, 1.29, 2.35, 3.41, 4.47, 5.53, 6.59, 7.65, 8.71, 9.77};
        double y[] = {4.64, 3.38, 3.01, 2.55, 2.29, 1.67, 1.59, 1.69, 1.38, 1.46};
        double e[] = {0.42, 0.37, 0.34, 0.31, 0.29, 0.27, 0.26, 0.25, 0.24, 0.24};
	int N = sizeof(t)/sizeof(t[0]);
	double sum = 0, f;
	for (int i = 0; i < N; i++) {
		f = A*exp(-(t[i])/T) + B;
		sum += pow((f - y[i])/e[i], 2);
	}
	return sum;
}

int main() {
	int n = 2;
	double tolerance = 1e-6;
	int steps = 0;
	gsl_vector* x = gsl_vector_alloc(n);
	printf("The tolerances of the approximations to all the minima in this exercise are %g.\n", tolerance);




	printf("\n------Exercise A------\n");
	printf("Finding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nwith Newton's method...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton(rosenbrock, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);

	printf("\nFinding a minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nwith Newton's method...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton(himmelblau, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);
	
	
	
	
	printf("\n------Exercise B------\n");
	printf("Finding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nwith Quasi-Newton method (Broyden's update) and with analytic gradient...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton_broyden_with_gradient(rosenbrock_no_H, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);
	printf("Doing the same with Quasi-Newton method (Broyden's update) but without analytic gradient...\n");
	gsl_vector_set(x, 0, 1.7);
	gsl_vector_set(x, 1, 3.3);
	steps = newton_broyden_without_gradient(rosenbrock_pure, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);

	printf("\nFinding a minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nwith Quasi-Newtons method (Broyden's update) and with analytic gradient...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	vector_print("Initial guess, (x, y)=", x, n, 0);
	steps = newton_broyden_with_gradient(himmelblau_no_H, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);
	printf("Doing the same with Quasi-Newton method (Broyden's update) but without analytic gradient...\n");
	gsl_vector_set(x, 0, 4.5);
	gsl_vector_set(x, 1, -2.8);
	steps = newton_broyden_without_gradient(himmelblau_pure, x, tolerance);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);

	int m = 3;
	gsl_vector* y = gsl_vector_alloc(m);
	printf("\nFitting the function\nf(t)=A*exp(-t/T)+B\nto the given data\nusing Broyden's update without an analytic gradient...\n");
	gsl_vector_set(y, 0, 4.0);
	gsl_vector_set(y, 1, 2.0);
	gsl_vector_set(y, 2, 1.0);
	vector_print("Initial guess, (A, T, B)=", y, m, 0);
	steps = newton_broyden_without_gradient(activity_fit, y, tolerance);
	vector_print("Minimum found at (A, T, B)=", y, m, 0);
	printf("in %d steps\n", steps);
	char* filename = "fitfunction.txt";
	printf("Printing information about the minimum to a separate file %s for gnuplot to use\n", filename);
	FILE* f = fopen(filename, "w");
	double A = gsl_vector_get(y, 0);
	double T = gsl_vector_get(y, 1);
	double B = gsl_vector_get(y, 2);
	fprintf(f, "f(x) = %g*exp(-x/%g)+%g\nfittitle = 'Broyden fit: f(t) = %1.3f*exp(-t/%1.3f)+%1.3f'\n", A, T, B, A, T, B);




	printf("\n------Exercise C------\n");
	gsl_matrix* S = gsl_matrix_alloc(n, m);
	printf("Finding the minimum of the Rosenbrock's valley function,\nf(x,y) = (1-x²) + 100(y-x²)²,\nwith the downhill simplex method...\n");
	gsl_matrix_set(S, 0, 0, 0.0);
	gsl_matrix_set(S, 1, 0, 0.0);
	gsl_matrix_set(S, 0, 1, 0.0);
	gsl_matrix_set(S, 1, 1, 3.0);
	gsl_matrix_set(S, 0, 2, 2.0);
	gsl_matrix_set(S, 1, 2, -1.0);
	matrix_print("Initial simplex (each column represents a point p):", S, n, m);
	steps = downhill_simplex(rosenbrock_pure, S, tolerance);
	gsl_matrix_get_col(x, S, 0);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);

	printf("\nFinding the minimum of the Himmelblau's function,\nf(x,y) = (x²+y-11)² + (x+y²-7)²,\nwith the downhill simplex method...\n");
	gsl_matrix_set(S, 0, 0, 2.0);
	gsl_matrix_set(S, 1, 0, 0.0);
	gsl_matrix_set(S, 0, 1, 6.0);
	gsl_matrix_set(S, 1, 1, 0.0);
	gsl_matrix_set(S, 0, 2, -6.0);
	gsl_matrix_set(S, 1, 2, 3.0);
	matrix_print("Initial simplex (each column represents a point p):", S, n, m);
	steps = downhill_simplex(himmelblau_pure, S, tolerance);
	gsl_matrix_get_col(x, S, 0);
	vector_print("Minimum found at (x, y)=", x, n, 0);
	printf("in %d steps\n", steps);

	gsl_matrix* SS = gsl_matrix_alloc(m, m + 1);
	printf("\nFitting the function\nf(t)=A*exp(-t/T)+B\nto the given data\nusing the downhill simplex method...\n");
	gsl_matrix_set(SS, 0, 0, 3.0);
	gsl_matrix_set(SS, 1, 0, 2.5);
	gsl_matrix_set(SS, 2, 0, 2.0);
	gsl_matrix_set(SS, 0, 1, 4.0);
	gsl_matrix_set(SS, 1, 1, 3.5);
	gsl_matrix_set(SS, 2, 1, 2.0);
	gsl_matrix_set(SS, 0, 2, 3.0);
	gsl_matrix_set(SS, 1, 2, 2.5);
	gsl_matrix_set(SS, 2, 2, 0.5);
	gsl_matrix_set(SS, 0, 3, 4.0);
	gsl_matrix_set(SS, 1, 3, 3.5);
	gsl_matrix_set(SS, 2, 3, 0.5);
	matrix_print("Initial simplex (each column represents a point p):", SS, m, m + 1);
	steps = downhill_simplex(activity_fit, SS, tolerance);
	gsl_matrix_get_col(y, SS, 1);
	vector_print("Minimum found at (A, T, B)=", y, m, 0);
	printf("in %d steps\n", steps);
	printf("Printing information about this different minimum to the same separate file %s for gnuplot to use\n", filename);
	A = gsl_vector_get(y, 0);
	T = gsl_vector_get(y, 1);
	B = gsl_vector_get(y, 2);
	fprintf(f, "g(x) = %g*exp(-x/%g)+%g\nfittitle2 = 'Simplex fit: g(t) = %1.3f*exp(-t/%1.3f)+%1.3f'\n", A, T, B, A, T, B);
	fclose(f);
	printf("Please see the file activity.svg for a plot of the data along with both the Broyden fit and the Simplex fit\n");




	gsl_matrix_free(S);
	gsl_matrix_free(SS);
	gsl_vector_free(x);
	gsl_vector_free(y);
	return 0;
}
