#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<float.h>
#include"ubvt_gkl.h"
#define LAMBDA_MIN 1.0/64
#define ALPHA 1e-4
#define EPS 1e-6

/*
 * Function from exercise 'roots' repurposed for this exercise. Number of steps is returned. 
 * A bit fewer calculations could be made in the implementation, but for transparency's sake 
 * (code more easily comparable to notes) the calculations are made anyway. The same goes for the 
 * other functions in this file.
 */
int newton(double fun(const gsl_vector* x, gsl_vector* df, gsl_matrix* H), gsl_vector* x, double epsilon) {
	int n = x->size;

	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n, n);
	double fx = fun(x, df, H); //we now have our initial df (gradient) and H of eq. (6.5)
	
	gsl_matrix *B, *U, *V; //used to solve H*Dx=-df; eq. (6.5)
	gsl_vector* Dx; //same as above
	B = gsl_matrix_alloc(n, n);
	U = gsl_matrix_alloc(n, n);
	V = gsl_matrix_alloc(n, n);
	Dx = gsl_vector_alloc(n);

	double lambda, ftemp, dotprod;
	int steps = 0;
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* dftemp = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(df) > epsilon) {
		gsl_vector_scale(df, -1.0); //df = -df
		ubvt_gkl_decomp(H, B, U, V);
		ubvt_gkl_solve(B, U, V, df, Dx); //solving eq. (6.5)
		gsl_vector_scale(df, -1.0); //df = df again
		lambda = 1.0;
		gsl_vector_set_zero(s);
		gsl_blas_daxpy(lambda, Dx, s); //s = lambda*Dx; eq. (6.8)
		gsl_vector_memcpy(xtemp, x); //copy x into xtemp
		gsl_blas_daxpy(lambda, Dx, xtemp); //xtemp = xtemp + lambda*Dx = x + lambda*Dx
		ftemp = fun(xtemp, dftemp, H); //ftemp <- f(xtemp) = f(x + lambda*Dx)
		gsl_blas_ddot(s, df, &dotprod); //dotprod <- (Dx^T)*df = ((lambda*Dx)^T)*df
		while (ftemp > fx + ALPHA*dotprod && lambda > LAMBDA_MIN) { //eq. (6.9)
			lambda /= 2;
			gsl_blas_daxpy(-lambda, Dx, s); //s -= lambda*Dx = lambda*Dx
			gsl_blas_ddot(s, df, &dotprod);
			gsl_blas_daxpy(-lambda, Dx, xtemp); //xtemp -= lambda*Dx = x + lambda*Dx
			ftemp = fun(xtemp, dftemp, H);
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		gsl_vector_memcpy(df, dftemp); //copy dftemp into df
		fx = ftemp; //ready to solve eq. (6.5) again
		steps++;
	}

	gsl_matrix_free(H); gsl_matrix_free(B); gsl_matrix_free(U); gsl_matrix_free(V);
	gsl_vector_free(df); gsl_vector_free(Dx); gsl_vector_free(xtemp); gsl_vector_free(s); gsl_vector_free(dftemp);
	return steps;
}

/*
 * Newton's method again but with symmetric Broyden's update. The analytic expression for the 
 * Hessian matrix H thus does not need to be specified, but the gradient df of the function fun 
 * still does.
 */
int newton_broyden_with_gradient(double fun(const gsl_vector* x, gsl_vector* df), gsl_vector* x, double epsilon) {
	int n = x->size;

	gsl_vector* df = gsl_vector_alloc(n);
	double fx = fun(x, df); //initial df (gradient) of eq. (6.5)
	gsl_matrix* B = gsl_matrix_alloc(n, n);
	gsl_matrix_set_identity(B); //initial B=H^-1; just below eq. (6.12)

	gsl_vector* Dx = gsl_vector_alloc(n);
	double lambda, ftemp, sTdf, uTy, sTy, gamma;
	int steps = 0, warnings = 0;
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* dftemp = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* u = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(df) > epsilon) {
		gsl_blas_dgemv(CblasNoTrans, -1.0, B, df, 0.0, Dx); //Dx = -B*df; eq. (6.6)
		lambda = 1.0;
		gsl_vector_set_zero(s);
		gsl_blas_daxpy(lambda, Dx, s); //s = lambda*Dx; eq. (6.8)
		gsl_vector_memcpy(xtemp, x); //copy x into xtemp
		gsl_blas_daxpy(lambda, Dx, xtemp); //xtemp = xtemp + lambda*Dx = x + lambda*Dx
		ftemp = fun(xtemp, dftemp);
		gsl_blas_ddot(s, df, &sTdf); //sTdf <- (s^T)*df; eq. (6.9)
		while (ftemp > fx + ALPHA*sTdf && lambda > LAMBDA_MIN) { //eq. (6.9)
			lambda /= 2;
			gsl_blas_daxpy(-lambda, Dx, s); //s -= lambda*Dx = lambda*Dx
			gsl_blas_ddot(s, df, &sTdf);
			gsl_blas_daxpy(-lambda, Dx, xtemp); //xtemp -= lambda*Dx = x + lambda*Dx
			ftemp = fun(xtemp, dftemp);
			if (lambda == LAMBDA_MIN) {
				warnings++;
			}
		}
		gsl_vector_memcpy(y, dftemp); //copy dftemp into y
		gsl_blas_daxpy(-1.0, df, y); //y -= df = dftemp - df; just below eq. (6.12)
		gsl_blas_ddot(s, y, &sTy); //sTy <- (s^T)*y; eq. (6.16)
		if (warnings < 2 && fabs(sTy) > EPS)  {
			gsl_blas_dgemv(CblasNoTrans, -1.0, B, y, 0.0, u); //u = -B*y
			gsl_blas_daxpy(1.0, s, u); //u += s = s - B*y; just below eq. (6.12)
			gsl_blas_ddot(u, y, &uTy); //uTy <- (u^T)*y; just below eq. (6.16)
			gamma = uTy/(2*sTy);
			gsl_blas_daxpy(-gamma, s, u); //u -= gamma*s = u - gamma*s
			gsl_blas_dger(1.0/sTy, u, s, B); //eq. (6.15)
			gsl_blas_dger(1.0/sTy, s, u, B); //eq. (6.15)
		} else { //update diverged. resetting B to identity matrix
			fprintf(stderr, "update diverged during step %d\n", steps + 1);
			gsl_matrix_set_identity(B);
			warnings = 0;
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		gsl_vector_memcpy(df, dftemp); //copy dftemp into df
		fx = ftemp; //ready to calculate eq. (6.5) again
		steps++;
	}


	gsl_matrix_free(B);
	gsl_vector_free(df); gsl_vector_free(Dx); gsl_vector_free(s); gsl_vector_free(xtemp); gsl_vector_free(dftemp); gsl_vector_free(y); gsl_vector_free(u);
	return steps;
}

/*
 * Approximation to the gradient of the function fun, given the "current" vector x and the gradient 
 * to be calculated df.
 */
void finite_difference_gradient(double fun(const gsl_vector* x), const gsl_vector* x, gsl_vector* df) {
	int n = x->size;
	double fx = fun(x);

	gsl_vector* y = gsl_vector_alloc(n);
	double fy, dx, xi;
	for (int i = 0; i < n; i++) {
		xi = gsl_vector_get(x, i);
		if (fabs(xi) > sqrt(DBL_EPSILON)) {
			//choose step-size in the same way as gsl_multiroot_fsolver_dnewton
			dx = sqrt(DBL_EPSILON)*fabs(xi);
		} else {
			//unless xi is very tiny
			dx = DBL_EPSILON;
		}
		gsl_vector_set_basis(y, i); //i'th row = 1; all others = 0
		gsl_vector_scale(y, dx); //i'th row = dx
		gsl_blas_daxpy(1.0, x, y); //y += x
		fy = fun(y);
		gsl_vector_set(df, i, (fy - fx)/dx);
	}

	gsl_vector_free(y);
}

/*
 * Newton's method with symmetric Broyden's update. Neither the analytic expression for the 
 * Hessian matrix H nor the analytic expression for the gradient of the function fun needs to 
 * be specified.
 */
int newton_broyden_without_gradient(double fun(const gsl_vector* x), gsl_vector* x, double epsilon) {
	int n = x->size;

	double fx = fun(x);
	gsl_vector* df = gsl_vector_alloc(n);
	finite_difference_gradient(fun, x, df);
	gsl_matrix* B = gsl_matrix_alloc(n, n);
	gsl_matrix_set_identity(B);

	gsl_vector* Dx = gsl_vector_alloc(n);
	double lambda, ftemp, sTdf, uTy, sTy, gamma;
	int steps = 0, warnings = 0;
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* xtemp = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* u = gsl_vector_alloc(n);
	while (gsl_blas_dnrm2(df) > epsilon) {
		gsl_blas_dgemv(CblasNoTrans, -1.0, B, df, 0.0, Dx); //Dx = -B*df; eq. (6.6)
		lambda = 1.0;
		gsl_vector_set_zero(s);
		gsl_blas_daxpy(lambda, Dx, s); //s = lambda*Dx; eq. (6.8)
		gsl_vector_memcpy(xtemp, x); //copy x into xtemp
		gsl_blas_daxpy(lambda, Dx, xtemp); //xtemp = xtemp + lambda*Dx = x + lambda*Dx
		ftemp = fun(xtemp);
		gsl_blas_ddot(s, df, &sTdf); //sTdf <- (s^T)*df; eq. (6.9)
		while (ftemp > fx + ALPHA*sTdf && lambda > LAMBDA_MIN) { //eq. (6.9)
			lambda /= 2;
			gsl_blas_daxpy(-lambda, Dx, s); //s -= lambda*Dx = lambda*Dx
			gsl_blas_ddot(s, df, &sTdf);
			gsl_blas_daxpy(-lambda, Dx, xtemp); //xtemp -= lambda*Dx = x + lambda*Dx
			ftemp = fun(xtemp);
			if (lambda == LAMBDA_MIN) {
				warnings++;
			}
		}
		finite_difference_gradient(fun, xtemp, y); //y <- grad(x+s)
		gsl_blas_daxpy(-1.0, df, y); //y -= df = grad(x+s) - grad(x); just below eq. (6.12)
		gsl_blas_ddot(s, y, &sTy); //sTy <- (s^T)*y; eq. (6.16)
		if (warnings < 2 && fabs(sTy) > EPS)  {
			gsl_blas_dgemv(CblasNoTrans, -1.0, B, y, 0.0, u); //u = -B*y
			gsl_blas_daxpy(1.0, s, u); //u += s = s - B*y; just below eq. (6.12)
			gsl_blas_ddot(u, y, &uTy); //uTy <- (u^T)*y; just below eq. (6.16)
			gamma = uTy/(2*sTy); //eq. (6.16)
			gsl_blas_daxpy(-gamma, s, u); //u -= gamma*s = u - gamma*s
			gsl_blas_dger(1.0/sTy, u, s, B); //eq. (6.15)
			gsl_blas_dger(1.0/sTy, s, u, B); //eq. (6.15)
		} else { //update diverged. resetting B to identity matrix
			fprintf(stderr, "update diverged during step %d\n", steps + 1);
			gsl_matrix_set_identity(B);
			warnings = 0;
		}
		gsl_vector_memcpy(x, xtemp); //copy xtemp into x
		finite_difference_gradient(fun, x, df); //df <- grad({new x})
		fx = ftemp; //ready to calculate eq. (6.5) again
		steps++;
	}


	gsl_matrix_free(B);
	gsl_vector_free(df); gsl_vector_free(Dx); gsl_vector_free(s); gsl_vector_free(xtemp); gsl_vector_free(y); gsl_vector_free(u);
	return steps;
}
