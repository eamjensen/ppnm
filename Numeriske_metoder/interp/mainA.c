#include<stdio.h>
#include<math.h>
#include"linterp.h"

void generate_sine_values(int n, double x[], double y[]) {
	double dx = 2*M_PI/(n - 1);
	FILE* f = fopen("sine.dat", "w");
	fprintf(f, "x\ty\n");
	for (int i = 0; i < n; i++) {
		x[i] = i*dx;
		y[i] = sin(x[i]);
		fprintf(f, "%g\t%g\n", x[i], y[i]);
	}
	fclose(f);
}

int main() {
	int n = 9;
	double x[n], y[n];
	generate_sine_values(n, x, y);

	int mult = 5;
	int numpoints = mult*n;
	double z = 0.0;
	double dz = (x[n - 1] - x[0])/numpoints;
	printf("#Evaluating linear spline, S, and integral of the linear spline, int[S], of sin(z) with z between 0 and 2*PI. The spline and the integral of the spline were given %d data points between 0 and 2*PI. They are now evaluated at %d evenly spaced points between 0 and 2*PI.\n#See linterp.svg and linterp_integ.svg.\n", n, numpoints+1);
	printf("#z\tS(z)\tint[S(z)]\n");
	for (int i = 0; i <= numpoints; i++) {
		z = x[0] + i*dz;
		printf("%g\t%g\t%g\n", z, linterp(n, x, y, z), linterp_integ(n, x, y, z));
	}

	return 0;
}
