#include<stdio.h>
#include"qinterp.h"

void generate_second_order_polynomium_values(int n, double x[], double y[]) {
	FILE* f = fopen("xsquared.dat", "w");
	fprintf(f, "x\ty\n");
	for (int i = 0; i < n; i++) {
		x[i] = i;
		y[i] = i*i;
		fprintf(f, "%g\t%g\n", x[i], y[i]);
	}
	fclose(f);
}

int main() {
	int n = 5;
	double x[n], y[n];
	generate_second_order_polynomium_values(n, x, y);

	qspline* s = qspline_alloc(n, x, y);

	int mult = 5;
	int numpoints = mult*n;
	double z = 0.0;
	double dz = (x[n - 1] - x[0])/numpoints;
	printf("#Evaluating quadratic spline, S, derivative of the qudratic spline, D[S], and integral of the quadratic spline, int[S], of z² with z between 0 and 4. The spline, the derivative of the spline and the integral of the spline were given %d data points between 0 and 4. They are now evaluated at %d evenly spaced points between 0 and 4.\n#See qinterp_evaluate.svg, qinterp_derivative.svg and qinterp_integral.svg.\n", n, numpoints+1);
	printf("#z\tS(z)\tD[S(z)]\tint[S(z)]\n");
	for (int i = 0; i <= numpoints; i++) {
		z = x[0] + i*dz;
		printf("%g\t%g\t%g\t%g\n", z, qspline_evaluate(s, z), 
				qspline_derivative(s, z), qspline_integral(s, z));
	}

	qspline_free(s);
	return 0;
}
