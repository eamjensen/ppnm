#include<assert.h>
#include<stdlib.h>
#include"cinterp.h"

cspline* cspline_alloc(int n, double x[], double y[]) {
	cspline* s = malloc(sizeof(cspline));
	s->n = n;
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->b = malloc(n*sizeof(double));
	s->c = malloc((n - 1)*sizeof(double));
	s->d = malloc((n - 1)*sizeof(double));

	for (int i = 0; i < n; i++) {
		s->x[i] = x[i];
		s->y[i] = y[i];
	}

	double dx[n - 1], p[n - 1];
	for (int i = 0; i < n - 1; i++) {
		dx[i] = x[i + 1] - x[i];
		p[i] = (y[i + 1] - y[i])/dx[i]; //eq. (1.6)
	}
	double D[n], Q[n - 1], B[n];
	D[0] = 2.0; //eq. (1.21)
	D[n - 1] = 2.0; //eq. (1.21)
	Q[0] = 1.0; //eq. (1.22)
	B[0] = 3*p[0]; //eq. (1.23)
	B[n - 1] = 3*p[n - 2]; //eq. (1.23)
	for (int i = 0; i < n - 2; i++) {
		D[i + 1] = 2*dx[i]/dx[i + 1] + 2; //eq. (1.21)
		Q[i + 1] = dx[i]/dx[i + 1]; //eq. (1.22)
		B[i + 1] = 3*p[i] + 3*p[i + 1]*dx[i]/dx[i + 1]; //eq. (1.23)
	}
	for (int i = 1; i < n; i++) {
		D[i] -= Q[i - 1]/D[i - 1]; //eq. (1.25)
		B[i] -= B[i - 1]/D[i - 1]; //eq. (1.26)
	}
	s->b[n - 1] = B[n - 1]/D[n - 1]; //eq. (1.27)
	double bi, bi1;
	for (int i = n - 2; i >= 0; i--) {
		bi1 = s->b[i + 1];
		s->b[i] = (B[i] - Q[i]*bi1)/D[i]; //eq. (1.27)
	}
	for (int i = 0; i < n - 1; i++) {
		bi = s->b[i];
		bi1 = s->b[i + 1];
		s->c[i] = (-2*bi - bi1 + 3*p[i])/dx[i]; //eq. (1.18)
		s->d[i] = (bi + bi1 - 2*p[i])/dx[i]/dx[i]; //eq. (1.18)
	}

	return s;
}

double cspline_evaluate(cspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}
	
	double r = z - s->x[i];
	double y = s->y[i];
	double b = s->b[i];
	double c = s->c[i];
	double d = s->d[i];
	return y + b*r + c*r*r + d*r*r*r; //eq. (1.14)
}

double cspline_derivative(cspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}

	double r = z - s->x[i];
	double b = s->b[i];
	double c = s->c[i];
	double d = s->d[i];
	return b + 2*c*r + 3*d*r*r; //derivative of eq. (1.14)
}

double cspline_integral(cspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}
	
	double r, y, b, c, d;
        double area = 0.0;
        for (int k = 0; k < i; k++) {
		r = s->x[k + 1] - s->x[k];
		y = s->y[k];
		b = s->b[k];
		c = s->c[k];
		d = s->d[k];
                area += y*r + b*r*r/2 + c*r*r*r/3 + d*r*r*r*r/4; //integral of eq. (1.14)
        }

	r = z - s->x[i];
	y = s->y[i];
	b = s->b[i];
	c = s->c[i];
	d = s->d[i];
        area += y*r + b*r*r/2 + c*r*r*r/3 + d*r*r*r*r/4; //final contribution

        return area;	

}

void cspline_free(cspline* s) {
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s->d);
	free(s);
}
