#ifndef LINTERP_H
#define LINTERP_H
double linterp(int n, double x[], double y[], double z);
double linterp_integ(int n, double x[], double y[], double z);
#endif
