#include<assert.h>
#include<stdlib.h>
#include"qinterp.h"

qspline* qspline_alloc(int n, double x[], double y[]) {
	qspline* s = malloc(sizeof(qspline));
	s->n = n;
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->b = malloc((n - 1)*sizeof(double));
	s->c = malloc((n - 1)*sizeof(double));

	for (int i = 0; i < n; i++) {
		s->x[i] = x[i];
		s->y[i] = y[i];
	}

	double dx[n - 1], p[n - 1];
	for (int i = 0; i < n - 1; i++) {
		dx[i] = x[i + 1] - x[i];
		p[i] = (y[i + 1] - y[i])/dx[i]; //eq. (1.6)
	}
	s->c[0] = 0.0; //recursion up (text between eq. (1.12) and (1.13))
	for (int i = 0; i < n - 2; i++) {
		s->c[i + 1] = (p[i + 1] - p[i] - s->c[i]*dx[i])/dx[i + 1]; //eq. (1.11)
	}
	s->c[n - 2] /= 2; //recursion down
	for (int i = n - 3; i >= 0; i--) {
		s->c[i] = (p[i + 1] - p[i] - s->c[i + 1]*dx[i + 1])/dx[i]; // eq. (1.12)
	}
	for (int i = 0; i < n - 1; i++) {
		s->b[i] = p[i] - s->c[i]*dx[i]; //eq. (1.13)
	}

	return s;
}

double qspline_evaluate(qspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}
	
	double r = z - s->x[i];
	double y = s->y[i];
	double b = s->b[i];
	double c = s->c[i];
	return y + b*r + c*r*r; //eq. (1.13)
}

double qspline_derivative(qspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}

	double r = z - s->x[i];
	double b = s->b[i];
	double c = s->c[i];
	return b + 2*c*r; //derivative of eq. (1.13)
}

double qspline_integral(qspline* s, double z) {
	assert(s->x[0] <= z && z <= s->x[s->n - 1]);

	int i = 0, j = s->n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (s->x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}
	
	double r, y, b, c;
        double area = 0.0;
        for (int k = 0; k < i; k++) {
		r = s->x[k + 1] - s->x[k];
		y = s->y[k];
		b = s->b[k];
		c = s->c[k];
                area += c*r*r*r/3 + b*r*r/2 + y*r; //integral of eq. (1.13)
        }

	r = z - s->x[i];
	y = s->y[i];
	b = s->b[i];
	c = s->c[i];
        area += c*r*r*r/3 + b*r*r/2 + y*r; //final contribution

        return area;	

}

void qspline_free(qspline* s) {
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}
