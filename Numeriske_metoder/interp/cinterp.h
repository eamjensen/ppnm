#ifndef CINTERP_H
#define CINTERP_H
//not possible to have "double x[], y[], (...)" in the struct definition below...
typedef struct {int n; double *x, *y, *b, *c, *d;} cspline;
cspline* cspline_alloc(int n, double x[], double y[]);
double cspline_evaluate(cspline* s, double z);
double cspline_derivative(cspline* s, double z);
double cspline_integral(cspline* s, double z);
void cspline_free(cspline* s);
#endif
