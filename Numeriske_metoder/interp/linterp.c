#include<assert.h>
#include"linterp.h"

double linterp(int n, double x[], double y[], double z) {
	assert(n > 1 && x[0] <= z && z <= x[n - 1]);
	
	int i = 0, j = n - 1;
	while (j - i > 1) { //binary search for z between x[i] and x[i+1]
		int m = (i + j)/2;
		if (x[m] < z) {
			i = m;
		} else {
			j = m;
		}
	}

	double dy = y[i + 1] - y[i]; //eq. (1.6)
	double dx = x[i + 1] - x[i];
	double p = dy/dx;
	
	return y[i] + p*(z-x[i]); //eq. (1.5)
}

double linterp_integ(int n, double x[], double y[], double z) {
        assert(n > 1 && x[0] <= z && z <= x[n - 1]);

        int i = 0, j = n - 1;
        while (j - i > 1) { //binary search for z between x[i] and x[i+1]
                int m = (i + j)/2;
                if (x[m] < z) {
                        i = m;
                } else {
                        j = m;
                }
        }

	double r, p, f;
        double area = 0.0;
        for (int k = 0; k < i; k++) {
		r = x[k + 1] - x[k];
		p = (y[k + 1] - y[k])/r;
		f = y[k];
                area += f*r + p*r*r/2; //integral of eq. (1.5)
        }

	r = z - x[i];
	p = (y[i + 1] - y[i])/r;
	f = y[i];
        area += f*r + p*r*r/2; //final contribution

        return area;
}
