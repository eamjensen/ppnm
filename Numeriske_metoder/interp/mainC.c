#include<stdio.h>
#include"cinterp.h"
#include<gsl/gsl_spline.h>

void generate_third_order_polynomium_values(int n, double x[], double y[]) {
	FILE* f = fopen("poly.dat", "w");
	fprintf(f, "x\ty\n");
	for (int i = 0; i < n; i++) {
		x[i] = i;
		y[i] = i*i*i -4*i*i + i + 5;
		fprintf(f, "%g\t%g\n", x[i], y[i]);
	}
	fclose(f);
}

int main() {
	int n = 6;
	double x[n], y[n];
	generate_third_order_polynomium_values(n, x, y);

	cspline* s = cspline_alloc(n, x, y);

	gsl_spline* gslspline = gsl_spline_alloc(gsl_interp_cspline, n);
	gsl_spline_init(gslspline, x, y, n);
	gsl_interp_accel* acc = gsl_interp_accel_alloc();
	

	int mult = 10;
	int numpoints = mult*n;
	double z = 0.0;
	double dz = (x[n - 1] - x[0])/numpoints;
	printf("#Evaluating cubic spline, S, derivative of the cubic spline, D[S], and integral of the cubic spline, int[S], of z³ - 4*z² + z + 5 with z between 0 and 5. The spline, the derivative of the spline and the integral of the spline were given %d data points between 0 and 5. They are now evaluated at %d evenly spaced points between 0 and 5.\n#See cinterp_evaluate.svg, cinterp_derivative.svg and cinterp_integral.svg.\n", n, numpoints+1);
	printf("#z\tS(z)\tD[S(z)]\tint[S(z)]\tGSL\n");
	for (int i = 0; i <= numpoints; i++) {
		z = x[0] + i*dz;
		printf("%g\t%g\t%g\t%g\t%g\n", z, cspline_evaluate(s, z), 
				cspline_derivative(s, z), cspline_integral(s, z), 
				gsl_spline_eval(gslspline, z, acc));
	}

	gsl_spline_free(gslspline);
	gsl_interp_accel_free(acc);
	cspline_free(s);
	return 0;
}
