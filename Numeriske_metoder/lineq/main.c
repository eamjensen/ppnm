#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"matvec.h"
#include"qr_gs.h"
#include"ubvt_gkl.h"

int main() {
	printf("------Exercise A.1------\n");
	int n = 5;
	int m = 3;
	gsl_matrix* A = gsl_matrix_alloc(n, m);
	generate_random_matrix_elements(A, n, m);
	matrix_print("A=", A, n, m);

	gsl_matrix* R = gsl_matrix_alloc(m, m);
	qr_gs_decomp(A, R);

	matrix_print("R=", R, m, m);
	
	gsl_matrix* C = gsl_matrix_alloc(m, m);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, C); //C = (Q^T)*Q
	matrix_print("(Q^T)*Q=", C, m, m);

	gsl_matrix* D = gsl_matrix_alloc(n, m);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, R, 0.0, D); //D = Q*R
	matrix_print("Q*R=A?", D, n, m);

	printf("\n------Exercise A.2------\n");
	gsl_matrix* AA = gsl_matrix_alloc(n, n);
	generate_random_matrix_elements(AA, n, n);
        matrix_print("AA=", AA, n, n);

	gsl_vector* b = gsl_vector_alloc(n);
	generate_random_vector_elements(b, n);
	vector_print("b (column vector)=", b, n);

	gsl_matrix* RR = gsl_matrix_alloc(n, n);
	gsl_matrix* QQ = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(QQ, AA); //copy AA into QQ because we will need AA again
	qr_gs_decomp(QQ, RR);

	gsl_vector* x = gsl_vector_alloc(n);
	qr_gs_solve(QQ, RR, b, x);
	vector_print("solution to 'AA*x=QQ*RR*x=b', x=", x, n);
	
	gsl_vector* c = gsl_vector_alloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1.0, AA, x, 0.0, c); //c = AA*x
	vector_print("AA*x=b?", c, n);

	printf("\n------Exercise B (reusing the matrix AA from just above)------\n");
	gsl_matrix* BB = gsl_matrix_alloc(n, n);
	qr_gs_inverse(QQ, RR, BB);
	matrix_print("inverse of AA, BB=", BB, n, n);

	gsl_matrix* CC = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, AA, BB, 0.0, CC); //CC = AA*BB
	matrix_print("AA*BB=I?", CC, n, n);
	gsl_matrix* X = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, BB, AA, 0.0, X); //X = BB*AA
	matrix_print("BB*AA=I?", X, n, n);

	printf("\n------Exercise C------\n");
	n = 4;
	gsl_matrix* AAA = gsl_matrix_alloc(n, n);
	gsl_matrix* BBB = gsl_matrix_alloc(n, n);
	gsl_matrix* UUU = gsl_matrix_alloc(n, n);
	gsl_matrix* VVV = gsl_matrix_alloc(n, n);
	generate_random_matrix_elements(AAA, n, n);
	matrix_print("AAA=", AAA, n, n);

	ubvt_gkl_decomp(AAA, BBB, UUU, VVV);
	matrix_print("BBB=", BBB, n, n);

	gsl_matrix* CCC = gsl_matrix_alloc(n, n);
	gsl_matrix* DDD = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, UUU, UUU, 0.0, CCC); //CCC = (UUU^T)*UUU
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, VVV, VVV, 0.0, DDD); //DDD = (VVV^T)*VVV
	matrix_print("(UUU^T)*UUU=", CCC, n, n);
	matrix_print("(VVV^T)*VVV=", DDD, n, n);

	gsl_matrix* EEE = gsl_matrix_alloc(n, n);
	gsl_matrix* FFF = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, UUU, AAA, 0.0, EEE); //EEE = (UUU^T)*AAA
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, EEE, VVV, 0.0, FFF); //FFF = EEE*VVV
	matrix_print("(UUU^T)*AAA*VVV=BBB?", FFF, n, n);
	
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* d = gsl_vector_alloc(n);
	generate_random_vector_elements(d, n);
	vector_print("d (column vector)=", d, n);
	ubvt_gkl_solve(BBB, UUU, VVV, d, y);
	vector_print("solution to 'AAA*y=UUU*BBB*(VVV^T)=d', y=", y, n);

        gsl_vector* u = gsl_vector_alloc(n);
        gsl_blas_dgemv(CblasNoTrans, 1.0, AAA, y, 0.0, u); //u = AAA*y
        vector_print("AAA*y=d?", u, n);
	
	gsl_matrix* GGG = gsl_matrix_alloc(n, n);
        ubvt_gkl_inverse(BBB, UUU, VVV, GGG);
        matrix_print("inverse of AAA, GGG=", GGG, n, n);

        gsl_matrix* HHH = gsl_matrix_alloc(n, n);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, AAA, GGG, 0.0, HHH); //HHH = AAA*GGG
        matrix_print("AAA*GGG=I?", HHH, n, n);
	gsl_matrix* Y = gsl_matrix_alloc(n, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, GGG, AAA, 0.0, Y); //Y = GGG*AAA
	matrix_print("GGG*AAA=I?", Y, n, n);

	printf("Determinant(AAA) with ubvt_gkl method:\t%g\n", ubvt_gkl_determinant(AAA));
	printf("Determinant(AAA) with qr_gs method:\t%g\n", qr_gs_determinant(AAA));

	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(C);
	gsl_matrix_free(D);
	gsl_matrix_free(AA);
	gsl_matrix_free(QQ);
	gsl_matrix_free(RR);
	gsl_matrix_free(BB);
	gsl_matrix_free(CC);
	gsl_matrix_free(AAA);
	gsl_matrix_free(BBB);
	gsl_matrix_free(UUU);
	gsl_matrix_free(VVV);
	gsl_matrix_free(CCC);
	gsl_matrix_free(DDD);
	gsl_matrix_free(EEE);
	gsl_matrix_free(FFF);
	gsl_matrix_free(GGG);
	gsl_matrix_free(HHH);
	gsl_matrix_free(X);
	gsl_matrix_free(Y);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(c);
	gsl_vector_free(y);
	gsl_vector_free(d);
	gsl_vector_free(u);
	return 0;
}
