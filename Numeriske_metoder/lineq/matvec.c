#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<stdio.h>
#include"matvec.h"
#define RND (double) rand()/RAND_MAX

void generate_random_matrix_elements(gsl_matrix* A, int rows, int cols) {
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			gsl_matrix_set(A, i, j, RND);
		}
	}
}

void generate_random_vector_elements(gsl_vector* x, int dim) {
	for (int i = 0; i < dim; i++) {
		gsl_vector_set(x, i, RND);
	}
}

void matrix_print(char* str, gsl_matrix* A, int rows, int cols) {
	printf("%s\n", str);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			printf("% 1.4f\t", gsl_matrix_get(A, i, j));
		}
		printf("\n");
	}
	printf("\n");
}

void vector_print(char* str, gsl_vector* x, int dim) {
	printf("%s\n(", str);
	for (int i = 0; i < dim - 1; i++) {
		printf("% 1.4f, ", gsl_vector_get(x, i));
	}
	printf("% 1.4f)\n\n", gsl_vector_get(x, dim - 1));
}
