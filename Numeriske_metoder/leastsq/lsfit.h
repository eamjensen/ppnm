#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef LSFIT_H
#define LSFIT_H
void lsfit(gsl_vector* x, gsl_vector* y, gsl_vector* dy, double f(int i, double x), gsl_vector* c, gsl_matrix* Sigma);
void lsfit_svd(gsl_vector* x, gsl_vector* y, gsl_vector* dy, double f(int i, double x), gsl_vector* c);
#endif
