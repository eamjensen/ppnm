#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"jacobi.h"
#include"svd.h"

/*
 * This function finds matrices for the singular value decomposition eq. (4.17).
 * Note: V is not in transposed form when the function returns.
 */
void svd_decomp(gsl_matrix* A, gsl_matrix* U, gsl_matrix* S, gsl_matrix* V) {
	int m = A->size2;

	gsl_matrix* ATA = gsl_matrix_alloc(m, m);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, ATA); //ATA = (A^T)*A
	gsl_vector* e = gsl_vector_alloc(m);
	jacobi_cyclic_sweeps(ATA, e, V); //eq. (4.22); information to create D is stored in e

	gsl_matrix* X = gsl_matrix_alloc(m, m);
	for (int i = 0; i < m; i++) {
		gsl_matrix_set(S, i, i, sqrt(gsl_vector_get(e, i))); //S = D^(1/2)
		gsl_matrix_set(X, i, i, 1/sqrt(gsl_vector_get(e, i))); //X = D^(-1/2)
	}
	gsl_matrix* Y = gsl_matrix_alloc(m, m);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, V, X, 0.0, Y); //Y = V*D^(-1/2)
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, Y, 0.0, U); //U = A*V*D^(-1/2)

	gsl_matrix_free(ATA);
	gsl_vector_free(e);
	gsl_matrix_free(X);
	gsl_matrix_free(Y);
}

/*
 * Solving the linear system A*c=b with A an n*m matrix (n>m), c an m-component vector and b an
 * n-component vector, given U, S and V (representing A) from the function svd_decomp and two 
 * vectors b and c of correct dimensions.
 */
void svd_solve(const gsl_matrix* U, const gsl_matrix* S, const gsl_matrix* V, const gsl_vector* b, gsl_vector* c) {
	int m = U->size2;

	//apply (U^T) to b, saving the result in c; going from eq. (4.17) to eq. (4.18)
	gsl_blas_dgemv(CblasTrans, 1.0, U, b, 0.0, c);

	gsl_vector* y = gsl_vector_alloc(m);
	double UTbi, Si;
	//S is diagonal, so according to eq. (4.19) y[i] = ((U^T)*b)[i]/S[i,i]
	for (int i = 0; i < m; i++) { 
		UTbi = gsl_vector_get(c, i);
		Si = gsl_matrix_get(S, i, i);
		gsl_vector_set(y, i, UTbi/Si);
	}

	//apply V to y, yielding the final result c; eq. (4.20)
	gsl_blas_dgemv(CblasNoTrans, 1.0, V, y, 0.0, c);

	gsl_vector_free(y);
}
