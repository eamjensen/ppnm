#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"lsfit.h"

double funs(int i, double x) {
	double result;
	switch(i) {
		case 0:
			result = cos(x);
			break;
		case 1:
			result = x;
			break;
		case 2:
			result = x*x*x*x*x;
			break;
		default:
			fprintf(stderr,"funs: wrong i:%d",i);
			result = NAN;
			break;
	}
	return result;
}

int main() {
	char* file_in = "inputB.dat";
	int n = 0, m = 3, dummy;
	FILE* input = fopen(file_in, "r");
	while (!feof(input)) {
		dummy = fgetc(input);
		if (dummy == '\n') {
			n++;
		}
	}
	//We now know the number of data points. Proceed by loading data into vectors.
	freopen(file_in, "r", input); //go back to beginning of file
	gsl_matrix* data = gsl_matrix_alloc(n, m);
	gsl_matrix_fscanf(input, data);
	fclose(input);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);
	gsl_matrix_get_col(x, data, 0);
	gsl_matrix_get_col(y, data, 1);
	gsl_matrix_get_col(dy, data, 2);
	
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_matrix* Sigma = gsl_matrix_alloc(m, m);
	printf("Finding coefficients c0, c1, c2 in f(x) = c0*cos(x) + c1*x + c2*x^5...\n");
	lsfit(x, y, dy, funs, c, Sigma);
	double c_err[m];
	for (int i = 0; i < m; i++) {
		c_err[i] = sqrt(gsl_matrix_get(Sigma, i, i)); //eq. (4.15)
	}
	for (int i = 0; i < m; i++) {
		printf("component %d of vector c from least-squares fit to data in %s: %g±%g\n", i + 1, file_in, gsl_vector_get(c, i), c_err[i]);
	}



	char* file_out = "fitfunctionB.txt";
	printf("Storing the above information in a separate file %s for gnuplot to use\n", file_out);
	FILE* output = fopen(file_out, "w");
	double c0 = gsl_vector_get(c, 0);
	double c1 = gsl_vector_get(c, 1);
	double c2 = gsl_vector_get(c, 2);
	fprintf(output, "f(x) = %g*cos(x)+%g*x%g*x*x*x*x*x\nfittitle = \"Fit: f(x) = %1.2f*cos(x)+%1.2f*x%1.2f*x^5\"\nfplus(x) = %g*cos(x)+%g*x%g*x*x*x*x*x\nfminus(x) = %g*cos(x)+%g*x%g*x*x*x*x*x\n", c0, c1, c2, c0, c1, c2, c0 + c_err[0], c1 + c_err[1], c2 + c_err[2], c0 - c_err[0], c1 - c_err[1], c2 - c_err[2]);
	fclose(output);
	
	printf("Please see plotB.svg for at plot of the data along with the fit\n");

	gsl_matrix_free(data);
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_vector_free(dy);
	gsl_vector_free(c);
	gsl_matrix_free(Sigma);
	return 0;
}
