#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

/*
 * This function is based on the pseudo-code on page 15 in the notes, with additions in two
 * spots from the pseudo-code on page 16 (marked with a comment 'p. 16')
 */
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R) {
	int m = A->size2;

	gsl_vector_view ai, aj;
	double norm, overlap;
	for (int i = 0; i < m; i++) {
		ai = gsl_matrix_column(A, i);
		norm = gsl_blas_dnrm2(&ai.vector);
		gsl_matrix_set(R, i, i, norm); //p. 16
		gsl_vector_scale(&ai.vector, 1/norm); //i'th vector q -> i'th row of A
		for (int j = i + 1; j < m; j++) {
			aj = gsl_matrix_column(A, j);
			gsl_blas_ddot(&aj.vector, &ai.vector, 
					&overlap); //dot product -> overlap
			gsl_matrix_set(R, i, j, overlap); //p. 16
			gsl_blas_daxpy(-overlap, &ai.vector, 
					&aj.vector); //j'th col of A -= overlap*q (q is i'th row of A)
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x) {
	int m = R->size1;

	//apply (Q^T) to b, saving the result in x
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);

	//in-place back-substitution, solving R*x=(Q^T)*b with (Q^T)*b already in x. eq. (2.4) 
	double c;
	for (int i = m - 1; i >= 0; i--) {
		c = gsl_vector_get(x, i);
		for (int k = i + 1; k < m; k++) {
			c -= gsl_matrix_get(R, i, k)*gsl_vector_get(x, k);
		}
		c /= gsl_matrix_get(R, i, i);
		gsl_vector_set(x, i, c);
	}
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B) {
	int n = R->size1;
	gsl_matrix_set_identity(B);
	
	gsl_vector_view e;
	gsl_vector* x = gsl_vector_alloc(n); //dummy vector for the solver to work on
	for (int i = 0; i < n; i++) { //solving Ax[i]=QRx[i]=e[i]; eq. (2.44)
		e = gsl_matrix_column(B, i);
		qr_gs_solve(Q, R, &e.vector, x);
		gsl_matrix_set_col(B, i, x);
	}

	gsl_vector_free(x);
}

double qr_gs_determinant(gsl_matrix* A) {
	int n = A->size1;
	gsl_matrix* R = gsl_matrix_alloc(n, n);

	qr_gs_decomp(A, R);

	double result = 1.0;
	for (int i = 0; i < n; i++) { //R is upper triangular and Q is orthogonal, so det. is +/- product of diag. elem. in R
		result *= gsl_matrix_get(R, i, i);
	}

	gsl_matrix_free(R);
        return result;	
}
