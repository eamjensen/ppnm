#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"lsfit.h"

double funs(int i, double x) {
	double result;
	switch(i) {
		case 0:
			result = log(x);
			break;
		case 1:
			result = 1.0;
			break;
		case 2:
			result = x;
			break;
		default:
			fprintf(stderr,"funs: wrong i:%d",i);
			result = NAN;
			break;
	}
	return result;
}

int main() {
	char* file_in = "inputA.dat";
	int n = 0, m = 3, dummy;
	FILE* input = fopen(file_in, "r");
	while (!feof(input)) {
		dummy = fgetc(input);
		if (dummy == '\n') {
			n++;
		}
	}
	//We now know the number of data points. Proceed by loading data into vectors.
	freopen(file_in, "r", input); //go back to beginning of file
	gsl_matrix* data = gsl_matrix_alloc(n, m);
	gsl_matrix_fscanf(input, data);
	fclose(input);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);
	gsl_matrix_get_col(x, data, 0);
	gsl_matrix_get_col(y, data, 1);
	gsl_matrix_get_col(dy, data, 2);
	
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_matrix* Sigma = gsl_matrix_alloc(m, m);
	printf("Finding coefficients c0, c1, c2 in f(x) = c0*log(x) + c1 + c2*x...\n");
	lsfit(x, y, dy, funs, c, Sigma);
	for (int i = 0; i < m; i++) {
		printf("component %d of vector c from least-squares fit to data in %s: %g\n", i + 1, file_in, gsl_vector_get(c, i));
	}
	char* file_out = "fitfunctionA.txt";
	printf("Storing the above information in a separate file %s for gnuplot to use\n", file_out);
	FILE* output = fopen(file_out, "w");
	double c0 = gsl_vector_get(c, 0);
	double c1 = gsl_vector_get(c, 1);
	double c2 = gsl_vector_get(c, 2);
	fprintf(output, "f(x) = %g*log(x)+%g%g*x\nfittitle = \"Fit: f(x) = %1.2f*log(x)+%1.2f%1.2f*x\"\n", c0, c1, c2, c0, c1, c2);
	fclose(output);
	
	printf("Please see plotA.svg for at plot of the data along with the fit\n");

	gsl_matrix_free(data);
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_vector_free(dy);
	gsl_vector_free(c);
	gsl_matrix_free(Sigma);
	return 0;
}
