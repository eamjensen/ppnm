#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef SVD_H
#define SVD_H
void svd_decomp(gsl_matrix* A, gsl_matrix* U, gsl_matrix* S, gsl_matrix* V);
void svd_solve(const gsl_matrix* U, const gsl_matrix* S, const gsl_matrix* V, const gsl_vector* b, gsl_vector* c);
#endif
