#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef JACOBI_H
#define JACOBI_H
int jacobi_cyclic_sweeps(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int jacobi_eig_by_eig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int num_eigvals, int DESCENDING);
int jacobi_classic(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
#endif
