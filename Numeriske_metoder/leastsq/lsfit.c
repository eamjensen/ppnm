#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"
#include"svd.h"
#include"lsfit.h"

void lsfit(gsl_vector* x, gsl_vector* y, gsl_vector* dy, double f(int i, double x), gsl_vector* c, gsl_matrix* Sigma) {
	int n = x->size;
	int m = c->size;
	
	gsl_matrix* A = gsl_matrix_alloc(n, m); //dimensions from text under eq. (4.1)
	gsl_vector* b = gsl_vector_alloc(n);
	double xi, yi, dyi;
	for (int i = 0; i < n; i++) { //fulfilling eq. (4.7) in this loop
		xi = gsl_vector_get(x, i);
		yi = gsl_vector_get(y, i);
		dyi = gsl_vector_get(dy, i);
		gsl_vector_set(b, i, yi/dyi);
		for (int k = 0; k < m; k++) {
			gsl_matrix_set(A, i, k, f(k, xi)/dyi);
		}
	}
	
	gsl_matrix* R = gsl_matrix_alloc(m, m);
	qr_gs_decomp(A, R); //decomposing A of eq. (4.1) into Q and R
	qr_gs_solve(A, R, b, c); //solving (A->Q due to above line) Q*R*c=b, eq. (4.8)

	gsl_matrix* Rinv = gsl_matrix_alloc(m, m);
	gsl_matrix* I = gsl_matrix_alloc(m, m);
	gsl_matrix_set_identity(I);
	qr_gs_inverse(I, R, Rinv); //find the inverse of I*R=R, i.e. R^-1 -> Rinv
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Rinv, Rinv, 0.0, Sigma); //eq. (4.14)
	
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(R);
	gsl_matrix_free(I);
	gsl_matrix_free(Rinv);
}

/*
 * Same as above function (sans covariance matrix) but using svd decomposition via jacobi 
 * algorithm instead of qr decomposition.
 */
void lsfit_svd(gsl_vector* x, gsl_vector* y, gsl_vector* dy, double f(int i, double x), gsl_vector* c) {
	int n = x->size;
	int m = c->size;
	
	gsl_matrix* A = gsl_matrix_alloc(n, m); //dimensions from text under eq. (4.1)
	gsl_vector* b = gsl_vector_alloc(n);
	double xi, yi, dyi;
	for (int i = 0; i < n; i++) { //fulfilling eq. (4.7) in this loop
		xi = gsl_vector_get(x, i);
		yi = gsl_vector_get(y, i);
		dyi = gsl_vector_get(dy, i);
		gsl_vector_set(b, i, yi/dyi);
		for (int k = 0; k < m; k++) {
			gsl_matrix_set(A, i, k, f(k, xi)/dyi);
		}
	}
	
	gsl_matrix* U = gsl_matrix_alloc(n, m);
	gsl_matrix* S = gsl_matrix_alloc(m, m);
	gsl_matrix* V = gsl_matrix_alloc(m, m);
	svd_decomp(A, U, S, V); //decomposing A of eq. (4.1) into U, S and V
	svd_solve(U, S, V, b, c); //solving A*c=U*S*(V^T)*c=b, eq. (4.8) and (4.16)

	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(U);
	gsl_matrix_free(S);
	gsl_matrix_free(V);
}
