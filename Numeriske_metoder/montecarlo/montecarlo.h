#include<gsl/gsl_vector.h>
#ifndef MONTECARLO_H
#define MONTECARLO_H
void plainmc(const gsl_vector* a, const gsl_vector* b, double f(const gsl_vector* x), int N, double* result, double* error);
void randomx(const gsl_vector* a, const gsl_vector* b, gsl_vector* x);
#endif
