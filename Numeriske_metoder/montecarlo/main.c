#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"montecarlo.h"
#include<math.h>

double sphere(const gsl_vector* x) {
	if (gsl_blas_dnrm2(x) <= 1.0) {
		return 1.0;
	} else {
		return 0.0;
	}
}

double charged_spherical_shell(const gsl_vector* x) {
	double R = 0.9; //proton radius in fm
	if (gsl_blas_dnrm2(x) < R) {
		return 0.0;
	} else {
		double alpha = 1.0/137; //fine structure constant
		double hbarc = 197.0; //ħc in MeV*fm
		double r = gsl_blas_dnrm2(x); //distance from center in fm
		return alpha*hbarc/(8*M_PI)/(r*r*r*r);
	}
}

double difficult(const gsl_vector* x) {
	double x0 = gsl_vector_get(x, 0);
	double x1 = gsl_vector_get(x, 1);
	double x2 = gsl_vector_get(x, 2);
	return 1.0/(1 - cos(x0)*cos(x1)*cos(x2))/(M_PI*M_PI*M_PI);
}

int main() {
	int n = 3;
	gsl_vector* a = gsl_vector_alloc(n);
	gsl_vector* b = gsl_vector_alloc(n);
	int N;
	double result, error;
	char* filename;
	FILE* out;

	
	printf("------Part A------\n");
	gsl_vector_set_all(a, -1.0);
	gsl_vector_set_all(b, 1.0);
	N = (int) 1e7;
	printf("Calculating the integral of f(x,y,z)=1 on and inside a unit sphere (the result is 4π/3) with a plain Monte Carlo integrator for N=%.0e...\n", (double) N);
	plainmc(a, b, sphere, N, &result, &error);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\n", result, 4*M_PI/3, error, fabs(result - 4*M_PI/3));

	gsl_vector_set_all(a, -40.0);
	gsl_vector_set_all(b, 40.0);
	printf("\nEstimating the electrostatic energy of a uniformly charged spherical shell of total charge equal to the elementary charge and radius R equal to (roughly) the proton radius, 0.9 fm, by integrating f(r(x,y,z))=αħc/(8πr⁴) for r ≥ R (the electric field is zero inside the shell). This is done with a plain Monte Carlo integrator with N=%.0e, sampling in a cubic box with lengths %g fm and the spherical shell placed in the box' center.\nThe result (integrating over all space) is αħc/(2R) ≈ 197/(137*2*0.9) MeV -- we shall call this the \"exact\" result.\n", (double) N, 2*gsl_vector_get(b, 0));
	plainmc(a, b, charged_spherical_shell, N, &result, &error);
	printf("Result:          % .16f MeV\n\"Exact\" result:  % .16f MeV\nEstimated error: % .16f\nActual error:    % .16f\n", result, (0.5*197)/(137*0.9), error, fabs(result - (0.5*197)/(137*0.9)));

	gsl_vector_set_all(a, 0.0);
	gsl_vector_set_all(b, M_PI);
	printf("\nCalculating the integral of (1/π³)[1-cos(x)cos(y)cos(z)]⁻¹ with \"lower-left\" vertex at (0,0,0) and \"upper-right\" vertex at (π,π,π) (the result is Γ(1/4)⁴/(4π³)) with a plain Monte Carlo integrator for N=%.0e...\n", (double) N);
	plainmc(a, b, difficult, N, &result, &error);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\n", result, 1.3932039296856769, error, fabs(result - 1.3932039296856769));




	printf("\n------Part B------\n");
	int datapoints = 45;
	int Ns[datapoints];
	Ns[0] = 1000;
	for (int i = 1; i < datapoints; i++) {
		Ns[i] = Ns[i-1]*1.25;
	}

	gsl_vector_set_all(a, -1.0);
	gsl_vector_set_all(b, 1.0);
	filename = "sphere.dat";
	out = fopen(filename, "w");
	printf("Calculating, again, the integral of f(x,y,z)=1 on a unit sphere with a plain Monte Carlo integrator for various N, and printing the actual errors to a separate file %s for gnuplot to use...\n", filename);
	fprintf(out, "#N\terr\n");
	for (int i = 0; i < datapoints; i++) {
		plainmc(a, b, sphere, Ns[i], &result, &error);
		fprintf(out, "%g\t%g\n", (double) Ns[i], fabs(result - 4*M_PI/3));
	}
	fclose(out);
	printf("Please see sphere.svg for an illustration of the O(1/√N) behaviour.\n");
	
	gsl_vector_set_all(a, -40.0);
	gsl_vector_set_all(b, 40.0);
	filename = "charge.dat";
	out = fopen(filename, "w");
	printf("\nEstimating, again, the electrostatic energy of a uniformly charged spherical shell of total charge equal to the elementary charge and radius R = 0.9 fm with a plain Monte Carlo integrator for various N, and printing the actual errors to a separate file %s for gnuplot to use...\n", filename);
	fprintf(out, "#N\terr\n");
	for (int i = 0; i < datapoints; i++) {
		plainmc(a, b, charged_spherical_shell, Ns[i], &result, &error);
		fprintf(out, "%g\t%g\n", (double) Ns[i], fabs(result - (0.5*197)/(137*0.9)));
	}
	fclose(out);
	printf("Please see charge.svg for an illustration of the O(1/√N) behaviour.\n");

	printf("\nAlso, please see 'Final notes on plots' below.\n");




	printf("\n------Final notes on plots------\n");
	printf("Looking in the two log files from the gnuplot fitting routines, especially one of the two rms' of residuals is not entirely convincing. However, the figures shows that there generally are a few outliers, but that the overall trend seems to be an inverse squareroot. The charged spherical shell poses the most problems, but this is not so strange, as the integration volume (which should actually have been infinite) was chosen as some sort of compromise; a compromise between, on the one hand, having too large an integration volume where the random points of the plain Monte Carlo integrator would, perhaps, never (for finite N) end up close enough to the shell to give any significant contributions and, on the other hand, having too small an integration volume where the inside of the shell (contributing nothing) would be sampled many times, again resulting in an underestimate.\n");

	return 0;
}
