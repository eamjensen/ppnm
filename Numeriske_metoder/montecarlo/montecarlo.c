#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<assert.h>
#include<math.h>
#include"montecarlo.h"
#define RND ((double) rand()/RAND_MAX)

/*
 * Inspired by table 9.1 in the notes, with references to equation numbers and similar in the notes.
 */
void plainmc(const gsl_vector* a, const gsl_vector* b, double f(const gsl_vector* x), int N, double* result, double* error) {
	assert(N > 0);
	int n = a->size;

	double V = 1.0;
	for (int i = 0; i < n; i++) { //integration volume
		V *= gsl_vector_get(b, i) - gsl_vector_get(a, i);
	}

	gsl_vector* x = gsl_vector_alloc(n);
	double sum = 0.0, sum2 = 0.0;
	double fx;
	for (int i = 0; i < N; i++) {
		randomx(a, b, x); //x <- random point inside volume between a and b
		fx = f(x);
		sum += fx;
		sum2 += fx*fx;
	}
	gsl_vector_free(x);

	double avr = sum/N; //<f> of eq. (9.2)
	double var = sum2/N - avr*avr; //eq. (9.4)
	*result = avr*V; //eq. (9.2)
	*error = V*sqrt(var/N); //eq. (9.3)
}

/*
 * Inspired by table 9.1 in the notes.
 */
void randomx(const gsl_vector* a, const gsl_vector* b, gsl_vector* x) {
	int n = a->size;
	double ai, bi;
	for (int i = 0; i < n; i++) {
		ai = gsl_vector_get(a, i);
		bi = gsl_vector_get(b, i);
		gsl_vector_set(x, i, ai + RND*(bi-ai)); //pseudo-random number between a[i] and b[i]
	}
}

/*
 * Inspired by table 9.2 in the notes.
 */
/*
void stratifiedmc(const gsl_vector* a, const gsl_vector* b, double f(const gsl_vector* x), int N, double* result, double* error, double abs, double rel) {
	assert(N > 0);
        int n = a->size;

	plainmc(a, b, f, N, result, error);
	if (*error < abs + rel*fabs(*result)) { //usual tolerance check, e.g. eq. (8.47)
		//do nothing
		//the result and the estimated error on the result are stored in result and error
	} else {
		
	}
}
*/
