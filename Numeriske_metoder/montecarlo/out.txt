------Part A------
Calculating the integral of f(x,y,z)=1 on and inside a unit sphere (the result is 4π/3) with a plain Monte Carlo integrator for N=1e+07...
Result:           4.1894096000000003
"Exact" result:   4.1887902047863905
Estimated error:  0.0012634921449470
Actual error:     0.0006193952136098

Estimating the electrostatic energy of a uniformly charged spherical shell of total charge equal to the elementary charge and radius R equal to (roughly) the proton radius, 0.9 fm, by integrating f(r(x,y,z))=αħc/(8πr⁴) for r ≥ R (the electric field is zero inside the shell). This is done with a plain Monte Carlo integrator with N=1e+07, sampling in a cubic box with lengths 80 fm and the spherical shell placed in the box' center.
The result (integrating over all space) is αħc/(2R) ≈ 197/(137*2*0.9) MeV -- we shall call this the "exact" result.
Result:           0.7990564968859201 MeV
"Exact" result:   0.7988645579886456 MeV
Estimated error:  0.0261498722162231
Actual error:     0.0001919388972744

Calculating the integral of (1/π³)[1-cos(x)cos(y)cos(z)]⁻¹ with "lower-left" vertex at (0,0,0) and "upper-right" vertex at (π,π,π) (the result is Γ(1/4)⁴/(4π³)) with a plain Monte Carlo integrator for N=1e+07...
Result:           1.3849848590905758
"Exact" result:   1.3932039296856769
Estimated error:  0.0022986696124243
Actual error:     0.0082190705951011

------Part B------
Calculating, again, the integral of f(x,y,z)=1 on a unit sphere with a plain Monte Carlo integrator for various N, and printing the actual errors to a separate file sphere.dat for gnuplot to use...
Please see sphere.svg for an illustration of the O(1/√N) behaviour.

Estimating, again, the electrostatic energy of a uniformly charged spherical shell of total charge equal to the elementary charge and radius R = 0.9 fm with a plain Monte Carlo integrator for various N, and printing the actual errors to a separate file charge.dat for gnuplot to use...
Please see charge.svg for an illustration of the O(1/√N) behaviour.

Also, please see 'Final notes on plots' below.

------Final notes on plots------
Looking in the two log files from the gnuplot fitting routines, especially one of the two rms' of residuals is not entirely convincing. However, the figures shows that there generally are a few outliers, but that the overall trend seems to be an inverse squareroot. The charged spherical shell poses the most problems, but this is not so strange, as the integration volume (which should actually have been infinite) was chosen as some sort of compromise; a compromise between, on the one hand, having too large an integration volume where the random points of the plain Monte Carlo integrator would, perhaps, never (for finite N) end up close enough to the shell to give any significant contributions and, on the other hand, having too small an integration volume where the inside of the shell (contributing nothing) would be sampled many times, again resulting in an underestimate.
