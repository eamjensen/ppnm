#include<stdio.h>
#include"integrator.h"
#include<math.h>
#include<float.h>
#include<gsl/gsl_integration.h>

double root(double x) {
	return sqrt(x);
}

double oneoverroot(double x) {
	return 1.0/sqrt(x);
}

double oneoverroot_GSL(double x, void* params) {
	return 1.0/sqrt(x);
}

double logoverroot(double x) {
	return log(x)/sqrt(x);
}

double logoverroot_GSL(double x, void* params) {
	return log(x)/sqrt(x);
}

double pifunc(double x) {
	return 4*sqrt(1-(1-x)*(1-x));
}

double gauss(double x) {
	return exp(-x*x);
}

double gauss_GSL(double x, void* params) {
	return exp(-x*x);
}

double pihalf(double x) {
	return 1.0/(x*x + 1);
}

double pihalf_GSL(double x, void* params) {
	return 1.0/(x*x + 1);
}

int main() {
	double abs = 1e-4;
	double rel = abs;
	printf("All relative and absolute accuracies are set to %g in this exercise unless otherwise specified.\n", rel);




	printf("\n------Part A------\n");
	double a = 0.0;
	double b = 1.0;
	double err;
	int evals;
	double result;
	printf("Calculating the integral of √(x) from x=%g to x=%g with recursive adaptive integrator...\n", a, b);
	result = adaptive_quadrature(root, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, 2.0/3, err, fabs(result - 2.0/3), evals);

	printf("\nCalculating the integral of 1/√(x) from x=%g to x=%g with recursive adaptive integrator...\n", a, b);
	result = adaptive_quadrature(oneoverroot, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, 2.0, err, fabs(result - 2.0), evals);

	printf("\nCalculating the integral of ln(x)/√(x) from x=%g to x=%g with recursive adaptive integrator...\n", a, b);
	result = adaptive_quadrature(logoverroot, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, -4.0, err, fabs(result + 4.0), evals);


	abs = DBL_EPSILON;
	rel = abs;
	printf("\nCalculating the integral of 4√(1-(1-x)²) from x=%g to x=%g with recursive adaptive integrator. Relative accuracy=%g and absolute accuracy=%g (machine epsilon for doubles)...\n", a, b, rel, abs);
	result = adaptive_quadrature(pifunc, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, M_PI, err, fabs(result - M_PI), evals);




	printf("\n------Part B------\n");
	abs = 1e-4;
	rel = abs;

	printf("Calculating the integral of 1/√(x) from x=%g to x=%g with Clenshaw-Curtis variable transformation...\n", a, b);
	result = clenshaw_curtis(oneoverroot, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, 2.0, err, fabs(result - 2.0), evals);


	printf("\nCalculating the integral of ln(x)/√(x) from x=%g to x=%g with Clenshaw-Curtis variable transformation...\n", a, b);
	result = clenshaw_curtis(logoverroot, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, -4.0, err, fabs(result + 4.0), evals);

	printf("\nNotice, in comparison to the method used in part A, that the number of integrand evaluations is a lot smaller and that the error is either of the same order or smaller.\n"); 
	

	printf("\n\n~~Comparison with library routine (gsl_integration_qags) follows~~\n");
	int n = 1000;
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(n);
	gsl_function F;
	F.function = &oneoverroot_GSL;
	F.params = NULL;
	printf("Calculating the integral of 1/√(x) from x=%g to x=%g with GSL...\n", a, b);
	gsl_integration_qags(&F, a, b, abs, rel, n, w, &result, &err);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nNumber of subinterval divisions: %ld\n", result, 2.0, err, fabs(result - 2.0), w->size);
	
	F.function = &logoverroot_GSL;
	printf("\nCalculating the integral of ln(x)/√(x) from x=%g to x=%g with GSL. Relative accuracy=%g and absolute accuracy=%g...\n", a, b, rel, abs);
	gsl_integration_qags(&F, a, b, abs, rel, n, w, &result, &err);
	printf("Result:          % .16f\nExact result:    % .16f\nEstimated error: % .16f\nActual error:    % .16f\nNumber of subinterval divisions: %ld\n", result, -4.0, err, fabs(result + 4.0), w->size);

	printf("\n\nAfter the GSL routine has returned a result, information on the number of subinterval divisions used to reach the result is available. These have been printed along with the results and errors above. We do not, however, know (as compared to our counted number of integrand evaluations) how GSL utilizes these divisions. The documentation states:\n\t\"The QAG algorithm is a simple adaptive integration procedure.\n\tThe integration region is divided into subintervals, and on each\n\titeration the subinterval with the largest estimated error is bisected.\n\tThis reduces the overall error rapidly, as the subintervals become\n\tconcentrated around local difficulties in the integrand.\n\t(...)\n\tThe presence of an integrable singularity in the integration region\n\tcauses an adaptive routine to concentrate new subintervals around the\n\tsingularity. As the subintervals decrease in size the successive\n\tapproximations to the integral converge in a limiting fashion.\n\tThis approach to the limit can be accelerated using an extrapolation\n\tprocedure. The QAGS algorithm combines adaptive bisection with the\n\tWynn epsilon-algorithm to speed up the integration of many types of\n\tintegrable singularities.\"\nThe adaptive quadrature method we, ourselves, have implemented makes n=(N-4)/4 subinterval divisions, where N is the number of intregrand evaluations.\nSo our own method makes (with the Clenshaw-Curtis variable transformation) (52-4)/4=%d subinterval divisions to calculate the integral of 1/√(x), and (272-4)/4=%d subinterval divisions to calculate the integral of ln(x)/√(x).\nThe errors on the results speaks for themselves, however: GSL's routine is clearly better than ours (which uses rectangle and trapezium rules).\n", (52-4)/4, (272-4)/4);




	printf("\n------Part C------\n");
	a = -INFINITY;
	b = INFINITY;
	printf("Calculating the integral of exp(-x²) from x=%g to x=%g (the result is √(PI)) with recursive adaptive integrator...\n", a, b);
	result = adaptive_quadrature(gauss, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, sqrt(M_PI), err, fabs(result - sqrt(M_PI)), evals);

	a = 0.0;
	printf("\nCalculating the integral of 1/(x² + 1) from x=%g to x=%g (the result is PI/2) with recursive adaptive integrator...\n", a, b);
	result = adaptive_quadrature(pihalf, a, b, abs, rel, &err, &evals);
	printf("Result:          % .16f\n\"Exact\" result:  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nIntegrand evaluations: %d\n", result, M_PI/2, err, fabs(result - M_PI/2), evals);


	printf("\n\n~~Comparison with library routines (gsl_integration_qagiu and gsl_integration_qagi) follows~~\n");
	a = -INFINITY;
	F.function = &gauss_GSL;
	printf("Calculating the integral of exp(-x²) from x=%g to x=%g with GSL...\n", a, b);
	gsl_integration_qagi(&F, abs, rel, n, w, &result, &err);
	printf("Result:          % .16f\n\"Exact result\":  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nNumber of subinterval divisions: %ld\n", result, sqrt(M_PI), err, fabs(result - sqrt(M_PI)), w->size);

	a = 0.0;
	F.function = &pihalf_GSL;
	printf("\nCalculating the integral of 1/(x² + 1) from x=%g to x=%g with GSL...\n", a, b);
	gsl_integration_qagiu(&F, a, abs, rel, n, w, &result, &err);
	printf("Result:          % .16f\n\"Exact result\":  % .16f\nEstimated error: % .16f\nActual error:    % .16f\nNumber of subinterval divisions: %ld\n", result, M_PI/2, err, fabs(result - M_PI/2), w->size);

	printf("\n\nOur own method makes %d subinterval divisions to calculate the integral of exp(-x²), and %d subinterval divisions to calculate the integral of 1/(x² + 1).\n", (88-4)/4, (56-4)/4);
	printf("Again, the errors on the GSL routines are much, much smaller than on ours.\n");

	gsl_integration_workspace_free(w);
	return 0;
}
