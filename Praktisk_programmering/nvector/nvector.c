#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"nvector.h"

nvector* nvector_alloc(int n) {
	assert(n > 0);

	nvector* v = malloc(sizeof(nvector));
	v->size = n;
	v->data = malloc(n*sizeof(double));
	if (v == NULL) {
		fprintf(stderr, "error in nvector_alloc\n");
	}
	return v;
}

void nvector_free(nvector* v) {
	free(v->data);
	free(v);
}

void nvector_set(nvector* v, int i, double value) {
	assert(0 <= i && i < v->size);
	
	v->data[i] = value;
}

double nvector_get(nvector* v, int i) {
	assert(0 <= i && i < v->size);
	
	return v->data[i];
}

double nvector_dot_product(nvector* u, nvector* v) {
	assert(u->size == v->size);

	int n = u->size;
	double result = 0;
	for (int i = 0; i < n; i++) {
		result += u->data[i] * v->data[i];
	}
	return result;
}

void nvector_print(char* s, nvector* v) {
	int n = v->size;
	printf("%s(", s);
	for (int i = 0; i < n; i++) {
		if (n - i != 1) {
			printf("%g, ", v->data[i]);
		} else {
			printf("%g)\n", v->data[i]);
		}
	}
}

void nvector_set_zero(nvector* v) {
	int n = v->size;
	for (int i = 0; i < n; i++) {
		v->data[i] = 0;
	}
}

int nvector_equal(nvector* a, nvector* b) {
	assert(a->size == b->size);
	
	int n = a->size;
	for (int i = 0; i < n; i++) {
		if (a->data[i] != b->data[i]) {
			return 0;
		}
	}
	return 1;
}

void nvector_add(nvector* a, nvector* b) {
	assert(a->size == b->size);
	
	int n = a->size;
	for (int i = 0; i < n; i++) {
		a->data[i] += b->data[i];
	}
}

void nvector_sub(nvector* a, nvector* b) {
	assert(a->size == b->size);
	
	int n = a->size;
	for (int i = 0; i < n; i++) {
		a->data[i] -= b->data[i];
	}
}

void nvector_scale(nvector* a, double x) {
	int n = a->size;
	for (int i = 0; i < n; i++) {
		a->data[i] *= x;
	}
}
