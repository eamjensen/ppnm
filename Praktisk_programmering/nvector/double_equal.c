#include<math.h>
#include<float.h>

int double_equal(double a, double b) {
	return (fabs(a - b) < DBL_EPSILON || 2*(fabs(a - b)/(fabs(a) + fabs(b))) < DBL_EPSILON)? 1 : 0;
}
