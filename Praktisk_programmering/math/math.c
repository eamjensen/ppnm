#include<stdio.h>
#include<math.h>
#include<complex.h>

int main() {
	//calculate some numbers
	double g_5 = tgamma(5.0);
	double j1_05 = j1(0.5);
	double complex comnums[] = {csqrt(-2.0), cexp(I * 1.0), cexp(I * M_PI), cpow(I * 1.0, M_E)};
		
	//test precision of float, double and long double
	float f = 0.1111111111111111111111111111;
	double d = 0.1111111111111111111111111111;
	long double ld = 0.1111111111111111111111111111L;
	
	//print results
	printf("Gamma(5) = %g\nJ_1(0.5) = %g\nThe complex numbers sqrt(-2), exp(i), exp(i*pi) and i^e are, in order:\n", g_5, j1_05);
	for (int i = 0; i < sizeof(comnums)/sizeof(long double); i++) {
		printf("%g%+gi\n", creal(comnums[i]), cimag(comnums[i]));
	}	
	printf("\n");
	printf("Defining a float, a double and a long double all to have the value 0.1111111111111111111111111111 results in the representations, in order:\n%.25g\n%.25lg\n%.25Lg\n", f, d, ld);

	return 0;
}
