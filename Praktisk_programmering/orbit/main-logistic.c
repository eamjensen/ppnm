#include<stdio.h>
#include<math.h>

double mylogistic(const double x);
double mathlogistic(double x) {
	return 0.5*(1 + tanh(x/2));
}

int main() {
	printf("x\tsol.\tlog. f.\n");
	for (double x = 0.0; x <= 3.01; x += 0.1) {
		printf("%6.5f\t%6.5f\t%6.5f\n", x, mylogistic(x), mathlogistic(x));
	}
	
	return 0;
}
