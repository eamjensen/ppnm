#include<stdio.h>
#include<math.h>

double phi0, eps;
double y[2];

double equatorial(const double phi, double phi0, double y[], double eps);

void print_ten_orbits_to_file(const char* name) {
	FILE* f = fopen(name, "w");
	double ytmp[2];
	fprintf(f, "phi\tu\n");
	for (double phi = 0.0; phi < 10*2*M_PI; phi += 0.1) {
		//the current implementation is not ideal. equatorial() integrates from phi0 to phi on each iteration, and the given array ytmp is tampered with on each iteration, necessitating keeping the values in y unchanged, and setting ytmp equal to y on each iteration.
		ytmp[0]=y[0]; ytmp[1]=y[1];
		fprintf(f, "%6.5f\t%6.5f\t\n", phi, equatorial(phi, phi0, ytmp, eps));
	}
	fclose(f);
}

int main() {
	printf("Solving the differential equation u''(phi) + u(phi) = 1 + eps*u^2(phi) with various initial conditions from phi=0 to phi=20*pi...\n");

	phi0 = 0.0, eps = 0.0;
	y[0] = 1.0; y[1] = 0.0;
	const char* fn1 = "eq1.txt";
	printf("Results for\n");
	printf("eps=%g, phi0=%g, u(phi0)=y0(phi0)=%g, u'(phi0)=y1(phi0)=%g\n", eps, phi0, y[0], y[1]);
	print_ten_orbits_to_file(fn1);
	printf("printed to %s\n", fn1);

	y[1] = -0.5;
	const char* fn2 = "eq2.txt";
	printf("Results for\n");
	printf("eps=%g, phi0=%g, u(phi0)=y0(phi0)=%g, u'(phi0)=y1(phi0)=%g\n", eps, phi0, y[0], y[1]);
	print_ten_orbits_to_file(fn2);
	printf("printed to %s\n", fn2);
	
	eps = 0.05;
	const char* fn3 = "eq3.txt";
	printf("Results for\n");
	printf("eps=%g, phi0=%g, u(phi0)=y0(phi0)=%g, u'(phi0)=y1(phi0)=%g\n", eps, phi0, y[0], y[1]);
	print_ten_orbits_to_file(fn3);
	printf("printed to %s\n", fn3);
	
	return 0;
}
