#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int ode_equatorial(double phi, const double y[], double dydphi[], void* params) {
	double eps = *(double*) params;
	//second order equation u'' + u = 1 + eps*u^2 rewritten as y0' = y1 and y1' = 1 + eps*y0^2 - y0 with y0 = u and y1 = u'
	dydphi[0] = y[1];
	dydphi[1] = 1 + eps*y[0]*y[0] - y[0];
	return GSL_SUCCESS;
}

double equatorial(const double phi, double phi0, double y[], double eps) {
	gsl_odeiv2_system sys;
	sys.function = ode_equatorial;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*) &eps;

	gsl_odeiv2_driver* driver;
	double hstart = 1e-3, abs = 1e-7, rel = 1e-7;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, hstart, abs, rel);

	gsl_odeiv2_driver_apply(driver, &phi0, phi, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
