#include<stdio.h>
#include<stdlib.h>

double largest_eigenvalue(int n);

int main(int argc, char** argv) {
	printf("My studienummer is 201504997, so I have to solve exercise 26.\n");

	if (argc == 3) {
		int a = (int) atof(argv[1]), b = (int) atof(argv[2]);
		
		if (a > b) {
			printf("Please supply two integers, the second greater than the first, as arguments.\n");
			return 0;
		}
		
		printf("Generating random real symmetric matrices with n in the range [%d, %d] and finding their largest eigenvalues...\n", a, b);

		printf("n\teigval\n");
		for (int i = a; i <= b; i++) {
			printf("%d\t%g\n", i, largest_eigenvalue(i));
		}
	} else {
		printf("Please supply two integers, the second greater than the first, as arguments.\n");
	}

	return 0;
}
