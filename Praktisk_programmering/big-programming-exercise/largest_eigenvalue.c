#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_sort_vector.h>
#define RND (double)rand()/RAND_MAX

void fill_with_random_symmetric_elements(int n, gsl_matrix* M) {
	double currentRND;
	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			currentRND = RND;
			if (i == j) {
				gsl_matrix_set(M, i, j, currentRND);
			} else {
				gsl_matrix_set(M, i, j, currentRND);
				gsl_matrix_set(M, j, i, currentRND);
			}
		}
	}

}

double largest_eigenvalue(int n) {
	gsl_matrix* M = gsl_matrix_alloc(n, n);
	fill_with_random_symmetric_elements(n, M);
	
	gsl_eigen_symm_workspace* w = gsl_eigen_symm_alloc(n);
	gsl_vector* eigvals = gsl_vector_alloc(n);
	gsl_eigen_symm(M, eigvals, w); //eigenvalues are stored in eigvals after this execution

	gsl_sort_vector(eigvals); //sorted in ascending order
	double result = gsl_vector_get(eigvals, n - 1);

	gsl_matrix_free(M);
	gsl_eigen_symm_free(w);
	gsl_vector_free(eigvals);
	return result;
}
