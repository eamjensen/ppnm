#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

double rb1_100(const gsl_vector* v, void* params);

int main() {
	double eps = 1e-6;
	double stepsize = 0.1;
	double x0 = 1.7, y0 = 3.3;
	
	int dim = 2;
	gsl_multimin_function F;
	F.f = rb1_100;
	F.n = dim;
	F.params = NULL;

	gsl_multimin_fminimizer* M;
	M = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, dim);
	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set(start, 0, x0);
	gsl_vector_set(start, 1, y0);
	gsl_vector_set(step, 0, stepsize);
	gsl_vector_set(step, 1, stepsize);

	gsl_multimin_fminimizer_set(M, &F, start, step);
	
	int flag = GSL_CONTINUE, iter = 0;
	double size;
	const char* filename = "path.txt";
	FILE* f = fopen(filename, "w");
	fprintf(f, "x\ty\tf(x,y)\n");
	double xtmp, ytmp, ftmp;
	do {
		gsl_multimin_fminimizer_iterate(M);
		iter++;
		xtmp = gsl_vector_get(M->x, 0);
		ytmp = gsl_vector_get(M->x, 1);
		ftmp = M->fval;
		fprintf(f, "%g\t%g\t%g\n", xtmp, ytmp, ftmp);
		size = gsl_multimin_fminimizer_size(M);
		flag = gsl_multimin_test_size(size, eps);
	} while (flag == GSL_CONTINUE);
	
	double xres = gsl_vector_get(M->x, 0);
	double yres = gsl_vector_get(M->x, 1);
	double fres = M->fval;

	printf("Minimum of Rosenbrock function with a=1 and b=100 is found to be at (x,y)=(%g,%g).\nThe function value at that point is (approximately) f(%g,%g)=%g.\nIt took %i iterations to find this point with initial guesses x0=%g and y0=%g.\n", xres, yres, xres, yres, fres, iter, x0, y0);
        printf("The points where the algorithm calculated the values of the function have been printed to the file %s\n", filename);
	printf("Please refer to rosenbrock.svg for a visualization of this\n");
        fprintf(f, "%g\t%g\t%g\n", xres, yres, fres);
        fclose(f);

	gsl_multimin_fminimizer_free(M);
	gsl_vector_free(start);
	gsl_vector_free(step);
	return 0;

}
