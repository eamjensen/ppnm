#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>

typedef struct {int n; double *t,*y,*e;} experimental_data;

double chi2(const gsl_vector *x, void *params) {
	double A = gsl_vector_get(x, 0);
	double T = gsl_vector_get(x, 1);
	double B = gsl_vector_get(x, 2);
	experimental_data* p = (experimental_data*) params;
	int n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum = 0;
	double f;
	for(int i = 0; i < n; i++) {
	       	f = A*exp(-(t[i])/T) + B;
		sum += pow((f - y[i])/e[i], 2);
	}
	return sum;
}

int main() {
	double eps = 1e-6;

	char* filename = "activity.dat";
	FILE* in = fopen(filename, "r");
	int n = 0, c = 0;
	//determine number of lines in the data file
	while ((c = fgetc(in)) != EOF) {
		if (c == '\n') {
			n++;
		}
	}
	fclose(in);

	//now, import the data points
	double t[n], y[n], e[n];
	in = fopen(filename, "r");
	for (int i = 0; i < n; i++) {
		fscanf(in, "%lg", &t[i]);
		fscanf(in, "%lg", &y[i]);
		fscanf(in, "%lg", &e[i]);
	}
	fclose(in);

	/*
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	*/
	experimental_data data = {n, t, y, e};
	
	int dim = 3;
	gsl_multimin_function F;
	F.f = chi2;
	F.n = dim;
	F.params = (void*) &data;

	gsl_multimin_fminimizer* M;
	M = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, dim);
	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set(start, 0, 6);
	gsl_vector_set(start, 1, 1);
	gsl_vector_set(start, 2, 1);
	gsl_vector_set(step, 0, 0.5);
	gsl_vector_set(step, 1, 0.1);
	gsl_vector_set(step, 2, 0.1);

	gsl_multimin_fminimizer_set(M, &F, start, step);

	int flag = GSL_CONTINUE, iter = 0;
	double size;
	do {
		gsl_multimin_fminimizer_iterate(M);
		iter++;
		
		size = gsl_multimin_fminimizer_size(M);
		flag = gsl_multimin_test_size(size, eps);
	} while (flag == GSL_CONTINUE);

	double A = gsl_vector_get(M->x, 0), T = gsl_vector_get(M->x, 1), B = gsl_vector_get(M->x, 2);
	printf("The least-squares fit of f(t)=A*exp(-t/T)+B to the given data yields A=%g, T=%g, B=%g\n", A, T, B);
	printf("Please see activity.svg for a visualization of this\n");

	//print function definition by itself with the found parameters to a separate file which gnuplot can interpret and plot
	FILE* out = fopen("fitfunction.txt", "w");
	fprintf(out, "f(x)=%g*exp(-x/%g)+%g", A, T, B);
	fclose(out);

	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(M);
	return 0;
}
