#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

double rb1_100(const gsl_vector* v, void* params) {
	double x = gsl_vector_get(v, 0);
	double y = gsl_vector_get(v, 1);

	return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}
