#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

int main(int argc, char** argv) {
	printf("Using OpenMP to approximate the value of pi utilising multiprocessing\n");
	int n = (argc > 1)? (int) atof(argv[1]) : (int) 1e9;

	int nin = 0;
	double x, y;
	unsigned int seed;
	#pragma omp parallel for private(x, y, seed) reduction(+:nin)
	for (int i = 0; i < n; i++) {
		if (i == 0) {
			seed = (unsigned int) time(NULL);
		}
		x = (double) rand_r(&seed)/(double) RAND_MAX;
		y = (double) rand_r(&seed)/(double) RAND_MAX;
		if (sqrt(x*x + y*y) <= 1.0) {
			nin++;
		}
	}

	double pi = 4.0*nin/n;
	printf("Total number of throws=%d, number of throws within circle=%d, approximation of pi=%g\n", n, nin, pi);
	
	char* filename = "data.txt";
        FILE* f = fopen(filename, "a");
        printf("Appending the total number of throws and the approximation of pi to %s\n", filename);
        fprintf(f, "%d\t%g\n", n, pi);

	return 0;
}
