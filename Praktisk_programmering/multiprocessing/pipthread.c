#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>
#include<time.h>

typedef struct {int N, Nin;} throws;

void* throwdarts(void* params) {
	unsigned int seed = time(NULL);

	throws* t = (throws*) params;
	int N = t->N;
	int Nin = 0;
	double x, y;
	for (int i = 0; i < N; i++) {
		x = (double) rand_r(&seed)/(double) RAND_MAX;
		y = (double) rand_r(&seed)/(double) RAND_MAX;
		if (sqrt(x*x + y*y) <= 1.0) {
			Nin++;
		}
	}
	t->Nin = Nin;
	
	return NULL;
}

int main(int argc, char** argv) {
	printf("Using pthreads to approximate the value of pi utilising multiprocessing\n");
	int n = (argc > 1)? (int) atof(argv[1]) : (int) 1e9;
	throws t1, t2;
	t1.N = n/2;
	t2.N = n/2;

	pthread_t thr;
	pthread_create(&thr, NULL, throwdarts, (void*) &t1);
	throwdarts((void*) &t2);
	pthread_join(thr, NULL);

	int Ntot = t1.N + t2.N, Nintot = t1.Nin + t2.Nin;
	double pi = 4.0*Nintot/Ntot;
	printf("Total number of throws=%d, number of throws within circle=%d, approximation of pi=%g\n", Ntot, Nintot, pi);
	
	return 0;
}
