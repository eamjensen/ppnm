#include<stdio.h>
#include<math.h>
#include"komplex.h"

void komplex_print(char *s, komplex a) {
	printf("%s (%g,%g)\n", s, a.re, a.im);
}

void komplex_set(komplex* z, double x, double y) {
	z->re = x;
	z->im = y;
}

komplex komplex_new(double x, double y) {
	komplex z = {x, y};
	return z;
}

komplex komplex_add(komplex a, komplex b) {
	komplex result = {a.re + b.re, a.im + b.im};
	return result;
}

komplex komplex_sub(komplex a, komplex b) {
	komplex result = {a.re - b.re, a.im - b.im};
	return result;
}

int komplex_equal(komplex a, komplex b, double acc, double eps) {
	int abs_equal = komplex_abs(komplex_sub(a, b)) < acc;
	int rel_equal = komplex_abs(komplex_sub(a, b))/(komplex_abs(a) + komplex_abs(b)) < eps/2;
	printf("abs_equal=%i, rel_equal=%i\n", abs_equal, rel_equal);
	return (abs_equal || rel_equal)? 1 : 0;
}

komplex komplex_mul(komplex a, komplex b) {
	komplex result = {a.re*b.re - a.im*b.im, a.re*b.im + a.im*b.re};
	return result;
}

komplex komplex_div(komplex a, komplex b) {
	double b_normsquare = komplex_abs(b)*komplex_abs(b);
	komplex a_times_b_star = komplex_mul(a, komplex_conjugate(b));
	komplex result = {a_times_b_star.re/b_normsquare, a_times_b_star.im/b_normsquare};
	return result;
}

komplex komplex_conjugate(komplex z) {
	komplex result = {z.re, -z.im};
	return result;
}

double komplex_abs(komplex z) {
	double result = sqrt(z.re*z.re + z.im*z.im);
	return result;
}
