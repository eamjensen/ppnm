#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int ode_swave(double r, const double y[], double dydr[], void* params) {
	double eps = *(double*) params;
	//second order equation -(1/2)*f'' -(1/r)*f = eps*f rewritten as y0' = y1 and y1' = 2*(1/r - eps)*y0 with y0 = f and y1 = f'
	dydr[0] = y[1];
	dydr[1] = -2*(1/r + eps)*y[0];
	
	return GSL_SUCCESS;
}

double Fe(double r, double eps) {
	gsl_odeiv2_system sys;
	sys.function = ode_swave;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*) &eps;

	gsl_odeiv2_driver* driver;
	double hstart = 1e-3, abs = 1e-7, rel = 1e-7;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, rel);

	//boundary condition is f(r->0) = r - r^2, ergo the boundary condition on f' is f'(r->0) = 1- 2*r
	double r0 = 1e-6;
	double y[] = {r0 - r0*r0, 1 - 2*r0};
	gsl_odeiv2_driver_apply(driver, &r0, r, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
