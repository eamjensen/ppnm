#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

//function of r with eps as parameter
double Fe(double r, double eps);

//function of eps with rmax as parameter. trying to find the root of this function i.e. varying eps to get zero
int auxfun(const gsl_vector* x, void* params, gsl_vector* f) {
	double eps = gsl_vector_get(x, 0);
	double rmax = *(double*) params;
	double f0 = Fe(rmax, eps);
	gsl_vector_set(f, 0, f0);
	
	return GSL_SUCCESS;
}
