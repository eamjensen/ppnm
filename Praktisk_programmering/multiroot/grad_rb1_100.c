#include<gsl/gsl_errno.h>
#include<gsl/gsl_vector.h>

int grad_rb1_100(const gsl_vector* x, void* params, gsl_vector* f) {
	const double x0 = gsl_vector_get(x, 0);
	const double x1 = gsl_vector_get(x, 1);

	gsl_vector_set(f, 0, 400*x0*x0*x0 + (2 - 400*x1)*x0 - 2);
	gsl_vector_set(f, 1, 200*(x1 - x0*x0));

	return GSL_SUCCESS;
}
