#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<math.h>

double Fe(double r, double eps);
int auxfun(const gsl_vector* x, void* params, gsl_vector* f);

int main(int argc, char** argv) {
	double rel = 1e-6; //precision of root finder
	double rmax = (argc > 1)? atof(argv[1]) : 10;
	double eps0 = -1.0;

	gsl_multiroot_function F;
	F.f = auxfun;
	F.n = 1;
	F.params = (void*) &rmax;
	
	gsl_multiroot_fsolver* s;
	s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, F.n);

	gsl_vector* init = gsl_vector_alloc(F.n);
	gsl_vector_set(init, 0, eps0);

	gsl_multiroot_fsolver_set(s, &F, init);

	int flag = GSL_CONTINUE, iter = 0;
	do {
		gsl_multiroot_fsolver_iterate(s);
		iter++;
		flag = gsl_multiroot_test_residual(s->f, rel);
	} while (flag == GSL_CONTINUE);

	double result = gsl_vector_get(s->x, 0);
	printf("For rmax=%g the lowest root of the auxiliary function M(eps) = Fe(rmax) is eps0=%g\n", rmax, result);
	
	const char* filename = "shoot.txt";
	printf("A table with three columns consisting of 1) r, 2) Fe(r) [the resulting function using the found root], and 3) the exact result [f(r) = r*exp(-r)] with r in the interval [%g,%g] is now printed to %s\n", 0.0, rmax, filename);
	FILE* f = fopen(filename, "w");
	int numpoints = 50;
	double r = 0, dr = rmax/numpoints;
	fprintf(f, "rmax=%g, eps0=%g\n", rmax, result);
	fprintf(f, "r\tFe\tf\n");
	for (int i = 0; i < numpoints; i++) {
		r += dr;
		fprintf(f, "%g\t%g\t%g\n", r, Fe(r, result), r*exp(-r));
	}
	fclose(f);
	printf("Please see shoot.svg for a visualization of the data\n");

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(init);
	return 0;
}
