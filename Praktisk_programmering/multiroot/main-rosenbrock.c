#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>

int grad_rb1_100(const gsl_vector* x, void* params, gsl_vector* f);

double rb1_100(double x, double y) {
	return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}

int main() {
	double eps = 1e-6;
	double x0 = 1.7, y0 = 3.3;

	gsl_multiroot_function F;
	F.f = grad_rb1_100;
	F.n = 2;
	F.params = NULL;

	gsl_multiroot_fsolver* s;
	s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, F.n);

	gsl_vector* init = gsl_vector_alloc(F.n);
	gsl_vector_set(init, 0, x0);
	gsl_vector_set(init, 1, y0);

	gsl_multiroot_fsolver_set(s, &F, init);

	int flag = GSL_CONTINUE, iter = 0;
	const char* filename = "path.txt";
	FILE* f = fopen(filename, "w");
	fprintf(f, "x\ty\tf(x,y)\n");
	double xtmp, ytmp, ftmp;
	do {
		xtmp = gsl_vector_get(s->x, 0);
		ytmp = gsl_vector_get(s->x, 1);
		ftmp = rb1_100(xtmp, ytmp);
		fprintf(f, "%g\t%g\t%g\n", xtmp, ytmp, ftmp);
		gsl_multiroot_fsolver_iterate(s);
		iter++;
		flag = gsl_multiroot_test_residual(s->f, eps);
	} while (flag == GSL_CONTINUE);

	double xres = gsl_vector_get(s->x, 0);
	double yres = gsl_vector_get(s->x, 1);
	double fres = rb1_100(xres, yres);

	printf("Extremum of Rosenbrock function with a=1 and b=100 is found to be at (x,y)=(%g,%g).\nThe function value at that point is (approximately) f(%g,%g)=%g.\nIt took %i iterations to find this point with initial guesses x0=%g and y0=%g.\n", xres, yres, xres, yres, fres, iter, x0, y0);
	printf("The points where the algorithm calculated the values of the gradient have been printed to the file %s\n", filename);
	printf("Please refer to rosenbrock.svg for a visualization of this\n");
	fprintf(f, "%g\t%g\t%g\n", xres, yres, fres);
	fclose(f);

	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(init);
	return 0;
}
