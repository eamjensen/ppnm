Extremum of Rosenbrock function with a=1 and b=100 is found to be at (x,y)=(1,1).
The function value at that point is (approximately) f(1,1)=2.31974e-19.
It took 231 iterations to find this point with initial guesses x0=1.7 and y0=3.3.
The points where the algorithm calculated the values of the gradient have been printed to the file path.txt
Please refer to rosenbrock.svg for a visualization of this
