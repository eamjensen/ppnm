#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<math.h>

double func1(double x, void* params) {
	return log(x)/sqrt(x);
}

double psi2(double x, void* params) {
	double a = *(double*) params;
	return exp(-a*x*x);
}

double Hexp(double x, void* params) {
	double a = *(double*) params;
	return 0.5*(a + (1 - a*a)*x*x)*exp(-a*x*x);
}

int main() {
	int limit = 100;
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);

	double result, abserr;
	double xmin = 0.0, xmax = 1.0, abs = 0.0, rel = 1e-7;
	gsl_function F;
	F.function = &func1;
	F.params = NULL;

	printf("PART ONE OF THE EXERCISE\n");
	printf("Integrating log(x)/sqrt(x) (which has a singularity at x=0) from x=%g to x=%g with required relative precision of %g...\n", xmin, xmax, rel);
	gsl_integration_qags(&F, xmin, xmax, abs, rel, limit,  w, &result, &abserr);
	printf("The result is %g with an estimated absolute error of %g\n", result, abserr);
	


	printf("\nPART TWO OF THE EXERCISE\n");
	printf("<psi|H|psi>/<psi|psi> can be evaluated by integrating separately in the numerator and denominator from x=0 to x=+inf (instead of x=-inf to x=+inf), since both integrands are even for psi=exp(-alpha*x^2/2) and H equal to the harmonic oscillator Hamiltonian.\nWe find the expectation value for various values of alpha, requiring a relative precision of %g...\n", rel);
	//we integrate two separate functions Fnum (numerator) and Fden (denominator) in the same workspace w, but storing each integration result in resnum and resden, etc.
	double resnum, resden, errnum, errden, alpha; 
	gsl_function Fnum, Fden;
	Fnum.function = &Hexp;
	Fden.function = &psi2;
	Fnum.params = (void*) &alpha;
	Fden.params = (void*) &alpha;
	double parammin = 0.1, parammax = 3.0;

	printf("alpha\tE(alpha)\n");
	double res, minres = 10000.0, minalpha;
	for (alpha = parammin; alpha < parammax; alpha += 0.1) {
		//we do not use errnum or errden for anything...
		gsl_integration_qagiu(&Fnum, xmin, abs, rel, limit, w, &resnum, &errnum);
		gsl_integration_qagiu(&Fden, xmin, abs, rel, limit, w, &resden, &errden);
		res = resnum/resden;
		printf("%g\t%g\n", alpha, res);
		if (res < minres) {
			minres = res; //record the smallest result
			minalpha = alpha;
		}
	}
	printf("The lowest energy, E(alpha)=%g, was achieved for alpha=%g\n", minres, minalpha);
	printf("This is not wholly surprising, since exp(-x^2/2) is *the* solution for the lowest energy level (0.5) of the harmonic oscillator in the given units\n");
	printf("A plot of the above expectation values vs. alpha can be seen in expectation.svg\n");

	gsl_integration_workspace_free(w);
	return 0;
}
