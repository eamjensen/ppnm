#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(int argc, char** argv) {
	if (argc < 2) {
		printf("Please supply some arguments next time.\n");
	} else {
		double x;
		for (int i = 1; i < argc; i++) {
			x = atof(argv[i]);
			printf("% lg\t% lg\n", x, sin(x));
		}
	}

	return 0;
}
