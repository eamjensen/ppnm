#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<math.h>

int max = INT_MAX/2;

void determine_maximum_integer();
void determine_minimum_integer();
void compare_integer_with_INT_MIN(int i);
void determine_machine_epsilons();
void compare_float_with_FLT_EPSILON(float f);
void compare_double_with_DBL_EPSILON(double d);
void compare_long_double_with_LDBL_EPSILON(long double ld);

void partial_harmonic_float();
void partial_harmonic_double();

int equal();

int main() {
	printf("PART ONE OF THE EXERCISE\n");
	determine_maximum_integer();
	determine_minimum_integer();
	determine_machine_epsilons();
	
	printf("\nPART TWO OF THE EXERCISE\n");
	partial_harmonic_float();
	printf("sum_down_float ends up being of larger (and more correct) value because the smallest values are added to each other first. Floating point numbers have limited precision, and adding, at each of the later iterations, a very tiny contribution to a much larger number in sum_up_float actually does not change the value of sum_up_float at all, because the precisions are incompatible, so to speak. Doing it the other way around, starting by adding up the tiniest contributions to sum_down_float, ensures that the tiniest contributions will sequentually add up to relatively large values which are not completely wasted when added to a larger value on each following iteration.\n");
	printf("The harmonic series does not converge. Therefore, calculating the partial sum for a sufficiently high number of terms will always have some imprecision no matter the number of (finite) bits used to represent it.\n");
	partial_harmonic_double();
	printf("This time, the two methods yields much more similar results. There is still a small difference, however. The results are more similar here, because the number of terms (the integer 'max') in the partial sum is better suited for the precision within the type double.\n");

	printf("\nPART THREE OF THE EXERCISE\n");
	printf("If, in the following line, the number 1 is printed, then the numbers 3.0 and 5.0 are equal with absolute precision 10.0 OR are equal with relative precision 0.10 (the first case is true, the second isn't).\n");
	printf("%i\n", equal(3.0, 5.0, 10.0, 0.10));

	return 0;
}

/* Determine the maximum possible value of an integer using both a while loop, a for loop and a do-while loop. */
void determine_maximum_integer() {	
	int i = 0;
	while (i + 1 > i) {
		i++;
	}
	printf("maximum integer (while loop) = %i\n", i);
	
	int j;
	for (j = 0; j + 1 > j; j++) {

	}
	printf("maximum integer (for loop) = %i\n", j);
	
	int k = 0;
	do {
		k++;
	} while (k + 1 > k);
	printf("maximum integer (do-while loop) = %i\n", k);
}

/* Determine the minimum possible value of an integer using both a while loop, a for loop and a do-while loop. In addition, compare the determined minima with INT_MIN from limits.h. */
void determine_minimum_integer() {	
	int i = 0;
	while (i - 1 < i) {
		i--;
	}
	printf("minimum integer (while loop) = %i\n", i);
	compare_integer_with_INT_MIN(i);

	int j;
	for (j = 0; j - 1 < j; j--) {
		
	}
	printf("minimum integer (for loop) = %i\n", j);
	compare_integer_with_INT_MIN(j);
	
	int k = 0;
	do {
		k--;
	} while (k - 1 < k);
	printf("minimum integer (do-while loop) = %i\n", k);
	compare_integer_with_INT_MIN(k);
}

void compare_integer_with_INT_MIN(int i) {
	printf("Comparing given integer with INT_MIN=%i from the header file limits.h... ", INT_MIN);
	(INT_MIN == i)? printf("They are equal\n") : printf("They are NOT equal\n");
}

/* Determine the machine epsilons for floats, doubles and long doubles using both a while loop, a for loop and a do-while loop. In addition, compare each of the determined machine epsilons respectively with FLT_EPSILON, DBL_EPSILON and LDBL_EPSILON from float.h. */
void determine_machine_epsilons() {
	float x = 1.0f;
	double y = 1.0;
	long double z = 1.0L;
	while (1 + x != 1) {
		x /= 2;
	}
	x *= 2;
	while (1 + y != 1) {
		y /= 2;
	}
	y *= 2;
	while (1 + z != 1) {
		z /= 2;
	}
	z *= 2;
	printf("machine epsilon for floats (while loop) = %g\n", x);
	printf("machine epsilon for doubles (while loop) = %g\n", y);
	printf("machine epsilon for long doubles (while loop) = %Lg\n", z);
	compare_float_with_FLT_EPSILON(x);
	compare_double_with_DBL_EPSILON(y);
	compare_long_double_with_LDBL_EPSILON(z);

	float m;
	double n;
	long double o;
	for (m = 1.0f; 1 + m != 1; m /= 2) {
		
	}
	m *= 2;
	for (n = 1.0; 1 + n != 1; n /= 2) {

	}
	n *= 2;
	for (o = 1.0L; 1 + o != 1; o /= 2) {

	}
	o *= 2;
	printf("machine epsilon for floats (for loop) = %g\n", m);
	printf("machine epsilon for doubles (for loop) = %g\n", n);
	printf("machine epsilon for long doubles (for loop) = %Lg\n", o);
	compare_float_with_FLT_EPSILON(m);
	compare_double_with_DBL_EPSILON(n);
	compare_long_double_with_LDBL_EPSILON(o);
	
	float u = 1.0f;
	double v = 1.0;
	long double w = 1.0L;
	do {
		u /= 2;
	} while (1 + u != 1);
	u *= 2;
	do {
		v /= 2;
	} while (1 + v != 1);
	v *= 2;
	do {
		w /= 2;
	} while (1 + w != 1);
	w *= 2;
	printf("machine epsilon for floats (do-while loop) = %g\n", u);
	printf("machine epsilon for doubles (do-while loop) = %g\n", v);
	printf("machine epsilon for long doubles (do-while loop) = %Lg\n", w);
	compare_float_with_FLT_EPSILON(u);
	compare_double_with_DBL_EPSILON(v);
	compare_long_double_with_LDBL_EPSILON(w);
}
void compare_float_with_FLT_EPSILON(float f) {
	printf("Comparing given float with FLT_EPSILON=%.5e from the header file float.h... ", FLT_EPSILON);
	(FLT_EPSILON == f)? printf("They are equal\n") : printf("They are NOT equal\n");
}

void compare_double_with_DBL_EPSILON(double d) {
	printf("Comparing given double with DBL_EPSILON=%g from the header file float.h... ", DBL_EPSILON);
	(DBL_EPSILON == d)? printf("They are equal\n") : printf("They are NOT equal\n");
}	

void compare_long_double_with_LDBL_EPSILON(long double ld) {
	printf("Comparing given long double with LDBL_EPSILON=%Lg from the header file float.h... ", LDBL_EPSILON);
	(LDBL_EPSILON == ld)? printf("They are equal\n") : printf("They are NOT equal\n");
}

void partial_harmonic_float() {
	float sum_up_float = 0, sum_down_float = 0;
	for (int i = 1; i <= max; i++) {
		sum_up_float += 1.0f/i;
	}
	for (int i = 0; i < max; i++) {
		sum_down_float += 1.0f/(max - i);
	}
	printf("sum_up_float=%g, sum_down_float=%g\n", sum_up_float, sum_down_float);
}

void partial_harmonic_double() {
	double sum_up_double = 0.0, sum_down_double = 0.0;
	for (int i = 1; i <= max; i++) {
		sum_up_double += 1.0/i;
	}
	for (int i = 0; i < max; i++) {
		sum_down_double += 1.0/(max - i);
	}
	if (fabs(sum_up_double - sum_down_double) > DBL_EPSILON) {
		printf("sum_up_double and sum_down_double are NOT equal to each other (their absolute difference is larger than the machine epsilon for doubles).\n");
	} else {
		printf("sum_up_double and sum_down_double are equal to each other within the precision of the type double.\n");
	}
	printf("sum_up_double=%.14g, sum_down_double=%.14g\n", sum_up_double, sum_down_double);
}
