#include<math.h>

int equal(double a, double b, double tau, double epsilon) {
	return (fabs(a - b) < tau || fabs(a - b)/(fabs(a) + fabs(b)) < epsilon/2)? 1 : 0;
}
