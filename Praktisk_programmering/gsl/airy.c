#include<stdio.h>
#include<gsl/gsl_sf_airy.h>

int main() {
	double xmin = -15;
	double xmax = 5;
	int numspacings = 500; //number of points is numspacings + 1, including both xmin and xmax
	double a, b, x;
	for (int i = 0; i <= numspacings; i++) {
		x = xmin + i*((xmax - xmin)/numspacings);
		a = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
		b = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
		printf("%g\t%g\t%g\n", x, a, b);
	}

	return 0;
}
