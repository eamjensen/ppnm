#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>

int main() {
	const int n = 3;
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n, n);
	gsl_vector* x = gsl_vector_alloc(n);
	
	FILE* vdat = fopen("vector.dat", "r");
	gsl_vector_fscanf(vdat, b);
	fclose(vdat);
	FILE* mdat = fopen("matrix.dat", "r");
	gsl_matrix_fscanf(mdat, A);
	fclose(mdat);
	// HH solver destroys the input matrix, so copy A into another matrix M
	gsl_matrix* M = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(M, A);
	
	printf("looking for a solution to A*x=b with\n");
	printf("A (a matrix) = \n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (j < n - 1) {
				printf("% 3.2f\t", gsl_matrix_get(A, i, j));
			} else {
				printf("% 3.2f\n", gsl_matrix_get(A, i, j));
			}
		}
	}
	printf("and\n");
	printf("b (a column vector) = (");
	for (int i = 0; i < n; i++) {
		if (i < n - 1) {
			printf("%3.2f, ", gsl_vector_get(b, i));
		} else {
			printf("%3.2f)\n", gsl_vector_get(b, i));
		}
	}

	printf("\nsolving...\n");
	gsl_linalg_HH_solve(M, b, x);
	printf("x = (");
	for (int i = 0; i < n; i++) {
		if (i < n - 1) {
			printf("%3.2f, ", gsl_vector_get(x, i));
		} else {
			printf("%3.2f)\n", gsl_vector_get(x, i));
		}
	}
	
	printf("\nchecking validity of solution...\n");
	gsl_vector* y = gsl_vector_alloc(n);
	//d = double precision, ge = general matrix, mv = matrix-vector-multiplication
	//expression being evaluated here is (number signifying the argument number) 6 = 2*3*4 + 5*6, when the first argument has the value CblasNoTrans (i.e. do not (conjugate) transpose the third argument)
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, y);
	printf("A*x = y = (");
	for (int i = 0; i < n; i++) {
		if (i < n - 1) {
			printf("%3.2f, ", gsl_vector_get(y, i));
		} else {
			printf("%3.2f)\n", gsl_vector_get(y, i));
		}
	}

	gsl_vector_free(b);
	gsl_matrix_free(A);
	gsl_vector_free(x);
	gsl_matrix_free(M);
	gsl_vector_free(y);
	return 0;
}
