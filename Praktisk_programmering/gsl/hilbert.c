#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_blas.h>

int main() {
	const int n = 4;
	gsl_matrix* H = gsl_matrix_alloc(n, n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			gsl_matrix_set(H, i, j, 1.0/((i + 1) + (j + 1) - 1));
		}
	}
	
	//the eigenvalue and -vector solver partially destroys the input matrix. copy H into M
	gsl_matrix* M = gsl_matrix_alloc(n, n);
	gsl_matrix_memcpy(M, H);
	//prepare vector to store eigenvalues and matrix to store eigenvectors
	gsl_vector* eval = gsl_vector_alloc(n);
	gsl_matrix* evec = gsl_matrix_alloc(n, n);
	//use the solver (on our SYMMetric matrix)
	gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(n);
	gsl_eigen_symmv(M, eval, evec, w);

	printf("H (the Hilbert matrix of 4th order) = \n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (j < n - 1) {
				printf("%4.3f\t", gsl_matrix_get(H, i, j));
			} else {
				printf("%4.3f\n", gsl_matrix_get(H, i, j));
			}
		}
	}
	printf("has eigenvalues\n");
	for (int i = 0; i < n; i++) {
		if (i < n - 1) {
			printf("%8.6f, ", gsl_vector_get(eval, i));
		} else {
			printf("%8.6f\n", gsl_vector_get(eval, i));
		}
	}
	printf("and corresponding eigen(column)vectors\n");
	gsl_vector_view view;
	for (int i = 0; i < n; i++) {
		view = gsl_matrix_column(evec, i);
		printf("(");
		for (int j = 0; j < n; j++) {
			if (j < n - 1) {
				printf("% 8.6f, ", gsl_vector_get(&view.vector, j));
			} else {
				printf("% 8.6f)\n", gsl_vector_get(&view.vector, j));
			}
		}
	}

	printf("\nv is an eigenvector of H if H*v=c*v, where c is the corresponding eigenvalue. So (1/c)*H*v should give the eigenvector itself.\n");
	printf("Performing this check on the four given eigenvectors with corresponding eigenvalues...\n");

	gsl_vector* u = gsl_vector_alloc(n);
	double c;
	for (int i = 0; i < n; i++) {
		view = gsl_matrix_column(evec, i);
		c = gsl_vector_get(eval, i);
        	//d = double precision, ge = general matrix, mv = matrix-vector-multiplication
        	//expression being evaluated here is (number signifying the argument number) 6 = 2*3*4 + 5*6, when the first argument has the value CblasNoTrans (i.e. do not (conjugate) transpose the third argument)
        	gsl_blas_dgemv(CblasNoTrans, 1/c, H, (const gsl_vector*) &view, 0, u);
        	printf("(1/c)*H*v = u = (");
        	for (int j = 0; j < n; j++) {
                	if (j < n - 1) {
                        	printf("%8.6f, ", gsl_vector_get(u, j));
                	} else {
                        	printf("%8.6f)\n", gsl_vector_get(u, j));
                	}
        	}
	}
	
	gsl_vector_free(u);
	gsl_matrix_free(H);
	gsl_matrix_free(M);
	gsl_vector_free(eval);
	gsl_matrix_free(evec);
	gsl_eigen_symmv_free(w);
	return 0;
}
