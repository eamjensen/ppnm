/*
 * Copied the example on Ordinary Differential Equations from the GSL Manual (https://www.gnu.org/software/gsl/doc/html/ode-initval.html#examples) and modified it as needed for this exercise
 */

#include<stdio.h>
#include<stdlib.h>

double errfun(double x);

int main(int argc, char** argv) {
	if (argc != 4) {
		printf("Please provide three numbers as arguments\n");
		return 0;
	}

	double a = atof(argv[1]), b = atof(argv[2]), dx = atof(argv[3]);
	if (a > b) {
		printf("First argument must be smaller than second argument\n");
		return 0;
	}

	double res;
	printf("x\tu(x)\n");
	for (double xtmp = a; xtmp < b + dx; xtmp += dx) {
		res = errfun(xtmp);
		printf("%g\t%g\n", xtmp, res);
	}

	return 0;
}
