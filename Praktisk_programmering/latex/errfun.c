/*
 * Copied the example on Ordinary Differential Equations from the GSL Manual (https://www.gnu.org/software/gsl/doc/html/ode-initval.html#examples) and modified it as needed for this exercise
 */

#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>

int diff(double x, const double u[], double dudx[], void *params) {
	dudx[0] = (2/sqrt(M_PI))*exp(-x*x);
	return GSL_SUCCESS;
}

double errfun(double x) {
	if (x == 0.0) {
		return 0.0; //this is given. 
		/*
		 * The main C program of this exercise does not, however, give a value x 
		 * close enough to zero for the condition to be satisfied.
		 * I considered comparing x to the machine epsilon but decided against it.
		 */
	} else if (x < 0.0) { //odd function
		return -errfun(-x);
	}

	double x0 = 0.0; //initial conditions on 'diff'
	double u[1] = {0.0};

	gsl_odeiv2_system sys = {diff, NULL, 1, NULL};

	gsl_odeiv2_driver* d;
	double initstepsize = 1e-3, abs = 1e-6, rel = 0;
	d = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, initstepsize, abs, rel);

	gsl_odeiv2_driver_apply(d, &x0, x, u);
	
	gsl_odeiv2_driver_free(d);
	return u[0];
}
