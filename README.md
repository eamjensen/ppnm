# Praktisk programmering og numeriske metoder

This project contains solutions (made by Erik A. M. Jensen, studienummer 201504997) to the exercises and the examination project in the course *Praktisk programmering og numeriske metoder* ([course website](http://62.107.14.89/~fedorov/prog/)).

**NOTE:**
For assessment, reports (always with filename "report.pdf") are assumed to have number 1 priority, followed by txt-files with "out" as their prefix.
Generally, report texts or txt-files refers to figures or other files as they become relevant in a given exercise.

## 1. General
The project is split into three directories, *Praktisk_programmering*, *Numeriske_metoder* and *exam*. 

The directories *Praktisk_programmering* and *Numeriske_metoder* contain a number of subdirectories, each containing a solution to each of the given exercises in the two parts of the course.
The directory *exam* contains a solution to the given examination project.

The solutions to the exercises are to be build and run on a POSIX system equipped with the *GNU C compiler (GCC)*, *gnuplot*, *GNU Scientific Library (GSL)* and *Tex Live*.

Output resulting from building and executing the programs is already included in each exercise subdirectory (except for object files and executables).

When a reference is made to a certain equation, page or similar in the code comments, the reference is to the [lecture notes](http://62.107.14.89/~fedorov/prog/numeric/book/main.pdf) unless otherwise specified.

## 2. Praktisk programmering

In the *Praktisk_programmering* directory the exercises are not ordered.
For reference, here is a numbered list of the exercises in the order they were given:

1. [hello](http://62.107.14.89/~fedorov/prog/exercise-hello.htm)
2. [math](http://62.107.14.89/~fedorov/prog/exercise-math.htm)
3. [epsilon](http://62.107.14.89/~fedorov/prog/exercise-epsilon.htm)
4. [komplex](http://62.107.14.89/~fedorov/prog/exercise-komplex.php)
5. [nvector](http://62.107.14.89/~fedorov/prog/exercise-nvector.htm)
6. [io-gnuplot](http://62.107.14.89/~fedorov/prog/exercise-io-gnuplot.htm)
7. [gsl](http://62.107.14.89/~fedorov/prog/exercise-gsl.htm)
8. [orbit](http://62.107.14.89/~fedorov/prog/exercise-orbit.htm)
9. [integ](http://62.107.14.89/~fedorov/prog/exercise-integ.htm)
10. [multiroot](http://62.107.14.89/~fedorov/prog/exercise-multiroot.htm)
11. [optimization](http://62.107.14.89/~fedorov/prog/exercise-optim.htm)
12. [multiprocessing](http://62.107.14.89/~fedorov/prog/exercise-mp.htm)
13. [latex](http://62.107.14.89/~fedorov/prog/exercise-latex.htm)
14. [debug](http://62.107.14.89/~fedorov/prog/exercise-debug.htm)
15. [big-programming-exercise](http://62.107.14.89/~fedorov/prog/plan-debug.htm)

The following optional parts of the *Praktisk programmering* exercises have not been done:

>3.4, 8.3, 10.2.B-C, 11.2.C-D, 12.D

## 3. Numeriske metoder

Similarly, in the *Numeriske_metoder* directory the exercises are not ordered.
For reference, here is a numbered list of the exercises in the order they were given:

1. [interp](http://62.107.14.89/~fedorov/prog/numeric/ex-interp.htm)
2. [lineq](http://62.107.14.89/~fedorov/prog/numeric/ex-lineq.htm)
3. [eigen](http://62.107.14.89/~fedorov/prog/numeric/ex-eigen.htm)
4. [leastsq](http://62.107.14.89/~fedorov/prog/numeric/ex-leastsq.htm)
5. [roots](http://62.107.14.89/~fedorov/prog/numeric/ex-roots.htm)
6. [optim](http://62.107.14.89/~fedorov/prog/numeric/ex-optim.htm)
7. [odes](http://62.107.14.89/~fedorov/prog/numeric/ex-odes.htm)
8. [adapt](http://62.107.14.89/~fedorov/prog/numeric/ex-adapt.htm)
9. [montecarlo](http://62.107.14.89/~fedorov/prog/numeric/ex-montecarlo.htm)
10. [ann](http://62.107.14.89/~fedorov/prog/numeric/ex-ann.htm)

The following parts of the *Numeriske metoder* exercises have not been done:

> 9.C, 10.C-D

## 4. Examination project

The examination project which has been assigned to me is exercise number 5 (= (97 mod 23)) from the [list of examination exercises](http://62.107.14.89/~fedorov/prog/numeric/ex-exam.php): **Hessenberg factorization of a real square matrix using Jacobi transformations**.