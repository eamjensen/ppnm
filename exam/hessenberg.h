#include<gsl/gsl_matrix.h>
#ifndef HESSENBERG_H
#define HESSENBERG_H
void lower_hessenberg_decomp(gsl_matrix* A, gsl_matrix* Q);
double lower_hessenberg_determinant(const gsl_matrix* H);
#endif
