\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{babel}
\usepackage{graphicx}
\begin{document}

\title{Report on examination project, exercise 5:\\\large{\emph{Hessenberg factorization of a real square matrix using Jacobi transformations}}}
\date{\today}
\author{Erik A. M. Jensen, studienummer 201504997}
\maketitle

\begin{abstract}
This report describes the problem of factorizing a general square real matrix $A$ into \emph{lower Hessenberg form} $H$, such that $A=QHQ^{T}$ with $Q$ an orthogonal matrix.
One possible solution, achieved by a succession of Jacobi transformations, is described, and an implementation of the solution in a C program is then briefly outlined.
The results of some simple tests of the \emph{lower Hessenberg factorization} routine, implemented in the C program, is then presented.
The execution times for various matrix dimensions of the lower Hessenberg factorization routine is then compared to those of $QR$ factorization.
The calculation of determinants of lower Hessenberg matrices is also described and demonstrated.
\end{abstract}

\section{The problem}

The main goal of this report is to describe the theory behind and implementation of a computational method which performs a \emph{lower Hessenberg factorization} of a general real $n\times n$ matrix $A$.
This means that $A$ is to be represented in the form $A=QHQ^{T}$ where $Q$ is an orthogonal
$n\times n$ matrix and $H$ is a \emph{lower Hessenberg matrix},

\begin{equation}
H=\begin{pmatrix}H_{11} & H_{12} & 0 & 0 & \cdots & 0\\
H_{21} & H_{22} & H_{23} & 0 & \cdots & 0\\
\vdots & \vdots & \vdots & \ddots & \ddots & \vdots\\
H_{n-2,1} & H_{n-2,2} & H_{n-2,3} & \cdots & H_{n-2,n-1} & 0\\
H_{n-1,1} & H_{n-1,2} & H_{n-1,3} & \cdots & H_{n-1,n-1} & H_{n-1,n}\\
H_{n,1} & H_{n,2} & H_{n,3} & \cdots & H_{n,n-1} & H_{n,n}
\end{pmatrix};\label{eq:hess}
\end{equation}
\\
that is, a lower triangular matrix which also has non-zero elements in the first superdiagonal.

\section{A solution}

One way of determining $H$ and $Q$ is by a specific succession of Jacobi transformations of $A$:
On each transformation of $A$, the rotation angle is chosen such that a non-zero element above $A$'s
first superdiagonal is zeroed.
When all the elements above $A$'s first superdiagonal have been zeroed in this way, $A$ has been transformed into the lower Hessenberg matrix $H$.
The succession of Jacobi transformations $J_{1},J_{2},\ldots$ yields the total orthogonal transformation matrix $Q$ as the product of successive Jacobi transformations, $Q=J_{1}J_{2}\cdots$.

Getting more specific now, consider a Jacobi transformation $A\rightarrow A'=J(p,q)^{T}AJ(p,q)$ where the rotation angle $\phi$ is chosen not such that the $(p,q)$'th element of the matrix $A$ is zeroed (as in section 3.2.1 in the notes), but instead such that the $(p-1,q)$'th element of the matrix $A$ is zeroed.
The Jacobi transformation $J(2,3)$ will then zero the $(1,3)$'th element of $A$, the transformation $J(2,4)$ will zero the $(1,4)$'th element, and so forth.
In particular, the sequence of Jacobi transformations

\begin{equation}
J(2,3),J(2,4),\ldots,J(2,n);J(3,4),\ldots,J(3,n);\ldots;J(n-1,n)\label{eq:jsequence}
\end{equation}
\\
will zero the elements of the following indices, in order, 

\begin{equation}
(1,3),(1,4),\ldots,(1,n);(2,4),\ldots,(2,n);\ldots;(n-2,n).\label{eq:isequence}
\end{equation}
\\
Each zeroing of an element performed in this order does not make a previously zeroed element different from zero again.
Comparing the sequence of indices in (\ref{eq:isequence}) with the form of $H$ in (\ref{eq:hess}), we see that a given real $n\times n$ matrix $A$ subjected to the sequence of Jacobi transformations in (\ref{eq:jsequence}) will become a lower Hessenberg matrix $H$.
(If $A$ is symmetric, it becomes a tridiagonal matrix $H$.)

In order to utilize this, we need to figure out how to pick the correct rotation angles for each of the successive Jacobi transformations in (\ref{eq:jsequence}). Generalizing the transformed matrix elements $A'_{ij}$ from page 27 in the notes to a matrix $A$ which is \emph{not} symmetric yields

\begin{eqnarray}
A'_{ij} & = & A_{ij}\forall i\neq p,q\land j\neq p,q\\
A'_{pi} & = & cA_{pi}-sA_{qi}\forall i\neq p,q\label{eq:Api}\\
A'_{ip} & = & cA_{ip}-sA_{iq}\forall i\neq p,q\\
A'_{qi} & = & sA_{pi}+cA_{qi}\forall i\neq p,q\\
A'_{iq} & = & sA_{ip}+cA_{iq}\forall i\neq p,q\label{eq:Aiq}\\
A'_{pp} & = & c^{2}A_{pp}-sc(A_{pq}+A_{qp})+s^{2}A_{qq}\\
A'_{qq} & = & s^{2}A_{pp}+sc(A_{pq}+A_{qp})+c^{2}A_{qq}\\
A'_{pq} & = & sc(A_{pp}-A_{qq})+c^{2}A_{pq}-s^{2}A_{qp}\\
A'_{qp} & = & sc(A_{pp}-A_{qq})+c^{2}A_{qp}-s^{2}A_{pq},\label{eq:Aqp}
\end{eqnarray}

where $c\equiv\cos\phi$ and $s\equiv\sin\phi$ as in the notes.
By eq. (\ref{eq:Aiq}), zeroing the $(p-1,q)$'th element of $A$ gives us

\begin{equation}
A'_{p-1,q}=sA_{p-1,p}+cA_{p-1,q}=0\Rightarrow\tan\phi=-\frac{A_{p-1,q}}{A_{p-1,p}}.\label{eq:angle}
\end{equation}

With (\ref{eq:angle}) we can now find the rotation angle $\phi$ for each successive Jacobi transformation in (\ref{eq:jsequence}).

\section{Implementation}

The method to achieve a lower Hessenberg factorization of a general real $n\times n$ matrix $A$, outlined in the previous section, has been implemented in a C function with signature
\begin{quote}
\texttt{void lower\_hessenberg\_decomp(gsl\_matrix{*} A, gsl\_matrix{*}
Q)}. 
\end{quote}
For the manipulation of matrices, the function uses the matrix data type and the matrix manipulation functions defined in \emph{GNU Scientific Library} (GSL).
The function takes the $n\times n$ matrix $A$ which is to be factorized and another $n\times n$ matrix $Q$ (with arbitrary elements) as its arguments.
When the function returns, $A$ has been transformed into the lower Hessenberg matrix $H$ and the accumulated total transformation matrix is stored in $Q$.
Performing the matrix multiplication $QHQ^{T}$ will then yield $A$ again.

The function loops over the rows $p=2,...,n-1$, and the columns $q=p+1,...,n$ as in (\ref{eq:jsequence}).
On each iteration, first the rotation angle $\phi$ of the Jacobi transformation to be carried out is determined by means of eq. (\ref{eq:angle}).
Then, the transformation matrix $Q$ (initialized as the identity matrix) is transformed as in (3.11) in the notes (with $V_{ij}=Q_{ij}$, etc.).
And finally, the transformations (\ref{eq:Api})-(\ref{eq:Aqp}) are carried out.
Along the way, the $(p-1,q)$'th element of $A$ will be zeroed by virtue of the chosen rotation angle $\phi$.

As a test, the matrix

\begin{equation}
A=\begin{pmatrix}0.8402 & 0.3944 & 0.7831 & 0.7984 & 0.9116\\
0.1976 & 0.3352 & 0.7682 & 0.2778 & 0.5540\\
0.4774 & 0.6289 & 0.3648 & 0.5134 & 0.9522\\
0.9162 & 0.6357 & 0.7173 & 0.1416 & 0.6070\\
0.0163 & 0.2429 & 0.1372 & 0.8042 & 0.1567
\end{pmatrix},\label{eq:Atest}
\end{equation}
\\
which is \emph{not} symmetric, was pseudo-randomly generated and given as input to the C function along with another $5\times5$ matrix $Q$ in which to store the total transformation matrix.
The result of the lower Hessenberg factorization was

\[
H=\begin{pmatrix}0.8402 & 1.4958 & 0.0000 & 0.0000 & 0.0000\\
0.8010 & 1.8219 & 0.4421 & 0.0000 & 0.0000\\
-0.0476 & 0.4667 & -0.0789 & 0.3559 & 0.0000\\
-0.1504 & 0.3105 & 0.0484 & -0.2642 & 0.2160\\
-0.6634 & -0.4480 & -0.2606 & -0.5111 & -0.4805
\end{pmatrix}
\]
\\
and

\[
Q=\begin{pmatrix}1.0000 & 0.0000 & 0.0000 & 0.0000 & 0.0000\\
0.0000 & 0.2637 & 0.9605 & 0.0636 & -0.0627\\
0.0000 & 0.5235 & -0.2122 & 0.7862 & -0.2505\\
0.0000 & 0.5338 & -0.1466 & -0.5841 & -0.5936\\
0.0000 & 0.6095 & -0.1049 & -0.1913 & 0.7622
\end{pmatrix}.
\]
\\
The matrix products $Q^{T}Q$ and $QQ^{T}$ yields the identity matrix, and the matrix product $QHQ^{T}$ yields $A$ again.
The determinant of $H$ (which is the same as the determinant of $A$) was also calculated.
The result was

\begin{equation}
\det(H)=-0.1164.\label{eq:det1}
\end{equation}
\\
The method used to calculate the determinant is described in the next section.

The same kind of test was made on the symmetric matrix

\[
A=\begin{pmatrix}0.4009 & 0.1298 & 0.1088 & 0.9989 & 0.2183\\
0.1298 & 0.5129 & 0.8391 & 0.6126 & 0.2960\\
0.1088 & 0.8391 & 0.6376 & 0.5243 & 0.4936\\
0.9989 & 0.6126 & 0.5243 & 0.9728 & 0.2925\\
0.2183 & 0.2960 & 0.4936 & 0.2925 & 0.7714
\end{pmatrix},
\]
\\
yielding a tridiagonal matrix 

\[
H=\begin{pmatrix}0.4009 & 1.0364 & 0.0000 & 0.0000 & 0.0000\\
1.0364 & 1.3852 & 0.9504 & 0.0000 & 0.0000\\
0.0000 & 0.9504 & 1.2632 & 0.1876 & 0.0000\\
0.0000 & 0.0000 & 0.1876 & 0.3314 & 0.3497\\
0.0000 & 0.0000 & 0.0000 & 0.3497 & -0.0852
\end{pmatrix},
\]
\\
which has determinant

\begin{equation}
\det(H)=0.1516.\label{eq:det2}
\end{equation}


\section{Extra}

The results of a test of the execution times of Hessenberg factorization, implemented above, and $QR$ factorization, implemented previously in the course in the exercise \emph{lineq}, will now be presented.
In the test, two separate C programs would each generate the same pseudo-random square matrix $A$ of a given dimension $n$ and then either Hessenberg or $QR$ factorize it.
For various dimensions $n$, the amount of real time it took for each of the two factorization routines to execute was gauged with the Bash command \texttt{time}.
The results can be seen in figure \ref{fig:timing}.

The complexity of both factorization routines are $O(n^{3})$, but Hessenberg factorization has many more operations per loop iteration.
This clearly has an impact.
We can conclude that if $QR$ factorization is a possibility in a given problem, then it is clearly favourable as compared to Hessenberg factorization.

\begin{figure}
	\centering
	\includegraphics[width=0.80\columnwidth]{timing.pdf}
	\caption{Execution times of Hessenberg factorization and of $QR$ factorization of identical pseudo-randomly generated asymmetric square matrices of various dimensions.}
	\label{fig:timing}
\end{figure}

Finally, the method used to calculate the determinants of lower Hessenberg matrices $H$ in the previous section will now be described.
Let us consider a $5\times5$ lower Hessenberg matrix $H$ as an example.
The determinant of this matrix is seen to be (by expanding the determinant along the first row)

\begin{eqnarray*}
\det(H) & = & \begin{vmatrix}H_{11} & H_{12} & 0 & 0 & 0\\
H_{21} & H_{22} & H_{23} & 0 & 0\\
H_{31} & H_{32} & H_{33} & H_{34} & 0\\
H_{41} & H_{42} & H_{43} & H_{44} & H_{45}\\
H_{51} & H_{52} & H_{53} & H_{54} & H_{55}
\end{vmatrix}\\
\\
 & = & H_{11}\begin{vmatrix}H_{22} & H_{23} & 0 & 0\\
H_{32} & H_{33} & H_{34} & 0\\
H_{42} & H_{43} & H_{44} & H_{45}\\
H_{52} & H_{53} & H_{54} & H_{55}
\end{vmatrix}-H_{12}\begin{vmatrix}H_{21} & H_{23} & 0 & 0\\
H_{31} & H_{33} & H_{34} & 0\\
H_{41} & H_{43} & H_{44} & H_{45}\\
H_{51} & H_{53} & H_{54} & H_{55}
\end{vmatrix}\\
\\
 & = & H_{11}\det(M_{1,1})-H_{12}\det(M_{1,2}),
\end{eqnarray*}
\\
where $M_{1,1}$ and $M_{1,2}$ are defined as the $(1,1)$'th and $(1,2)$'th minors of the matrix $H$.
We now have two terms, each containing a determinant.
The determinants can again, referring to their form after the second equals sign above, with benefit be expanded along their respective first rows.
Expanding either of these two determinants along their first row will result in two new terms of the same form as the expression after the second equals sign above;
for example, expanding the first of the two determinants yields $\det(M_{1,1})=H_{22}\det(\tilde{M}_{1,1})-H_{23}\det(\tilde{M}_{1,2})$, where $\tilde{M}_{1,1}$ and $\tilde{M}_{1,2}$ are now defined as the $(1,1)$'th and $(1,2)$'th minors of the matrix $M_{1,1}$.
If we keep expanding all determinants along their first rows, we will wind up with the two terms after the second equals sign above each recursively containing two similar terms, right until we reach determinants of dimension $2$.
Then, all the remaining determinants will be of the form

\[
\begin{vmatrix}a & b\\
c & d
\end{vmatrix},
\]
\\
which we know has the simple result $ad-bc$.

Generalizing the above considerations to any lower Hessenberg matrix $H$ of dimension $n\times n$ with $n\geq2$, the determinant of any lower Hessenberg matrix $H$ can be computed recursively as

\begin{equation}
\det(H)=\begin{cases}
H_{11}H_{22}-H_{12}H_{21} & \textrm{if}\,n=2,\\
H_{11}\det(M_{1,1})-H_{12}\det(M_{1,2}) & \textrm{if}\,n>2,
\end{cases}\label{eq:detalg}
\end{equation}
\\
where $M_{1,1}$ and $M_{1,2}$ are the $(1,1)$'th and $(1,2)$'th minors of $H$. These two minors are each redefined as $H$ when $\det(M_{1,1})$ and $\det(M_{1,2})$ is called.

The method described in (\ref{eq:detalg}) has been implemented in a C function with signature
\begin{quote}
\texttt{double lower\_hessenberg\_determinant(const gsl\_matrix{*}
H)}.
\end{quote}
The function takes a lower Hessenberg matrix $H$ as its argument, and it returns the determinant of $H$, $\det(H)$, as a double-precision number.
This function was used to calculate the determinants of the two matrices $H$ in (\ref{eq:det1}) and (\ref{eq:det2}).

To ensure that the function was implemented correctly, the first test matrix $A$ from above, eq. (\ref{eq:Atest}), was also $QR$ factorized.
The determinant of $R$ was then calculated and compared to the calculated determinant of $H$.
According to eq. (2.41) in the notes, we have $\det(A)=\det(Q)\det(R)=\pm\det(R)$, and the determinant of the right triangular matrix $R$ is just the product of the diagonal.
Similarly, we find $\det(A)=\det(Q)\det(H)\det(Q^{T})=(\pm1)\det(H)(\pm1)=\det(H)$, so $\det(A)=\det(H)=\pm\det(R)$.
The matrix $R$ resulting from the $QR$ factorization was

\[
R=\begin{pmatrix}1.3463 & 0.9539 & 1.2206 & 0.8272 & 1.4028\\
0.0000 & 0.4655 & 0.2596 & 0.4881 & 0.4938\\
0.0000 & 0.0000 & 0.5591 & 0.1816 & 0.1847\\
0.0000 & 0.0000 & 0.0000 & 0.8304 & 0.1778\\
0.0000 & 0.0000 & 0.0000 & 0.0000 & 0.4002
\end{pmatrix}.
\]
\\
The determinant of this matrix is $\det(R)=0.1164$, in agreement with the determinant of $H$, eq. (\ref{eq:det1}), up to a minus sign.

\end{document}
