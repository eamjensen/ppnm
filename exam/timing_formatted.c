#include<stdio.h>
#include<stdlib.h>

int main() {
	FILE* file1 = fopen("timing_matrixdims.in", "r");
	FILE* file2 = fopen("timing_hessenberg.out", "r");
	FILE* file3 = fopen("timing_qr_gs.out", "r");

	int n = 0;
	char c1[100];
	char c2[100];
	printf("#N\thess\tqr\n");
	fscanf(file1, "%d", &n); //first value to be used from file1
	for (int i = 0; i < 2; i++) { //the first values to be used from file1 and file2 are the second strings of the two files
		fscanf(file2, "%s", c1);
		fscanf(file3, "%s", c2);
	}
	while (!feof(file1)) {
		printf("%d\t%1.2f\t%1.2f\n", n, atof(c1), atof(c2));
		fscanf(file1, "%d", &n);
		for (int i = 0; i < 6; i++) { //every sixth string in file2 and file3 are then to be used
			fscanf(file2, "%s", c1);
			fscanf(file3, "%s", c2);
		}
	}

	fclose(file1); fclose(file2); fclose(file3);
	return 0;
}
