#include<assert.h>
#include<gsl/gsl_matrix.h>
#include"qr_gs.h"
#include"matvec.h"

int main(int argc, char** argv) {
	assert(argc > 1);
	int n = (int) atof(argv[1]);

	gsl_matrix* A = gsl_matrix_alloc(n, n);
	gsl_matrix* R = gsl_matrix_alloc(n, n);
	
	generate_random_matrix_elements(A, n);
	qr_gs_decomp(A, R);
	
	gsl_matrix_free(A); gsl_matrix_free(R);
	return 0;
}
