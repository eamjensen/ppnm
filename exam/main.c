#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"hessenberg.h"
#include"matvec.h"
#include"qr_gs.h"

int main() {
	int n = 5;
	gsl_matrix* A = gsl_matrix_alloc(n, n);
	gsl_matrix* H = gsl_matrix_alloc(n, n);
	gsl_matrix* Q = gsl_matrix_alloc(n, n);
	gsl_matrix* X = gsl_matrix_alloc(n, n);
	gsl_matrix* Y = gsl_matrix_alloc(n, n);
	gsl_matrix* AA = gsl_matrix_alloc(n, n);
	gsl_matrix* R = gsl_matrix_alloc(n, n);

	printf("Filling a %dx%d matrix, A, with random elements. Notice: A is *not* symmetric.\n", n, n);
	generate_random_matrix_elements(A, n);
	matrix_print("A=", A, n);
	gsl_matrix_memcpy(H, A); //copy A into H
	
	printf("Reducing the matrix A to lower Hessenberg form, H, with total transformation matrix Q...\n");
	lower_hessenberg_decomp(H, Q);
	matrix_print("H=", H, n);
	matrix_print("Q=", Q, n);

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Q, Q, 0.0, X); //X <- Q*(Q^T)
	matrix_print("Q*(Q^T)=", X, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, Q, 0.0, X); //X <- (Q^T)*Q
	matrix_print("(Q^T)*Q=", X, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, H, 0.0, X); //X <- Q*H
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, X, Q, 0.0, Y); // Y <- X*(Q^T) = Q*H*(Q^T)
	matrix_print("Q*H*(Q^T)=", Y, n);
	printf("The above calculations show us that H and Q have the desired properties.\n");
	
	printf("Calculation of the determinant of a given Hessenberg matrix, det(H), has also been implemented.\nWe have, here, \n");
	printf("det(H) = %g\n", lower_hessenberg_determinant(H));




	printf("\nDoing the same once more, but this time, A is symmetric. In other words, H should be tridiagonal.\n");
	generate_symmetric_random_matrix_elements(AA, n);
	matrix_print("A=", AA, n);
	gsl_matrix_memcpy(H, AA); //copy AA into H
	
	lower_hessenberg_decomp(H, Q);
	matrix_print("H=", H, n);
	matrix_print("Q=", Q, n);

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Q, Q, 0.0, X); //X <- Q*(Q^T)
	matrix_print("Q*(Q^T)=", X, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, Q, 0.0, X); //X <- (Q^T)*Q
	matrix_print("(Q^T)*Q=", X, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, H, 0.0, X); //X <- Q*H
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, X, Q, 0.0, Y); // Y <- X*(Q^T) = Q*H*(Q^T)
	matrix_print("Q*H*(Q^T)=", Y, n);
	printf("The above calculations show us that H and Q have the desired properties.\n");

	printf("det(H) = %g\n", lower_hessenberg_determinant(H));




	printf("\nIn the figure contained in the file \"timing.pdf\" the execution time of Hessenberg factorization is compared to that of QR factorization for various matrix dimensions.\n");
	printf("The QR factorization routine is taken from the solution to exercise \"lineq\".\n");
	printf("For completeness' sake, the result of this routine will be demonstrated here on the first of the matrices A from above...\n");
	matrix_print("A=", A, n);
	gsl_matrix_memcpy(Q, A); //copy A into Q; overriding Q from before

	qr_gs_decomp(Q, R);
	printf("QR factorization gives\n");
	matrix_print("Q=", Q, n);
	matrix_print("R=", R, n);

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, Q, Q, 0.0, X); //X <- Q*(Q^T)
	matrix_print("Q*(Q^T)=", X, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, Q, Q, 0.0, X); //X <- (Q^T)*Q
	matrix_print("(Q^T)*Q=", X, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0.0, X); //X <- Q*R
	matrix_print("Q*R=", X, n);
	printf("The above calculations show us that Q and R have the desired properties.\n");

	printf("To verify that the first calculated value of det(H) is correct, we calculate det(R), which is just the product of the diagonal of R and which should be ±det(H).\n(QR factorization preserves the determinant of the orignal matrix A up to a plus or minus sign (see eq. (2.41) in the notes), whereas Hessenberg facorization completely preserves the determinant of the original matrix A: det(A) = det(Q)*det(H)*det(Q^T) = (±1)*det(H)*(±1) = det(H).)\n");
	double diag = 1.0;
	for (int i = 0; i < n; i++) {
		diag *= gsl_matrix_get(R, i, i);
	}
	printf("We find\ndet(R) = %g\n", diag);
	printf("This agrees with det(H) from above.\n");

	gsl_matrix_free(A); gsl_matrix_free(H); gsl_matrix_free(Q); gsl_matrix_free(X); gsl_matrix_free(Y); gsl_matrix_free(R); gsl_matrix_free(AA);
	return 0;
}
