#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef MATVEC_H
#define MATVEC_H
void generate_random_matrix_elements(gsl_matrix* A, int dim);
void generate_symmetric_random_matrix_elements(gsl_matrix* A, int dim);
void generate_random_vector_elements(gsl_vector* x, int dim);
void matrix_print(char* str, gsl_matrix* A, int dim);
void vector_print(char* str, gsl_vector* x, int dim);
#endif
