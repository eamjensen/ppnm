#include<gsl/gsl_matrix.h>
#include<math.h>
#include<assert.h>
#include"hessenberg.h"

void lower_hessenberg_decomp(gsl_matrix* A, gsl_matrix* Q) {
	int n = A->size1;
	assert(n > 2); //nothing to be done
	assert(n == A->size2); //A should be a square matrix
	assert(n == Q->size1 && n == Q->size2); //and so should Q -- with the same dimensions

	gsl_matrix_set_identity(Q);
	double Api, Aip, Aqi, Aiq, App, Aqq, Apq, Aqp;
	double Ap1p, Ap1q;
	double Qip, Qiq;
	double phi, c, s;
	for (int p = 1; p < n - 1; p++) {
		for (int q = p + 1; q < n; q++) {
			//finding this iteration's rotation angle
			Ap1p = gsl_matrix_get(A, p - 1, p);
			Ap1q = gsl_matrix_get(A, p - 1, q);
			phi = atan2(-Ap1q, Ap1p);
			c = cos(phi);
			s = sin(phi);

			//accumulating the transformation matrix Q
			for (int i = 0; i < n; i++) {
				Qip = gsl_matrix_get(Q, i, p);
				Qiq = gsl_matrix_get(Q, i, q);
				gsl_matrix_set(Q, i, p, c*Qip - s*Qiq);
				gsl_matrix_set(Q, i, q, s*Qip + c*Qiq);
			}

			//transforming A
			App = gsl_matrix_get(A, p, p);
			Apq = gsl_matrix_get(A, p, q);
			Aqp = gsl_matrix_get(A, q, p);
			Aqq = gsl_matrix_get(A, q, q);
			gsl_matrix_set(A, p, p, c*c*App - s*c*(Apq+Aqp) + s*s*Aqq);
			gsl_matrix_set(A, p, q, s*c*(App-Aqq) + c*c*Apq - s*s*Aqp);
			gsl_matrix_set(A, q, p, s*c*(App-Aqq) + c*c*Aqp - s*s*Apq);
			gsl_matrix_set(A, q, q, s*s*App + s*c*(Apq+Aqp) + c*c*Aqq);
			for (int i = 0; i < n; i++) { //somewhere in here A(p-1,q) is zeroed
				if (i != p && i != q) {
				Aip = gsl_matrix_get(A, i, p);
				Aiq = gsl_matrix_get(A, i, q);
				gsl_matrix_set(A, i, p, c*Aip - s*Aiq);
				gsl_matrix_set(A, i, q, s*Aip + c*Aiq);
				Api = gsl_matrix_get(A, p, i);
				Aqi = gsl_matrix_get(A, q, i);
				gsl_matrix_set(A, p, i, c*Api - s*Aqi);
				gsl_matrix_set(A, q, i, s*Api + c*Aqi);
				}
			}
		}
	}
}

double lower_hessenberg_determinant(const gsl_matrix* H) {
	int n = H->size1;
	assert(n > 1);

	double h00 = gsl_matrix_get(H, 0, 0);
	double h01 = gsl_matrix_get(H, 0, 1);
	if (n == 2) {
		double h10 = gsl_matrix_get(H, 1, 0);
		double h11 = gsl_matrix_get(H, 1, 1);
		return h00*h11 - h01*h10; //determinant of 2x2 matrix
	} else { 
		gsl_matrix_const_view M00 = 
			gsl_matrix_const_submatrix(H, 1, 1, n - 1, n - 1); //(0,0)'th minor of H
		gsl_matrix* Htemp = gsl_matrix_alloc(n, n);
		gsl_matrix_memcpy(Htemp, H); //copy H into Htemp
		gsl_matrix_swap_columns(Htemp, 0, 1);
		gsl_matrix_view M01 = 
			gsl_matrix_submatrix(Htemp, 1, 1, n - 1, n - 1); //(0,1)'th minor of H
		double result = h00*lower_hessenberg_determinant(&M00.matrix) - h01*lower_hessenberg_determinant(&M01.matrix);
		gsl_matrix_free(Htemp);
		return result;
	}
}
