#include<assert.h>
#include<gsl/gsl_matrix.h>
#include"hessenberg.h"
#include"matvec.h"

int main(int argc, char** argv) {
	assert(argc > 1);
	int n = (int) atof(argv[1]);

	gsl_matrix* A = gsl_matrix_alloc(n, n);
	gsl_matrix* Q = gsl_matrix_alloc(n, n);
	
	generate_random_matrix_elements(A, n);
	lower_hessenberg_decomp(A, Q);
	
	gsl_matrix_free(A); gsl_matrix_free(Q);
	return 0;
}
